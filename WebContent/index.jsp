<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<title>Petit Poá - Café da manhã, platters e boxes.</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="café da manhã, platters, boxes, presente, café" />
	<meta name="description" content="Café da manhã, platters e boxes. Ideal para presentear e apreciar.
O melhor presente é eternizar momentos especiais.
Entregamos em todo o rio de janeiro" />
<!--===============================================================================================-->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet">
<!--===============================================================================================-->
<link rel="icon" type="image/png" href="images/icons/favicon.png" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->

<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/daterangepicker/daterangepicker.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
	integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
	crossorigin="anonymous" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css"
	integrity="sha512-rxThY3LYIfYsVCWPCW9dB0k+e3RZB39f23ylUYTEuZMDrN/vRqLdaCBo/FbvVT6uC2r0ObfPzotsfKF9Qc5W5g=="
	crossorigin="anonymous" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">

<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" href="css/bootstrap-steps.min.css" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css">
<!--[if (lt IE 9)]><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/min/tiny-slider.helper.ie8.js"></script><![endif]-->
<link rel="stylesheet" type="text/css" href="css/util.css">
<link rel="stylesheet" type="text/css" href="css/albery.css">
<link rel="stylesheet" type="text/css" href="css/main.css">

<!--===============================================================================================-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-LQ5VQT07M1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-LQ5VQT07M1');
</script>
</head>
<body class="animsition">
	<!-- Header -->
	<header>
		<!-- Header desktop -->
		<div class="wrap-menu-header gradient1 trans-0-4">
			<div class="container h-full">
				<div class="wrap_header trans-0-3">
					<!-- Logo -->
					<div class="logo">
						<a href="index.html"> <img src="images/icons/logo.png"
							alt="IMG-LOGO" data-logofixed="images/icons/logo.png">
						</a>
					</div>

					<!-- Menu -->
					<div class="wrap_menu p-l-45 p-l-0-xl">
						<nav class="menu">
							<ul class="main_menu">
								<li><a href="index.jsp">Home</a></li>

								<li><a href="#about">Sobre</a></li>

								<li><a href="#cardapio">Faça Seu Pedido</a></li>
								
								<li><a href="#midia">Mídia</a></li>

								<li><a href="#depoimentos">Depoimentos</a></li>


							</ul>
						</nav>
					</div>

					<!-- Social -->
					<div class="social flex-w flex-l-m p-r-20">
						<a href="https://www.instagram.com/carol_da_petitpoa/"><i
							class="fa fa-instagram" aria-hidden="true"></i></a>
						<button class="btn-show-sidebar m-l-33 trans-0-4"></button>
					</div>
				</div>
			</div>
		</div>
	</header>

	<!-- Sidebar -->
	<aside class="sidebar trans-0-4">
		<!-- Button Hide sidebar -->
		<button class="btn-hide-sidebar ti-close color0-hov trans-0-4"></button>

		<!-- - -->
		<ul class="menu-sidebar p-t-95 p-b-70">
			<li class="t-center m-b-13"><a href="index.html" class="txt19 sidebarlink">Home</a>
			</li>

			<li class="t-center m-b-13"><a href="#about" class="txt19 sidebarlink">Sobre</a>
			</li>

			<li class="t-center m-b-13"><a href="#cardapio" class="txt19 sidebarlink">Faça
					seu pedido</a></li>

		
			
			<li class="t-center m-b-13"><a href="#midia" class="txt19 sidebarlink">Mídia</a>
			</li>

			<li class="t-center m-b-13"><a href="#depoimentos" class="txt19 sidebarlink">Depoimentos</a>
			</li>


		</ul>
	</aside>

	<!-- Title Page -->
	<section class="bg-title-page flex-c-m p-t-160 p-b-80 p-l-15 p-r-15"
		style="background-image: url(images/bg-intro-01.jpg); height: 10%;">

		<img src="images/icons/logo.png" alt="IMG-LOGO"
			style="width: 30%; height: auto;">


	</section>
	<section class="section-welcome bg1-pattern p-t-120 p-b-105" id="about">
		<div class="container">
			<div class="row">
				<div class="col-md-6 p-t-45 p-b-30">
					<div class="wrap-text-welcome t-center">
						<span class="tit2 t-center"> Carol </span>

						<h3 class="tit3 t-center m-b-35 m-t-5">da Petit Poá</h3>

						<p class="t-center m-b-22 size3 m-l-r-auto">Meu nome é Carol,
							sou carioca e tenho 30 anos. Sou apaixonada por plantas, gatos,
							decoração, vinho, comida boa e, principalmente, cafés da
							manhã. Juntei algumas das minhas paixões e criei a PETIT POÁ.
							Tenho como objetivo transformar o ato de comer em um gesto de
							amor. Aqui você pode eternizar momentos com uma deliciosa
							experiência, se presenteando, presenteando os amigos, o mozão ou
							alguém da família! Afinal, quem não curte um carinho em forma
							de comida?</p>


					</div>
				</div>

				<div class="col-md-6 p-b-30">
					<div
						class="wrap-pic-welcome size2 bo-rad-10 hov-img-zoom m-l-r-auto">
						<img src="images/our-story-01.jpg" alt="IMG-OUR">
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Lunch -->
	<section class="section-lunch bgwhite" id="cardapio">
		<div class="header-lunch  "
			style="background-image: url(images/cardapio.jpg)">
			<div class="bg1-overlay t-center p-t-170 p-b-165">
				<h2 class="tit4 t-center">Cardápio</h2>
			</div>
		</div>

		<div class="container">


			<div class="my-slider pagination">

				<li><a href="#"> 1
						<p class="pagination-text">Clique na foto do produto desejado</p>
				</a></li>

				<li><a href="#"> 2
						<p class="pagination-text">Preencha seus dados e escolha os
							itens do pedido</p>
				</a></li>

				<li><a href="#"> 3
						<p class="pagination-text">Clique em finalizar pedido</p>
				</a></li>

				<li><a href="#"> 4
						<p class="pagination-text">Prontinho! Agora é so aguardar
							nosso contato para realizar o pagamento</p>
				</a></li>

			</div>

			<ul class="controls customized-arrows">
				<li class="prev"><i class="fa fa-angle-left slider-prev"></i></li>
				<li class="next"><i class="fa fa-angle-right slider-next"></i>
				</li>
			</ul>


			<!-- or ul.my-slider > li -->

			<div class="row p-t-108 p-b-70">
				<div class="col-md-8 col-lg-6 m-l-r-auto">
					<!-- Block3 -->
					<div class="blo3 flex-w flex-col-l-sm m-b-30">
						<div class="pic-blo3 size20 bo-rad-10 hov-img-zoom m-r-28">
							<a href="#boxp" data-toggle="modal" data-target="#boxp"><img
								src="images/boxp.jpg" alt="IMG-MENU"></a>
						</div>

						<div class="text-blo3 size21 flex-col-l-m">
							<a href="#boxp" data-toggle="modal" data-target="#boxp"
								class="txt21 m-b-3"> Box P </a> <span class="txt23">


								<ul class="list-unstyled">

									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Sugestão para 1 pessoa:</li>
									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Contém 1 tipo de queijo e 1 tipo de frio do "grupo básico" ou
										"grupo especial".</li>

								</ul> Usamos como base: Pães, torradas, nuts, frutas frescas e secas,
								fruta tipo fondue, geleias artesanais e outros itens.<br>
								<b>A partir de R$115,00</b>
							</span>							
						</div>
					</div>

					<!-- Block3 -->
					<div class="blo3 flex-w flex-col-l-sm m-b-30">
						<div class="pic-blo3 size20 bo-rad-10 hov-img-zoom m-r-28">
							<a href="#boxm" data-toggle="modal" data-target="#boxm"><img
								src="images/boxmvinho.jpg" alt="IMG-MENU"></a>
						</div>

						<div class="text-blo3 size21 flex-col-l-m">
							<a href="#boxm" data-toggle="modal" data-target="#boxm"
								class="txt21 m-b-3"> Box M com "bons drinks" </a> <span class="txt23">

								<ul class="list-unstyled">

									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Sugestão para 1 pessoa:</li>
									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Contém 2 tipo de queijo e 3 tipo de frio do "grupo básico" ou
										"grupo especial".</li>

								</ul> Usamos como base: Pães, torradas, nuts, frutas frescas e secas,
								fruta tipo fondue, geleias artesanais e outros itens.<br>
<b>A partir de R$220,00</b>



							</span>
						</div>
					</div>

					<!-- Block3 -->
					<div class="blo3 flex-w flex-col-l-sm m-b-30">
						<div class="pic-blo3 size20 bo-rad-10 hov-img-zoom m-r-28">
							<a href="#platterm" data-toggle="modal" data-target="#platterm"><img
								src="images/platterm.jpg" alt="IMG-MENU"></a>
						</div>

						<div class="text-blo3 size21 flex-col-l-m">
							<a href="#platterm" data-toggle="modal" data-target="#platterm"
								class="txt21 m-b-3"> Platter M </a> <span class="txt23">

								<ul class="list-unstyled">

									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Sugestão para até 4 pessoas:</li>
									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Contém 2 tipos de queijos e 3 tipos de frios do "grupo básico"
										ou "grupo especial".</li>

								</ul> Usamos como base: Pães, torradas, nuts, frutas frescas e secas,
								fruta tipo fondue, geleias artesanais e outros itens.<br>
<b>A partir de R$170,00</b>


							</span>
						</div>
					</div>
					
									
					
					

				</div>

				<div class="col-md-8 col-lg-6 m-l-r-auto">
					<!-- Block3 -->
					<div class="blo3 flex-w flex-col-l-sm m-b-30">
						<div class="pic-blo3 size20 bo-rad-10 hov-img-zoom m-r-28">
							<a href="#platterg" data-toggle="modal" data-target="#platterg"><img
								src="images/platterg.jpg" alt="IMG-MENU"></a>
						</div>

						<div class="text-blo3 size21 flex-col-l-m">
							<a href="#platterg" data-toggle="modal" data-target="#platterg"
								class="txt21 m-b-3"> Platter G </a> <span class="txt23">
								<ul class="list-unstyled">

									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Sugestão para até 8 pessoas:</li>
									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Contém 3 tipos de queijos e 3 tipos de frios do "grupo básico"
										ou "grupo especial".</li>

								</ul> Usamos como base: Pães, torradas, nuts, frutas frescas e secas,
								fruta tipo fondue, geleias artesanais e outros itens. Escolha
								apenas os tipos de frios e os queijos da sua preferência.<br>

<b>A partir de R$270,00</b>


							</span>
						</div>
					</div>
					
													<!-- Block3 -->
					<div class="blo3 flex-w flex-col-l-sm m-b-30">
						<div class="pic-blo3 size20 bo-rad-10 hov-img-zoom m-r-28">
							<a href="#cafe" data-toggle="modal" data-target="#cafe"><img
								src="images/cafedamanha.jpg" alt="IMG-MENU"></a>
						</div>

						<div class="text-blo3 size21 flex-col-l-m">
							<a href="#cafe" data-toggle="modal" data-target="#cafe"
								class="txt21 m-b-3"> Café da manhã </a> <span class="txt23">
								<ul class="list-unstyled">

									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Sugestão para 2 ou 3 pessoas:</li>
									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Contém 1 tipo de queijo e 3 tipos de frios do "grupo básico"
										ou "grupo especial".</li>

								</ul> Usamos como base: Frutas frescas, fruta estilo fondue, pão de
								fermentação natural, pães doces, suco natural, capuccino, bolo
								caseiro, geleia, waffle, biscoitos, mix de nuts, iogurte com
								granola.<br>

<b>A partir de R$215,00</b>


							</span>

						</div>
					</div>
					

	<!-- 	<div class="blo3 flex-w flex-col-l-sm m-b-30">
						<div class="pic-blo3 size20 bo-rad-10 hov-img-zoom m-r-28">
							<a href="#especial2" data-toggle="modal" data-target="#especial2"><img
								src="images/especial2.jpg" alt="IMG-MENU"></a>
						</div>

						<div class="text-blo3 size21 flex-col-l-m">
							<a href="#especial2" data-toggle="modal" data-target="#especial2"
								class="txt21 m-b-3"> Sacola “me leva que eu vou” </a> <span class="txt23">
								<ul class="list-unstyled">

									
									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										

Contém 1 bouquet de begônias, 
1 sacola de ratan, 
1 pão fondue artesanal de queijo e peperoni, 
1 foto do casal estilo polaroid, 
1 azeite aromatico, 
1 mix de castanhas, 
1 batida ozê ou garrafa de vinho. 
<br>
<b>A partir de R$182,00</b>

</li></ul> 
							</span>

						</div>
					</div> 	 -->

 			<!-- <div class="blo3 flex-w flex-col-l-sm m-b-30">
						<div class="pic-blo3 size20 bo-rad-10 hov-img-zoom m-r-28">
							<a href="#especial" data-toggle="modal" data-target="#especial"><img
								src="images/especial.jpg" alt="IMG-MENU"></a>
						</div>

						<div class="text-blo3 size21 flex-col-l-m">
							<a href="#especial" data-toggle="modal" data-target="#especial"
								class="txt21 m-b-3"> Box “à luz de velas” </a> <span class="txt23">
								<ul class="list-unstyled">

									
									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Contém Torradinhas, 
Flor de salame, 
Queijo brie com geleia, 
Castanhas, 
Pastinha artesanal de gorgonzola, 
Uva, 
Morango com chocolate, 
Queijo minas frescal com favo de mel, 
Pão artesanal FAS, 
Petit four em formato de coração,  
Coco fresco, 
1 flor, 
1 mini vela.
<br>
<b>A partir de R$158,00</b>

</li></ul> 
							</span>

						</div>
					</div> 
 -->


				</div>
			</div>
		</div>
	</section>


<jsp:include page="/includes/boxPForm.jsp" />
	<jsp:include page="/includes/cafeForm.jsp" />
	<jsp:include page="/includes/platterMForm.jsp" />
	<jsp:include page="/includes/platterGForm.jsp" />
	
	<jsp:include page="/includes/boxMForm.jsp" />
	<jsp:include page="/includes/especialForm.jsp" />
	<jsp:include page="/includes/especialForm2.jsp" />

 
	
 

	
	<!-- Title Page -->
	<section class="section-lunch bgwhite" id="midia">
		<div class="header-lunch  "
			style="background-image: url(images/galeria.jpg);">
			<div class="bg1-overlay t-center p-t-170 p-b-165">
				<h2 class="tit4 t-center">Mídia</h2>
			</div>
		</div>




		<!-- Gallery -->
		<div class="section-gallery p-t-118 p-b-100">

<script src="https://apps.elfsight.com/p/platform.js" defer></script>
			<div class="wrap-gallery  flex-w p-l-25 p-r-25" style="height: auto !important;">
				
				<div class="elfsight-app-d46aaa2d-6c1d-4982-b41f-d7ad1c551320"></div>


			</div>
		</div>
	</section>

	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Carol da Petit Poá</h4>
				</div>
				<div class="modal-body">
					<p id="error">PRONTO! Seu pedido foi enviado e você receberá um email confirmando o seu pedido.Ele será criado de
						forma exclusiva e com muito carinho :)</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	
	
		<div class="modal fade centerModal" id="myModal1" role="dialog" style="padding-right:0px;">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header d-block">
				<div class="center-text">				
				<span class="tit2 t-center">Kit especial de dia das mães</span>	<br>
					<span
												style="color: red;">Pedidos até 04/05</span><br>
												Clique no banner para pegar o seu cupom de desconto "MELHORMAE"
					<h2 class="blink_me"><span class="txt9"><span
												style="color: red;">Últimas vagas para o dia 09/05. Somente para o período da tarde ou noite!</span></h2>
				</div></div>
				<div class="modal-body">
				<a href="#" id="promo">
<img class="animated-gif img-center" src="images/promo_1.gif">
				</a>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	
	
	<!-- Review -->
	<section class="section-review p-t-115 bg1-pattern" id="depoimentos">
		<!-- - -->
		<div class="title-review t-center m-b-2">
			<span class="tit2 p-l-15 p-r-15"> O que os clientes dizem </span>

			<h3 class="tit8 t-center p-l-20 p-r-15 p-t-3">Opinioes</h3>
		</div>

		<!-- - -->
		<div class="wrap-slick3">
			<div class="slick3">




				<div class="item-slick3 item3-slick3">
					<div class="wrap-content-slide3 p-b-50 p-t-50">
						<div class="container">
							<div
								class="pic-review size14 bo4 wrap-cir-pic m-l-r-auto animated visible-false"
								data-appear="zoomIn">
								<img src="images/barbara.jpeg" alt="IGM-AVATAR">
							</div>

							<div class="content-review m-t-33 animated visible-false"
								data-appear="fadeInUp">
								<p class="t-center txt12 size15 m-l-r-auto">Eu adoro
									caprichar no café da manhã! Acho que dá um gostinho de
									felicidade para o dia todo. Ando cansada de filas intermináveis
									em restaurantes, até pq a refeição tem que acontecer
									calmamente, para processarmos e incorporarmos o alimento. Ver
									uma cesta lindíssima como a Carol reúne relaxamento, prazer e a
									diversidade, atendendo todos os paladares. Os sonhos são
									disputados a tapa. As cestas são incríveis e escolho ocasiões
									especiais para fugir sempre do óbvio e acabo surpreendendo
									sempre!</p>

								<div class="star-review fs-18 color0 flex-c-m m-t-12">
									<i class="fa fa-star" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i>
								</div>

								<div
									class="more-review txt4 t-center animated visible-false m-t-32"
									data-appear="fadeInUp">Barbara Joia</div>
							</div>
						</div>
					</div>
				</div>

				<div class="item-slick3 item2-slick3">
					<div class="wrap-content-slide3 p-b-50 p-t-50">
						<div class="container">
							<div
								class="pic-review size14 bo4 wrap-cir-pic m-l-r-auto animated visible-false"
								data-appear="zoomIn">
								<img src="images/thaiza.jpeg" alt="IGM-AVATAR">
							</div>

							<div class="content-review m-t-33 animated visible-false"
								data-appear="fadeInUp">
								<p class="t-center txt12 size15 m-l-r-auto">A cesta da Carol
									é amor e aconchego em forma de comida. Ela tem o cuidado de
									fazer um arranjo lindo, com os mínimos detalhes decorativos,
									com tudo bem fresquinho e com várias combinações de sabores.
									Dessa forma, agrada àqueles que comem com os olhos e atende a
									todos os tipos de paladares. Iniciar o dia com uma cesta de
									café de manhã da Carol, é garantia de que você terá um bom dia,
									pois coisas boas acontecem com aqueles que estão com o coração
									(e estômago) preenchido 💛</p>

								<div class="star-review fs-18 color0 flex-c-m m-t-12">
									<i class="fa fa-star" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i>
								</div>

								<div
									class="more-review txt4 t-center animated visible-false m-t-32"
									data-appear="fadeInUp">Thaiza Gomara</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="item-slick3 item3-slick3">
					<div class="wrap-content-slide3 p-b-50 p-t-50">
						<div class="container">
							<div
								class="pic-review size14 bo4 wrap-cir-pic m-l-r-auto animated visible-false"
								data-appear="zoomIn">
								<img src="images/anapaula.jpeg" alt="IGM-AVATAR">
							</div>

							<div class="content-review m-t-33 animated visible-false"
								data-appear="fadeInUp">
								<p class="t-center txt12 size15 m-l-r-auto">Num mundo cheio de obviedades e coisas sem autenticidade, presentear alguém que você ama com um platter ou um box desses é uma surpresa maravilhosa, sensível e inesquecível. Parece que a Carol da Petit Poa pensa nisso tudo quando ela arruma cada nut e harmoniza os sabores com muita delicadeza. Pra impressionar qualquer pessoa de bom gosto, vale a pena pedir uma.</p>

								<div class="star-review fs-18 color0 flex-c-m m-t-12">
									<i class="fa fa-star" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i>
								</div>

								<div
									class="more-review txt4 t-center animated visible-false m-t-32"
									data-appear="fadeInUp">Ana Paula</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="item-slick3 item2-slick3">
					<div class="wrap-content-slide3 p-b-50 p-t-50">
						<div class="container">
							<div
								class="pic-review size14 bo4 wrap-cir-pic m-l-r-auto animated visible-false"
								data-appear="zoomIn">
								<img src="images/janis.jpeg" alt="IGM-AVATAR">
							</div>

							<div class="content-review m-t-33 animated visible-false"
								data-appear="fadeInUp">
								<p class="t-center txt12 size15 m-l-r-auto">A Carol da Petit
									poá apareceu na hora certa pra mim, eu precisava de um algo
									original, que precisava ser lindo também, e ela atendeu a todas
									as minhas expectativas, e foi além. Tudo feito e escolhido com
									muito carinho e amor, eu amei e super indico.</p>

								<div class="star-review fs-18 color0 flex-c-m m-t-12">
									<i class="fa fa-star" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i> <i
										class="fa fa-star p-l-1" aria-hidden="true"></i>
								</div>

								<div
									class="more-review txt4 t-center animated visible-false m-t-32"
									data-appear="fadeInUp">Janis joplin</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="wrap-slick3-dots m-t-30"></div>
		</div>
	</section>

	<!-- Footer -->
	<footer class="bg1">


		<div class="end-footer bg2">
			<div class="container">
				<div class="flex-sb-m flex-w p-t-22 p-b-22">
					<div class="p-t-5 p-b-5">
						<a href="https://www.instagram.com/carol_da_petitpoa/"
							class="fs-15 c-white"><i class="fa fa-instagram"
							aria-hidden="true"></i></a>
					</div>

					<div class="txt17 p-r-20 p-t-5 p-b-5">Copyright &copy; 2020
						CaroldaPetitPoa | Desenvolvido por <a href="https://www.zeituneinformatica.com.br"
							class="fs-15 c-white">Zeituneinformatica.</a></div>
				</div>
			</div>
		</div>
	</footer>


	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top"> <i
			class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<a href="https://api.whatsapp.com/send?phone=5521965802201" class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a>


	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/jquery/jquery.validate.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript"
		src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript"
		src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript"
		src="vendor/daterangepicker/moment.min.js"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"
		integrity="sha512-3n19xznO0ubPpSwYCRRBgHh63DrV+bdZfHK52b1esvId4GsfwStQNPJFjeQos2h3JwCmZl0/LgLxSKMAI55hgw=="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
		integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
		crossorigin="anonymous"></script>

	<script
		src="//cdn.rawgit.com/niftylettuce/bootstrap-datepicker-mobile/9999ca720ebf89600bda1659c96a291dc447ff39/bootstrap-datepicker-mobile.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.pt-BR.min.js"
		integrity="sha512-mVkLPLQVfOWLRlC2ZJuyX5+0XrTlbW2cyAwyqgPkLGxhoaHNSWesYMlcUjX8X+k45YB8q90s88O7sos86636NQ=="
		crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="js/slick-custom.js"></script>

	<!--===============================================================================================-->
	<script type="text/javascript"
		src="vendor/lightbox2/js/lightbox.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/isotope/isotope.pkgd.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
	<!-- NOTE: prior to v2.2.1 tiny-slider.js need to be in <body> -->
	<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery.form.js"></script>
	<script src="js/buscar-cep.js"></script>
	<script src="js/jquery.mask.js"></script>
	<script src="https://js.api.here.com/v3/3.1/mapsjs-core.js"
  type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-service.js"
  type="text/javascript" charset="utf-8"></script>
  <script src="js/albery.js"></script>
	<script src="js/main.js"></script>

	<script>
		$('body').on(
				'show.bs.modal',
				'.modal',
				function(e) {
					// fix the problem of ios modal form with input field
					var $this = $(this);
					if (navigator.userAgent.match(/Android/i)
							&& $(window).width() < 767) {
						$this.find('input').blur(function() {
							$('.modal-open').removeClass('fix-modal')
						}).focus(function() {
							$('.modal-open').addClass('fix-modal')
						});
					}
				});
		
        
		var slider = tns({
			container: '.my-slider',
			items: 1,
			controls: false,
			autoplay: true,
			navAsThumbnails: true,
			autoplayButtonOutput: false,
			responsive: {
				640: {
					edgePadding: 20,
					gutter: 20,
					items: 2
				},
				700: {
					gutter: 30
				},
				900: {
					items: 4
				}
			}
		});


		document.querySelector('.slider-next').onclick = function () { slider.goTo('next'); };
		document.querySelector('.slider-prev').onclick = function () { slider.goTo('prev'); };



	</script>

</body>
</html>
