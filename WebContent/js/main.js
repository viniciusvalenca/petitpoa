
(function ($) {
	"use strict";


	/*
	 * [ Load page ] ===========================================================
	 */
	$(".animsition").animsition({
		inClass: 'fade-in',
		outClass: 'fade-out',
		inDuration: 1500,
		outDuration: 800,
		linkElement: '.animsition-link',
		loading: true,
		loadingParentElement: 'html',
		loadingClass: 'animsition-loading-1',
		loadingInner: '<div class="cp-spinner cp-meter"></div>',
		timeout: false,
		timeoutCountdown: 2000,
		onLoadEvent: true,
		browser: ['animation-duration', '-webkit-animation-duration'],
		overlay: false,
		overlayClass: 'animsition-overlay-slide',
		overlayParentElement: 'html',
		transition: function (url) { window.location.href = url; }
	
	});

	/*
	 * [ Back to top ]
	 * ===========================================================
	 */
	var windowH = $(window).height() / 2;

	$(window).on('scroll', function () {
		if ($(this).scrollTop() > windowH) {
			$("#myBtn").css('display', 'flex');
		} else {
			$("#myBtn").css('display', 'none');
		}
	});

	$('#myBtn').on("click", function () {
		$('html, body').animate({ scrollTop: 0 }, 300);
	});


	/*
	 * [ Select ] ===========================================================
	 */
	$(".selection-1").select2({
		minimumResultsForSearch: -1
	});
	
	



	/*
	 * [ Daterangepicker ]
	 * ===========================================================
	 */
	$('.date').datepicker({
		language: 'pt-BR',
		format: "dd/mm/yyyy",
		datesDisabled: ["30/04/2021", "01/05/2021", "02/05/2021","18/06/2021", "19/06/2021", "20/06/2021", "21/06/2021", "17/06/2021"],
		autoclose: true
	});

	

	



	/*
	 * [ Fixed Header ]
	 * ===========================================================
	 */
	var header = $('header');
	var logo = $(header).find('.logo img');
	var linkLogo1 = $(logo).attr('src');
	var linkLogo2 = $(logo).data('logofixed');


	$(window).on('scroll', function () {
		if ($(this).scrollTop() > 5 && $(this).width() > 992) {
			$(logo).attr('src', linkLogo2);
			$(header).addClass('header-fixed');
		}
		else {
			$(header).removeClass('header-fixed');
			$(logo).attr('src', linkLogo1);
		}

	});

	/*
	 * [ Show/hide sidebar ]
	 * ===========================================================
	 */
	$('body').append('<div class="overlay-sidebar trans-0-4"></div>');
	var ovlSideBar = $('.overlay-sidebar');
	var btnShowSidebar = $('.btn-show-sidebar');
	var btnHideSidebar = $('.btn-hide-sidebar');
	var sidebar = $('.sidebar');
	var sidebarlink = $('.sidebarlink');

	$(btnShowSidebar).on('click', function () {
		$(sidebar).addClass('show-sidebar');
		$(ovlSideBar).addClass('show-overlay-sidebar');
	})

	$(btnHideSidebar).on('click', function () {
		$(sidebar).removeClass('show-sidebar');
		$(ovlSideBar).removeClass('show-overlay-sidebar');
	})

	$(btnHideSidebar).on('click', function () {
		$(sidebar).removeClass('show-sidebar');
		$(ovlSideBar).removeClass('show-overlay-sidebar');
	})

	$(ovlSideBar).on('click', function () {
		$(sidebar).removeClass('show-sidebar');
		$(ovlSideBar).removeClass('show-overlay-sidebar');
	})

	$(sidebarlink).on('click', function () {
		$(sidebar).removeClass('show-sidebar');
		$(ovlSideBar).removeClass('show-overlay-sidebar');
	})



	/*
	 * [ Isotope ] ===========================================================
	 */
	var $topeContainer = $('.isotope-grid');
	var $filter = $('.filter-tope-group');



	// init Isotope
	$(window).on('load', function () {
		var $grid = $topeContainer.each(function () {
			$(this).isotope({
				itemSelector: '.isotope-item',
				percentPosition: true,
				animationEngine: 'best-available',
				masonry: {
					columnWidth: '.isotope-item'
				}
			});
		});
	});

	var labelGallerys = $('.label-gallery');

	$(labelGallerys).each(function () {
		$(this).on('click', function () {
			for (var i = 0; i < labelGallerys.length; i++) {
				$(labelGallerys[i]).removeClass('is-actived');
			}

			$(this).addClass('is-actived');
		});
	});



	// Ascending order
	var responsiveIsotope = [
		[480, 7],
		[720, 10]
	];

	var itemsPerPageDefault = 6;
	var itemsPerPage = defineItemsPerPage();
	var currentNumberPages = 1;
	var currentPage = 1;
	var currentFilter = '*';
	var filterAtribute = 'data-filter';
	var pageAtribute = 'data-page';
	var pagerClass = 'isotope-pager';

	function changeFilter(selector) {
		$topeContainer.isotope({
			filter: selector
		});
	}


	function goToPage(n) {

		currentPage = n;

		var selector = ".isotope-item";
		selector += (currentFilter != '*') ? '[' + filterAtribute + '="' + currentFilter + '"]' : '';
		selector += '[' + pageAtribute + '="' + currentPage + '"]';

		changeFilter(selector);
	}

	function defineItemsPerPage() {
		var pages = itemsPerPageDefault;

		for (var i = 0; i < responsiveIsotope.length; i++) {
			if ($(window).width() <= responsiveIsotope[i][0]) {
				pages = responsiveIsotope[i][1];
				break;
			}



		}

		return pages;
	}

	function setPagination() {

		var SettingsPagesOnItems = function () {

			var itemsLength = $topeContainer.children(".isotope-item").length;

			var pages = Math.ceil(itemsLength / itemsPerPage);
			var item = 1;
			var page = 1;
			var selector = ".isotope-item";
			selector += (currentFilter != '*') ? '[' + filterAtribute + '="' + currentFilter + '"]' : '';

			$topeContainer.children(selector).each(function () {
				if (item > itemsPerPage) {
					page++;
					item = 1;
				}
				$(this).attr(pageAtribute, page);
				item++;
			});

			currentNumberPages = page;

		}();

		var CreatePagers = function () {

			var $isotopePager = ($('.' + pagerClass).length == 0) ? $('<div class="item-pagination flex-c-m trans-0-4' + pagerClass + '"></div>') : $('.' + pagerClass);

			$isotopePager.html('');

			for (var i = 0; i < currentNumberPages; i++) {
				var $pager = $('<a href="javascript:void(0);" class="pager item-pagination flex-c-m trans-0-4" ' + pageAtribute + '="' + (i + 1) + '"></a>');
				$pager.html(i + 1);

				$pager.click(function () {
					var page = $(this).eq(0).attr(pageAtribute);
					goToPage(page);
				});

				$pager.appendTo($isotopePager);
			}

			$topeContainer.after($isotopePager);

		}();

	}

	// filter items on button click
	$('.isotope-pager').on('click', 'a', function () {
		var filterValue = $(this).attr('data-page');

		$('.isotope-pager a').removeClass('active-pagination');
		$(this).addClass('active-pagination');
	})


	setPagination();
	goToPage(1);

	// Adicionando Event de Click para as categorias
	$filter.on('click', 'button', function () {
		var filter = $(this).attr(filterAtribute);
		currentFilter = filter;

		setPagination();
		goToPage(1);


	});

	// Evento Responsivo
	$(window).resize(function () {
		itemsPerPage = defineItemsPerPage();
		setPagination();
	});


 

	$('input[name="presente"]').on('change', function () {
		$('input[name="' + this.name + '"]').not(this).prop('checked', false);
	});
	
	
	var $fpagamento = 0;
	var $fAme = 0;
	var $fDesconto = 0;
	var $fAcrescimo = 0;
	$('input[name="fpagamento"]').on('change', function () {
	if(this.value==="Cartão de crédito" && $fAme===0){
	$fpagamento=1;
	$fAme=1
	$fAcrescimo = Math.ceil(($sum*4/100));
	$sum += + $fAcrescimo
		$("input[name=subtotal]").attr("placeholder", "R$" + $sum-$fAcrescimo + ",00");
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");		
		$("input[name=valor]").val($sum);
	}else{
		if($fpagamento===1 && $fAme===1){
			$fpagamento=0
			$fAme=0
			$sum -= + $fAcrescimo
			$fAcrescimo=0
			$("input[name=subtotal]").attr("placeholder", "R$" + $sum-$fAcrescimo + ",00");
			$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");		
			$("input[name=valor]").val($sum);
		}
	}
		$('input[name="' + this.name + '"]').not(this).prop('checked', false);
	});
	
	
	


	var array_chk_cafe_queijo = [];
	$("input[name=chk-cafe-queijo]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_cafe_queijo.push(this.id);
			if (array_chk_cafe_queijo.length > 1) {
				array_chk_cafe_queijo.splice(0, 1);
			}
			$("input[name=chk-cafe-queijo]").prop("checked", false);
			for (var i = 0; i < array_chk_cafe_queijo.length; i++) {
				$("#" + array_chk_cafe_queijo[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_cafe_queijo.indexOf(this.id);
			array_chk_cafe_queijo.splice(index, 1);
		}
	});



	var array_chk_cafe_frios = [];
	$("input[name=chk-cafe-frios]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_cafe_frios.push(this.id);
			if (array_chk_cafe_frios.length > 3) {
				array_chk_cafe_frios.splice(0, 1);
			}
			$("input[name=chk-cafe-frios]").prop("checked", false);
			for (var i = 0; i < array_chk_cafe_frios.length; i++) {
				$("#" + array_chk_cafe_frios[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_cafe_frios.indexOf(this.id);
			array_chk_cafe_frios.splice(index, 1);
		}
	});



	var array_chk_cafe_sucos = [];
	$("input[name=chk-cafe-sucos]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_cafe_sucos.push(this.id);
			if (array_chk_cafe_sucos.length > 1) {
				array_chk_cafe_sucos.splice(0, 1);
			}
			$("input[name=chk-cafe-sucos]").prop("checked", false);
			for (var i = 0; i < array_chk_cafe_sucos.length; i++) {
				$("#" + array_chk_cafe_sucos[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_cafe_sucos.indexOf(this.id);
			array_chk_cafe_sucos.splice(index, 1);
		}
	});






	var array_chk_cafe_bolos = [];
	$("input[name=chk-cafe-bolos]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_cafe_bolos.push(this.id);
			if (array_chk_cafe_bolos.length > 1) {
				array_chk_cafe_bolos.splice(0, 1);
			}
			$("input[name=chk-cafe-bolos]").prop("checked", false);
			for (var i = 0; i < array_chk_cafe_bolos.length; i++) {
				$("#" + array_chk_cafe_bolos[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_cafe_bolos.indexOf(this.id);
			array_chk_cafe_bolos.splice(index, 1);
		}
	});


	var array_chk_boxp_queijo = [];
	$("input[name=chk-boxp-queijo]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_boxp_queijo.push(this.id);
			if (array_chk_boxp_queijo.length > 1) {
				array_chk_boxp_queijo.splice(0, 1);
			}
			$("input[name=chk-boxp-queijo]").prop("checked", false);
			for (var i = 0; i < array_chk_boxp_queijo.length; i++) {
				$("#" + array_chk_boxp_queijo[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_boxp_queijo.indexOf(this.id);
			array_chk_boxp_queijo.splice(index, 1);
		}
	});
	
	var array_chk_especial_batida = [];
	$("input[name=chk-especial-batida]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_especial_batida.push(this.id);
			if (array_chk_especial_batida.length > 2) {
				array_chk_especial_batida.splice(0, 1);
			}
			$("input[name=chk-especial-batida]").prop("checked", false);
			for (var i = 0; i < array_chk_especial_batida.length; i++) {
				$("#" + array_chk_especial_batida[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_especial_batida.indexOf(this.id);
			array_chk_especial_batida.splice(index, 1);
		}
	});
	
	var array_chk_especial2_batida = [];
	$("input[name=chk-especial2-batida]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_especial2_batida.push(this.id);
			if (array_chk_especial2_batida.length > 1) {
				array_chk_especial2_batida.splice(0, 1);
			}
			$("input[name=chk-especial2-batida]").prop("checked", false);
			for (var i = 0; i < array_chk_especial2_batida.length; i++) {
				$("#" + array_chk_especial2_batida[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_especial2_batida.indexOf(this.id);
			array_chk_especial2_batida.splice(index, 1);
		}
	});
	
	var array_chk_especial2_vinho= [];
	$("input[name=chk-especial2-vinho]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_especial2_vinho.push(this.id);
			if (array_chk_especial2_vinho.length > 1) {
				array_chk_especial2_vinho.splice(0, 1);
			}
			$("input[name=chk-especial2-vinho]").prop("checked", false);
			for (var i = 0; i < array_chk_especial2_vinho.length; i++) {
				$("#" + array_chk_especial2_vinho[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_especial2_vinho.indexOf(this.id);
			array_chk_especial2_vinho.splice(index, 1);
		}
	});



	var array_chk_boxp_frios = [];
	$("input[name=chk-boxp-frios]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_boxp_frios.push(this.id);
			if (array_chk_boxp_frios.length > 1) {
				array_chk_boxp_frios.splice(0, 1);
			}
			$("input[name=chk-boxp-frios]").prop("checked", false);
			for (var i = 0; i < array_chk_boxp_frios.length; i++) {
				$("#" + array_chk_boxp_frios[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_boxp_frios.indexOf(this.id);
			array_chk_boxp_frios.splice(index, 1);
		}
	});




	var array_chk_boxm_queijo = [];
	$("input[name=chk-boxm-queijo]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_boxm_queijo.push(this.id);
			if (array_chk_boxm_queijo.length > 2) {
				array_chk_boxm_queijo.splice(0, 1);
			}
			$("input[name=chk-boxm-queijo]").prop("checked", false);
			for (var i = 0; i < array_chk_boxm_queijo.length; i++) {
				$("#" + array_chk_boxm_queijo[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_boxm_queijo.indexOf(this.id);
			array_chk_boxm_queijo.splice(index, 1);
		}
	});


	var array_chk_boxm_frios = [];
	$("input[name=chk-boxm-frios]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_boxm_frios.push(this.id);
			if (array_chk_boxm_frios.length > 3) {
				array_chk_boxm_frios.splice(0, 1);
			}
			$("input[name=chk-boxm-frios]").prop("checked", false);
			for (var i = 0; i < array_chk_boxm_frios.length; i++) {
				$("#" + array_chk_boxm_frios[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_boxm_frios.indexOf(this.id);
			array_chk_boxm_frios.splice(index, 1);
		}
	});
	
	
	var $drink = 0;
	
	var array_chk_boxm_drink = [];
	$("input[name=chk-boxm-drink]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_boxm_drink.push(this.id);
			if(this.id==="dtinto" || this.id==="dbranco" ||this.id==="drose"){
				$sum -= + $drink;				
				$drink=0;			
		}else if(this.id==="dchandon"){
			$sum -= + $drink;				
			$drink=60;
		}else if(this.id==="dgin"){
			$sum -= + $drink;				
			$drink=90;
		}		
			if (array_chk_boxm_drink.length > 1) {
				array_chk_boxm_drink.splice(0, 1);				
			}
			$("input[name=chk-boxm-drink]").prop("checked", false);
			for (var i = 0; i < array_chk_boxm_drink.length; i++) {
				$("#" + array_chk_boxm_drink[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_boxm_drink.indexOf(this.id);
			$sum -= + $drink;
			$drink=0;
			array_chk_boxm_drink.splice(index, 1);
		}		
		$sum += + $drink;
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
		$("input[name=valor]").val($sum);
	});




	var array_chk_platterg_queijo = [];
	$("input[name=chk-platterg-queijo]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_platterg_queijo.push(this.id);
			if (array_chk_platterg_queijo.length > 3) {
				array_chk_platterg_queijo.splice(0, 1);
			}
			$("input[name=chk-platterg-queijo]").prop("checked", false);
			for (var i = 0; i < array_chk_platterg_queijo.length; i++) {
				$("#" + array_chk_platterg_queijo[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_platterg_queijo.indexOf(this.id);
			array_chk_platterg_queijo.splice(index, 1);
		}
	});

	var array_chk_platterg_frios = [];
	$("input[name=chk-platterg-frios]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_platterg_frios.push(this.id);
			if (array_chk_platterg_frios.length > 3) {
				array_chk_platterg_frios.splice(0, 1);
			}
			$("input[name=chk-platterg-frios]").prop("checked", false);
			for (var i = 0; i < array_chk_platterg_frios.length; i++) {
				$("#" + array_chk_platterg_frios[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_platterg_frios.indexOf(this.id);
			array_chk_platterg_frios.splice(index, 1);
		}
	});



	var array_chk_platterm_queijo = [];
	$("input[name=chk-platterm-queijo]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_platterm_queijo.push(this.id);
			if (array_chk_platterm_queijo.length > 2) {
				array_chk_platterm_queijo.splice(0, 1);
			}
			$("input[name=chk-platterm-queijo]").prop("checked", false);
			for (var i = 0; i < array_chk_platterm_queijo.length; i++) {
				$("#" + array_chk_platterm_queijo[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_platterm_queijo.indexOf(this.id);
			array_chk_platterm_queijo.splice(index, 1);
		}
	});

	var array_chk_platterm_frios = [];
	$("input[name=chk-platterm-frios]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_platterm_frios.push(this.id);
			if (array_chk_platterm_frios.length > 3) {
				array_chk_platterm_frios.splice(0, 1);
			}
			$("input[name=chk-platterm-frios]").prop("checked", false);
			for (var i = 0; i < array_chk_platterm_frios.length; i++) {
				$("#" + array_chk_platterm_frios[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_platterm_frios.indexOf(this.id);
			array_chk_platterm_frios.splice(index, 1);
		}
	});

	
	var array_chk_especial_queijo = [];
	$("input[name=chk-especial-queijo]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_especial_queijo.push(this.id);
			if (array_chk_especial_queijo.length > 1) {
				array_chk_especial_queijo.splice(0, 1);
			}
			$("input[name=chk-especial-queijo]").prop("checked", false);
			for (var i = 0; i < array_chk_especial_queijo.length; i++) {
				$("#" + array_chk_especial_queijo[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_especial_queijo.indexOf(this.id);
			array_chk_especial_queijo.splice(index, 1);
		}
	});


	var array_chk_especial_frios = [];
	$("input[name=chk-especial-frios]").change(function () {
		if ($(this).is(":checked")) {
			array_chk_especial_frios.push(this.id);
			if (array_chk_especial_frios.length > 1) {
				array_chk_especial_frios.splice(0, 1);
			}
			$("input[name=chk-especial-frios]").prop("checked", false);
			for (var i = 0; i < array_chk_especial_frios.length; i++) {
				$("#" + array_chk_especial_frios[i]).prop("checked", true);
			}
		}
		else {
			var index = array_chk_especial_frios.indexOf(this.id);
			array_chk_especial_frios.splice(index, 1);
		}
	});





	var behavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
		options = {
			onKeyPress: function (val, e, field, options) {
				field.mask(behavior.apply({}, arguments), options);
			}
		};

	$('input[name=telefone]').mask(behavior, options);
	$('input[name=data]').mask("00/00/0000");
	$('input[name=cep]').mask("00000-000");
	$('input[name=numero]').mask("9999999");


	var $sum = 0;
	var $subSum = 0;
	var $especial = 0;
	var $platterg = 0;
	var $cafe = 0;
	var $boxp = 0;
	var $cupom = 0;

	$('input:checkbox').on('change', function () {
		if (this.checked) {
			if (verificaespecial(this.value)) {
				if ($especial == 0) {
					if ($platterg == 1) {
						$sum += + 30;
						$subSum += + 30;
					} else if ($cafe == 1) {
						$sum += + 15;
						$subSum += + 15;
					} else if ($boxp == 1) {
						$sum += + 10;
						$subSum += + 10;
					}else {
						$sum += + 25;
						$subSum += + 25;
					}
				}
				$especial += +1
			} else {
				$sum += + pegarvalor(this.value);
				$subSum += + pegarvalor(this.value);
			}
		}
		else {
			if (verificaespecial(this.value)) {
				if ($especial == 1) {
					if ($platterg == 1) {
						$sum -= + 30;
						$subSum -= + 30;
					} else if ($cafe == 1) {
						$sum -= + 15;
						$subSum -= + 15;
					} else if ($boxp == 1) {
						$sum -= + 10;
						$subSum -= + 10;
					}else {
						$sum -= + 25;
						$subSum -= + 25;
					}
				}
				$especial -= +1
			} else {
				$sum -= + pegarvalor(this.value);
				$subSum -= + pegarvalor(this.value);
			}
		}
		
			$("input[name=total]").attr("placeholder", "R$" +$sum + ",00");	
			
			$("input[name=valor]").val(($sum));
			$("input[name=subtotalhidden]").val(($subSum));
			
		
		
		$("input[name=subtotal]").attr("placeholder", "R$" + $subSum  + ",00");
		
	});

	$('#boxp').on('shown.bs.modal', function (e) {
		$sum = 105;
		$fDesconto = 0;
		$subSum = 105;
		$boxp = 1;
		$freteAntigo = 0;
		$especial=0;
		$(".sucos").hide();
		$(".bloco_end").show();
		$(".vinhos").hide();
		$( ".cupom" ).prop( "disabled", false );
		$(".textbox").hide();
		$("input[name=frete]").attr("placeholder", "Preencha o endereço e calcule o valor do frete");
		$("input[name=fretehidden]").val($freteAntigo);
		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
		$("input[name=subtotal]").attr("placeholder", "R$" + $subSum + ",00");
		$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
		$("#cidade1").val("Rio de Janeiro");
		$("#estado1").val("RJ");
		$(':checkbox, :radio').prop('checked', false);
	
		
	});
	
	$('#especial').on('shown.bs.modal', function (e) {
		$sum=158;
		$subSum=158;
		$fDesconto = 0;
		
		$boxp = 1;
		$freteAntigo = 0;
		$especial=0;
		
		$(".bloco_end").show();
		
		
		$(".textbox").hide();
		$("input[name=frete]").attr("placeholder", "Preencha o endereço e calcule o valor do frete");
		$("input[name=fretehidden]").val($freteAntigo);
		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
		$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
		$("#cidade6").val("Rio de Janeiro");
		$("#estado6").val("RJ");
		$(':checkbox, :radio').prop('checked', false);
	});
	
	$('#especial2').on('shown.bs.modal', function (e) {
		$sum=182;
		$subSum=182;
		$fDesconto = 0;
		
		$boxp = 1;
		$freteAntigo = 0;
		$especial=0;
		
		$(".bloco_end").show();
		
		
		$(".textbox").hide();
		$("input[name=frete]").attr("placeholder", "Preencha o endereço e calcule o valor do frete");
		$("input[name=fretehidden]").val($freteAntigo);
		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
		$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
		$("#cidade7").val("Rio de Janeiro");
		$("#estado7").val("RJ");
		$(':checkbox, :radio').prop('checked', false);
		
	});
	
	
	
	
	$("input[name=cupom]").blur(function(){		
		
		  if($("#form-pedido1 input[name=cupom]").val()=="PETITPOAFAZ1" || $("#form-pedido2 input[name=cupom]").val()=="PETITPOAFAZ1" || $("#form-pedido3 input[name=cupom]").val()=="PETITPOAFAZ1" || $("#form-pedido4 input[name=cupom]").val()=="PETITPOAFAZ1" || $("#form-pedido5 input[name=cupom]").val()=="PETITPOAFAZ1"){
			  $cupom = 1;
				$fDesconto = Math.ceil((($sum-$freteAntigo)*15/100));
				$sum -= + $fDesconto
			  $("input[name=total]").attr("placeholder", "R$" + ($sum) + ",00");
			  $("input[name=desconto]").attr("placeholder", "R$"+$fDesconto+",00");
			  $("input[name=valor]").val(($sum));
			  $("input[name=subtotal]").attr("placeholder", "R$" + ($subSum)  + ",00");
			  $("input[name=subtotalhidden]").val(($subSum));
			  $( "#cupom" ).prop( "disabled", true );
		  }else{
			  $cupom = 0;
			  $("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
			  $("input[name=desconto]").attr("placeholder", "R$0,00");
			  $("input[name=valor]").val($sum);
			  $("input[name=subtotal]").attr("placeholder", "R$" + ($subSum)  + ",00");
			  $("input[name=subtotalhidden]").val(($subSum));
		  }
		});

	$('#boxm').on('shown.bs.modal', function (e) {
		$sum = 220;
		$fDesconto = 0;
		$subSum = 220;
		$freteAntigo = 0;
		$especial=0;
		$(".sucos").hide();
		$(".textbox").hide();
		$( ".cupom" ).prop( "disabled", false );
		$(".bloco_end").show();
		$("input[name=frete]").attr("placeholder", "Preencha o endereço e calcule o valor do frete");
		$("input[name=fretehidden]").val($freteAntigo);
		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
		$("input[name=subtotal]").attr("placeholder", "R$" + $subSum + ",00");
		$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
		$("#cidade2").val("Rio de Janeiro");
		$("#estado2").val("RJ");
		$(':checkbox, :radio').prop('checked', false);
	});

	$('#platterm').on('shown.bs.modal', function (e) {
		$sum = 170
		$subSum = 170
		$fDesconto = 0;
		$freteAntigo = 0;
		$especial=0;
		$(".sucos").hide();
		$(".textbox").hide();
		$( ".cupom" ).prop( "disabled", false );
		$(".bloco_end").show();
		$("input[name=frete]").attr("placeholder", "Preencha o endereço e calcule o valor do frete");
		$("input[name=fretehidden]").val($freteAntigo);
		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
		$("input[name=subtotal]").attr("placeholder", "R$" + $subSum + ",00");
		$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
		$("#cidade5").val("Rio de Janeiro");
		$("#estado5").val("RJ");
		$(':checkbox, :radio').prop('checked', false);
	});

	$('#platterg').on('shown.bs.modal', function (e) {
		$sum = 270
		$subSum = 270
		$fDesconto = 0;
		$freteAntigo = 0;
		$especial=0;
		$(".sucos").hide();
		$( ".cupom" ).prop( "disabled", false );
		$(".textbox").hide();
		$(".bloco_end").show();
		$("input[name=frete]").attr("placeholder", "Preencha o endereço e calcule o valor do frete");
		$("input[name=fretehidden]").val($freteAntigo);
		$platterg = 1;
		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
		$("input[name=subtotal]").attr("placeholder", "R$" + $subSum + ",00");
		$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
		$("#cidade4").val("Rio de Janeiro");
		$("#estado4").val("RJ");
		$(':checkbox, :radio').prop('checked', false);
	});

	$('#platterg').on('hidden.bs.modal', function () {
		$platterg = 0;
	})

	$('#cafe').on('shown.bs.modal', function (e) {
		$sum = 215
		$subSum = 215
		$fDesconto = 0;
		$freteAntigo = 0;
		$especial=0;
		$(".sucos").hide();
		$(".textbox").hide();
		$(".bloco_end").show();
		$( ".cupom" ).prop( "disabled", false );
		$("input[name=frete]").attr("placeholder", "Preencha o endereço e calcule o valor do frete");
		$("input[name=fretehidden]").val($freteAntigo);
		$cafe = 1;
		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
		$("input[name=subtotal]").attr("placeholder", "R$" + $subSum + ",00");
		$(':input').not(':button, :submit, :reset, :hidden, :checkbox, :radio').val('');
		$("#cidade3").val("Rio de Janeiro");
		$("#estado3").val("RJ");
		$(':checkbox, :radio').prop('checked', false);
	});

	$('#cafe').on('hidden.bs.modal', function () {
		$cafe = 0;
	})
	
	$('#boxp').on('hidden.bs.modal', function () {
		$boxp = 0;
	})
	
	$('#especial').on('hidden.bs.modal', function () {
		$boxp = 0;
		$cupom=0;
		$("input[name=cupom]").attr("placeholder", "Insira aqui seu cupom");
	})

	var $vinho = 0;


	$('#tipobebida').change(function () {
		$('.bebidas').hide();
		$('#' + $(this).val()).show();
		if ($(this).val() === "Vinhos" && $vinho == 0) {
			$vinho = 1;
			$sum += + 20;
			$subSum += + 20;
		} else if ($(this).val() === "Sucos" && $vinho == 1) {
			$vinho = 0;
			$sum -= + 20;
			$subSum -= + 20;
		}
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
		$("input[name=subtotal]").attr("placeholder", "R$" + $subSum + ",00");
		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
	});
	
	$('#tipobebida1').change(function () {
		$('.bebidas1').hide();
		$('#' + $(this).val()).show();
		if ($(this).val() === "Vinhos" && $vinho == 0) {
			$vinho = 1;
			$sum += + 20;
			$subSum += + 20;
		} else if ($(this).val() === "Sucos" && $vinho == 1) {
			$vinho = 0;
			$sum -= + 20;
			$subSum -= + 20;
		}
		$("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
		
		$("input[name=subtotal]").attr("placeholder", "R$" + $subSum + ",00");

		$("input[name=valor]").val($sum);
		$("input[name=subtotalhidden]").val(($subSum));
	});






	$(".suco").click(function () {
		if ($(this).is(":checked")) {
			$(".sucos").show();
		}
		else
			$(".sucos").hide();
	});
	
	
	$(".vinho").click(function () {
		if ($(this).is(":checked")) {
			$(".vinhos").show();
		}
		else
			$(".vinhos").hide();
	});



	$(".cartao").click(function () {
		if ($(this).is(":checked")) {
			$(".textbox").show();
		}
		else
			$(".textbox").hide();
	});
	
	$(".retirar").click(function () {
		if ($(this).is(":checked")) {
			$(".bloco_end").hide();
			$sum -= + $freteAntigo;
			$freteAntigo=0;
			
		}
		else
			$(".bloco_end").show();
		$("#endereco1").val("");
        $("#bairro1").val("");
        $("#numero1").val("");
        $("#cep1").val("");
         $("#endereco2").val("");
        $("#bairro2").val("");
        $("#cep2").val("");
        $("#numero2").val("");
         $("#endereco3").val("");
        $("#bairro3").val("");
        $("#cep3").val("");
        $("#numero3").val("");
         $("#endereco4").val("");
        $("#bairro4").val("");
        $("#cep4").val("");
        $("#numero4").val("");
         $("#endereco5").val("");
        $("#bairro5").val("");
        $("#cep5").val("");
        $("#numero5").val("");
        $("#endereco6").val("");
        $("#bairro6").val("");
        $("#cep6").val("");
        $("#numero6").val("");
        $("#endereco7").val("");
        $("#bairro7").val("");
        $("#cep7").val("");
        $("#numero7").val("");
	});


	function pegarvalor(value) {
		if (value == "Garrafa de vinho")
			return 40
		else if (value == "Vinho em lata")
			return 20
		else if (value == "Pastinhas artesanais")
			return 15
		else if (value == "Cerveja artesanal")
			return 30
		else if (value == "Long neck de cerveja")
			return 10
		else if (value == "Brownie")
			return 15
		else if (value == "Sucos")
			return 10
		else if (value == "Caneca personalizada")
			return 50
		else if (value == "Mini arranjo de flor")
			return 25
		else if (value == "Master arranjo de flor")
			return 48
		else if (value == "Nutella")
			return 15
		else if (value == "Foto polaroid")
			return 8
		else
			return 0

	}

	function verificaespecial(value) {
		if (value == "Parma")
			return true
		else if (value == "Parmesão")
			return true
		else if (value == "Mussarela de Búfala")
			return true
		else if (value == "Camembert")
			return true
		else if (value == "Copa")
			return true
		else if (value == "Salame")
			return true
		else if (value == "Pepperoni")
			return true
		else
			return false

	}

	




	$("#form-pedido1").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			data: "required",
			nome: "required",
			hora: "required",
			telefone: "required",
			conheceu: "required",
			tipobebida: "required",
			nomeReceber: "required",
			email: {
				required: true,
				email: true
			},
			endereco: "required",
			numero: "required",
			bairro: "required",
			cidade: "required",
			estado: "required",
			'chk-boxp-queijo': "required",
			'chk-boxp-frios': "required",
			'chk-boxp-sucos': "required",
			'chk-boxp-vinhos': "required",
			'fpagamento': "required",

		},

		submitHandler: function (form) {
			$('#pedidobtn1').toggleClass(
				'uiButtonActive');
			$('#pedidobtn1').prop('disabled', true);
			$('.closemodal').prop(
				'disabled', true);
			$('.closemodal').addClass(
				'noHover')
				
				

			$('#enderecocompleto1').val(
				$("#form-pedido1 input[name=endereco]").val() + ", " +
				$("#form-pedido1 input[name=numero]").val() + ", " +
				$("#form-pedido1 input[name=complemento]").val() + ", " +
				$("#form-pedido1 input[name=bairro]").val() + ", " +
				$("#form-pedido1 input[name=cidade]").val() + ", " +
				$("#form-pedido1 input[name=estado]").val());


			$('#pedidobtn1')
				.html(
					`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando Pedido...`);

			$.ajax({
				url: 'emitirPedido',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						$(
							'#pedidobtn1')
							.html(
								`Realizar Pedido`);
						$(
							'#pedidobtn1')
							.toggleClass(
								'uiButtonActive');
						$(
							'#pedidobtn1')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.removeClass(
								'noHover')
						$(
							'#boxp')
							.modal(
								"hide");

						$(
							'#myModal')
							.modal(
								"show");
					} else {
						console.log(response)


					}
				}
			});
		}
	});


	$("#form-pedido2").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			data: "required",
			nome: "required",
			hora: "required",
			telefone: "required",
			conheceu: "required",
			nomeReceber: "required",
			endereco: "required",
			numero: "required",
			email: {
				required: true,
				email: true
			},
			bairro: "required",
			cidade: "required",
			estado: "required",
			'chk-boxm-queijo': "required",
			'chk-boxm-frios': "required",
			'fpagamento': "required",

		},

		submitHandler: function (form) {
			$('#pedidobtn2').toggleClass(
				'uiButtonActive');
			$('#pedidobtn2').prop('disabled', true);
			$('.closemodal').prop(
				'disabled', true);
			$('.closemodal').addClass(
				'noHover')


			$('#enderecocompleto2').val(
				$("#form-pedido2 input[name=endereco]").val() + ", " +
				$("#form-pedido2 input[name=numero]").val() + ", " +
				$("#form-pedido2 input[name=complemento]").val() + ", " +
				$("#form-pedido2 input[name=bairro]").val() + ", " +
				$("#form-pedido2 input[name=cidade]").val() + ", " +
				$("#form-pedido2 input[name=estado]").val());

			$('#pedidobtn2')
				.html(
					`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando Pedido...`);

			$.ajax({
				url: 'emitirPedido',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						$(
							'#pedidobtn2')
							.html(
								`Realizar Pedido`);
						$(
							'#pedidobtn2')
							.toggleClass(
								'uiButtonActive');
						$(
							'#pedidobtn2')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.removeClass(
								'noHover')
						$(
							'#boxm')
							.modal(
								"hide");

						$(
							'#myModal')
							.modal(
								"show");
					} else {
						console.log(response)


					}
				}
			});
		}
	});


	$("#form-pedido3").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			data: "required",
			nome: "required",
			hora: "required",
			telefone: "required",
			conheceu: "required",
			nomeReceber: "required",
			endereco: "required",
			email: {
				required: true,
				email: true
			},
			numero: "required",
			bairro: "required",
			cidade: "required",
			estado: "required",
			'chk-cafe-queijo': "required",
			'chk-cafe-frios': "required",
			'chk-cafe-sucos': "required",
			'chk-cafe-bolos': "required",
			'fpagamento': "required",

		},

		submitHandler: function (form) {
			$('#pedidobtn3').toggleClass(
				'uiButtonActive');
			$('#pedidobtn3').prop('disabled', true);
			$('.closemodal').prop(
				'disabled', true);
			$('.closemodal').addClass(
				'noHover')


			$('#enderecocompleto3').val(
				$("#form-pedido3 input[name=endereco]").val() + ", " +
				$("#form-pedido3 input[name=numero]").val() + ", " +
				$("#form-pedido3 input[name=complemento]").val() + ", " +
				$("#form-pedido3 input[name=bairro]").val() + ", " +
				$("#form-pedido3 input[name=cidade]").val() + ", " +
				$("#form-pedido3 input[name=estado]").val());

			$('#pedidobtn3')
				.html(
					`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando Pedido...`);

			$.ajax({
				url: 'emitirPedido',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						$(
							'#pedidobtn3')
							.html(
								`Realizar Pedido`);
						$(
							'#pedidobtn3')
							.toggleClass(
								'uiButtonActive');
						$(
							'#pedidobtn3')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.removeClass(
								'noHover')
						$(
							'#cafe')
							.modal(
								"hide");

						$(
							'#myModal')
							.modal(
								"show");
					} else {
						console.log(response)


					}
				}
			});
		}
	});

	$("#form-pedido4").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			data: "required",
			nome: "required",
			hora: "required",
			telefone: "required",
			conheceu: "required",
			nomeReceber: "required",
			endereco: "required",
			email: {
				required: true,
				email: true
			},
			numero: "required",
			bairro: "required",
			cidade: "required",
			estado: "required",
			'chk-platterg-queijo': "required",
			'chk-platterg-frios': "required",
			'fpagamento': "required",

		},

		submitHandler: function (form) {
			$('#pedidobtn4').toggleClass(
				'uiButtonActive');
			$('#pedidobtn4').prop('disabled', true);
			$('.closemodal').prop(
				'disabled', true);
			$('.closemodal').addClass(
				'noHover')


			$('#enderecocompleto4').val(
				$("#form-pedido4 input[name=endereco]").val() + ", " +
				$("#form-pedido4 input[name=numero]").val() + ", " +
				$("#form-pedido4 input[name=complemento]").val() + ", " +
				$("#form-pedido4 input[name=bairro]").val() + ", " +
				$("#form-pedido4 input[name=cidade]").val() + ", " +
				$("#form-pedido4 input[name=estado]").val());

			$('#pedidobtn4')
				.html(
					`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando Pedido...`);

			$.ajax({
				url: 'emitirPedido',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						$(
							'#pedidobtn4')
							.html(
								`Realizar Pedido`);
						$(
							'#pedidobtn4')
							.toggleClass(
								'uiButtonActive');
						$(
							'#pedidobtn4')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.removeClass(
								'noHover')
						$(
							'#platterg')
							.modal(
								"hide");

						$(
							'#myModal')
							.modal(
								"show");
					} else {
						console.log(response)


					}
				}
			});
		}
	});

	$("#form-pedido5").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			data: "required",
			nome: "required",
			hora: "required",
			telefone: "required",
			conheceu: "required",
			nomeReceber: "required",
			endereco: "required",
			numero: "required",
			email: {
				required: true,
				email: true
			},
			bairro: "required",
			cidade: "required",
			estado: "required",
			'chk-platterm-queijo': "required",
			'chk-platterm-frios': "required",
			'fpagamento': "required",

		},

		submitHandler: function (form) {
			$('#pedidobtn5').toggleClass(
				'uiButtonActive');
			$('#pedidobtn5').prop('disabled', true);
			$('.closemodal').prop(
				'disabled', true);
			$('.closemodal').addClass(
				'noHover')


			$('#enderecocompleto5').val(
				$("#form-pedido5 input[name=endereco]").val() + ", " +
				$("#form-pedido5 input[name=numero]").val() + ", " +
				$("#form-pedido5 input[name=complemento]").val() + ", " +
				$("#form-pedido5 input[name=bairro]").val() + ", " +
				$("#form-pedido5 input[name=cidade]").val() + ", " +
				$("#form-pedido5 input[name=estado]").val());

			$('#pedidobtn5')
				.html(
					`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando Pedido...`);

			$.ajax({
				url: 'emitirPedido',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						$(
							'#pedidobtn5')
							.html(
								`Realizar Pedido`);
						$(
							'#pedidobtn5')
							.toggleClass(
								'uiButtonActive');
						$(
							'#pedidobtn5')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.removeClass(
								'noHover')
						$(
							'#platterm')
							.modal(
								"hide");

						$(
							'#myModal')
							.modal(
								"show");
					} else {
						console.log(response)


					}
				}
			});
		}
	});
	
	
	$("#form-pedido6").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			data: "required",
			nome: "required",
			hora: "required",
			telefone: "required",
			conheceu: "required",
			tipobebida: "required",
			nomeReceber: "required",
			email: {
				required: true,
				email: true
			},
			endereco: "required",
			numero: "required",
			bairro: "required",
			cidade: "required",
			estado: "required",
			'chk-especial-queijo': "required",
			'chk-especial-frios': "required",
			'fpagamento': "required",

		},

		submitHandler: function (form) {
			$('#pedidobtn6').toggleClass(
				'uiButtonActive');
			$('#pedidobtn6').prop('disabled', true);
			$('.closemodal').prop(
				'disabled', true);
			$('.closemodal').addClass(
				'noHover')
				
				

			$('#enderecocompleto6').val(
				$("#form-pedido6 input[name=endereco]").val() + ", " +
				$("#form-pedido6 input[name=numero]").val() + ", " +
				$("#form-pedido6 input[name=complemento]").val() + ", " +
				$("#form-pedido6 input[name=bairro]").val() + ", " +
				$("#form-pedido6 input[name=cidade]").val() + ", " +
				$("#form-pedido6 input[name=estado]").val());


			$('#pedidobtn6')
				.html(
					`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando Pedido...`);

			$.ajax({
				url: 'emitirPedido',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						$(
							'#pedidobtn6')
							.html(
								`Realizar Pedido`);
						$(
							'#pedidobtn6')
							.toggleClass(
								'uiButtonActive');
						$(
							'#pedidobtn6')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.removeClass(
								'noHover')
						$(
							'#especial')
							.modal(
								"hide");

						$(
							'#myModal')
							.modal(
								"show");
					} else {
						console.log(response)


					}
				}
			});
		}
	});

	$("#form-pedido7").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			data: "required",
			nome: "required",
			hora: "required",
			telefone: "required",
			conheceu: "required",
			tipobebida: "required",
			nomeReceber: "required",
			email: {
				required: true,
				email: true
			},
			endereco: "required",
			numero: "required",
			bairro: "required",
			cidade: "required",
			estado: "required",
			'chk-especial-queijo': "required",
			'chk-especial-frios': "required",
			'fpagamento': "required",

		},

		submitHandler: function (form) {
			$('#pedidobtn7').toggleClass(
				'uiButtonActive');
			$('#pedidobtn7').prop('disabled', true);
			$('.closemodal').prop(
				'disabled', true);
			$('.closemodal').addClass(
				'noHover')
				
				

			$('#enderecocompleto7').val(
				$("#form-pedido7 input[name=endereco]").val() + ", " +
				$("#form-pedido7 input[name=numero]").val() + ", " +
				$("#form-pedido7 input[name=complemento]").val() + ", " +
				$("#form-pedido7 input[name=bairro]").val() + ", " +
				$("#form-pedido7 input[name=cidade]").val() + ", " +
				$("#form-pedido7 input[name=estado]").val());


			$('#pedidobtn7')
				.html(
					`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando Pedido...`);

			$.ajax({
				url: 'emitirPedido',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						$(
							'#pedidobtn7')
							.html(
								`Realizar Pedido`);
						$(
							'#pedidobtn7')
							.toggleClass(
								'uiButtonActive');
						$(
							'#pedidobtn7')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.removeClass(
								'noHover')
						$(
							'#especial2')
							.modal(
								"hide");

						$(
							'#myModal')
							.modal(
								"show");
					} else {
						console.log(response)


					}
				}
			});
		}
	});




	// //////////////////frete//////////////////
	




	var endereco;
	var numero;
	var bairro;
	var cidade;
	var data;
	var hora;
	var lat;
	var lng;

	$("#calcularFrete1").click(function () {
		$('#calcularFrete1').toggleClass('uiButtonActive');			
		$('#calcularFrete1').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);

		endereco = $("#endereco1").val()
		numero = $("#numero1").val()
		bairro = $("#bairro1").val()
		cidade = $("#cidade1").val()
		data = $("#data1").val().split("/")[2] + "-" + $("#data1").val().split("/")[1] + "-" + $("#data1").val().split("/")[0]
		hora = $("#hora1").val().split('-')[1] + ":30"
	
		geocode(platform)
	});

	$("#calcularFrete2").click(function () {
		$('#calcularFrete2').toggleClass('uiButtonActive');		
		$('#calcularFrete2').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);

		endereco = $("#endereco2").val()
		numero = $("#numero2").val()
		bairro = $("#bairro2").val()
		cidade = $("#cidade2").val()
		data = $("#data2").val().split("/")[2] + "-" + $("#data2").val().split("/")[1] + "-" + $("#data2").val().split("/")[0]
		hora = $("#hora2").val().split('-')[1] + ":30"

		geocode(platform)
	});

	$("#calcularFrete3").click(function () {
		$('#calcularFrete3').toggleClass('uiButtonActive');			
		$('#calcularFrete3').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		endereco = $("#endereco3").val()
		numero = $("#numero3").val()
		bairro = $("#bairro3").val()
		cidade = $("#cidade3").val()
		data = $("#data3").val().split("/")[2] + "-" + $("#data3").val().split("/")[1] + "-" + $("#data3").val().split("/")[0]
		hora = $("#hora3").val().split('-')[1] + ":30"

		geocode(platform)
	});

	$("#calcularFrete4").click(function () {
		$('#calcularFrete4').toggleClass('uiButtonActive');			
		$('#calcularFrete4').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		endereco = $("#endereco4").val()
		numero = $("#numero4").val()
		bairro = $("#bairro4").val()
		cidade = $("#cidade4").val()
		data = $("#data4").val().split("/")[2] + "-" + $("#data4").val().split("/")[1] + "-" + $("#data4").val().split("/")[0]
		hora = $("#hora4").val().split('-')[1] + ":30"

		geocode(platform)
	});


	$("#calcularFrete5").click(function () {
		$('#calcularFrete5').toggleClass('uiButtonActive');				
		$('#calcularFrete5').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		endereco = $("#endereco5").val()
		numero = $("#numero5").val()
		bairro = $("#bairro5").val()
		cidade = $("#cidade5").val()
		data = $("#data5").val().split("/")[2] + "-" + $("#data5").val().split("/")[1] + "-" + $("#data5").val().split("/")[0]
		hora = $("#hora5").val().split('-')[1] + ":30"

		geocode(platform)
	});
	

	$("#calcularFrete6").click(function () {
		$('#calcularFrete6').toggleClass('uiButtonActive');				
		$('#calcularFrete6').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		endereco = $("#endereco6").val()
		numero = $("#numero6").val()
		bairro = $("#bairro6").val()
		cidade = $("#cidade6").val()
		data = $("#data6").val().split("/")[2] + "-" + $("#data6").val().split("/")[1] + "-" + $("#data6").val().split("/")[0]
		hora = $("#hora6").val().split('-')[1] + ":30"

		geocode(platform)
	});
	

	$("#calcularFrete7").click(function () {
		$('#calcularFrete7').toggleClass('uiButtonActive');				
		$('#calcularFrete7').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		endereco = $("#endereco7").val()
		numero = $("#numero7").val()
		bairro = $("#bairro7").val()
		cidade = $("#cidade7").val()
		data = $("#data7").val().split("/")[2] + "-" + $("#data7").val().split("/")[1] + "-" + $("#data7").val().split("/")[0]
		hora = $("#hora7").val().split('-')[1] + ":30"

		geocode(platform)
	});

$('.f1').on('change', function () { 
	
$('#calcularFrete1').toggleClass('uiButtonActive');			
		$('#calcularFrete1').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		
		endereco = $("#endereco1").val()
		numero = $("#numero1").val()
		bairro = $("#bairro1").val()
		cidade = $("#cidade1").val()
		data = $("#data1").val().split("/")[2] + "-" + $("#data1").val().split("/")[1] + "-" + $("#data1").val().split("/")[0]
		hora = $("#hora1").val().split('-')[1] + ":30"
	
		geocode(platform)
 });
 
 $('.f2').on('change', function () { 
$('#calcularFrete2').toggleClass('uiButtonActive');		
		$('#calcularFrete2').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);

		endereco = $("#endereco2").val()
		numero = $("#numero2").val()
		bairro = $("#bairro2").val()
		cidade = $("#cidade2").val()
		data = $("#data2").val().split("/")[2] + "-" + $("#data2").val().split("/")[1] + "-" + $("#data2").val().split("/")[0]
		hora = $("#hora2").val().split('-')[1] + ":30"

		geocode(platform)
 });
 
 
 $('.f3').on('change', function () { 
$('#calcularFrete3').toggleClass('uiButtonActive');			
		$('#calcularFrete3').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		endereco = $("#endereco3").val()
		numero = $("#numero3").val()
		bairro = $("#bairro3").val()
		cidade = $("#cidade3").val()
		data = $("#data3").val().split("/")[2] + "-" + $("#data3").val().split("/")[1] + "-" + $("#data3").val().split("/")[0]
		hora = $("#hora3").val().split('-')[1] + ":30"

		geocode(platform)
 });
 
 
 $('.f4').on('change', function () { 
$('#calcularFrete4').toggleClass('uiButtonActive');			
		$('#calcularFrete4').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		endereco = $("#endereco4").val()
		numero = $("#numero4").val()
		bairro = $("#bairro4").val()
		cidade = $("#cidade4").val()
		data = $("#data4").val().split("/")[2] + "-" + $("#data4").val().split("/")[1] + "-" + $("#data4").val().split("/")[0]
		hora = $("#hora4").val().split('-')[1] + ":30"

		geocode(platform)
 });
 
 
  $('.f5').on('change', function () { 
$('#calcularFrete5').toggleClass('uiButtonActive');				
		$('#calcularFrete5').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		endereco = $("#endereco5").val()
		numero = $("#numero5").val()
		bairro = $("#bairro5").val()
		cidade = $("#cidade5").val()
		data = $("#data5").val().split("/")[2] + "-" + $("#data5").val().split("/")[1] + "-" + $("#data5").val().split("/")[0]
		hora = $("#hora5").val().split('-')[1] + ":30"

		geocode(platform)
 });
  
  
  $('.f6').on('change', function () { 
	  $('#calcularFrete6').toggleClass('uiButtonActive');				
	  		$('#calcularFrete6').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
	  		endereco = $("#endereco6").val()
	  		numero = $("#numero6").val()
	  		bairro = $("#bairro6").val()
	  		cidade = $("#cidade6").val()
	  		data = $("#data6").val().split("/")[2] + "-" + $("#data6").val().split("/")[1] + "-" + $("#data6").val().split("/")[0]
	  		hora = $("#hora6").val().split('-')[1] + ":30"
	  		geocode(platform)
	   });
  
  $('.f7').on('change', function () { 
	  $('#calcularFrete7').toggleClass('uiButtonActive');				
	  		$('#calcularFrete7').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
	  		endereco = $("#endereco7").val()
	  		numero = $("#numero7").val()
	  		bairro = $("#bairro7").val()
	  		cidade = $("#cidade7").val()
	  		data = $("#data7").val().split("/")[2] + "-" + $("#data7").val().split("/")[1] + "-" + $("#data7").val().split("/")[0]
	  		hora = $("#hora7").val().split('-')[1] + ":30"
	  		geocode(platform)
	   });

	var $freteAntigo = 0;


	var platform = new H.service.Platform({
		'apikey': '_5sbZz1Hl8XUJ4EpMlQ9lTDq08j2wXhwwoZNkhrCZb4'
	});



	function geocode(platform) {
		var geocoder = platform.getSearchService(),
			geocodingParameters = {
				q: endereco + ", " + numero + ", " + bairro + ", " + cidade
			};

		geocoder.geocode(
			geocodingParameters,
			onSuccess,
			onError
		);
	}

	function onSuccess(result) {
		var locations = result.items[0];
		lat = result.items[0].position.lat
		lng = result.items[0].position.lng


		var routingService = platform.getRoutingService()
		const params = {
			mode: "fastest;car;traffic:enabled",
			waypoint0: "-22.92432,-43.23895",
			waypoint1: lat + "," + lng,
			departure: data + 'T' + hora,
			representation: "display",
			routeAttributes: "summary"
		};

		routingService.calculateRoute(params, success => {
console.log(success.response.route[0].summary)

			calculaFreteTaxi(success.response.route[0].summary.baseTime, success.response.route[0].summary.trafficTime - success.response.route[0].summary.baseTime, success.response.route[0].summary.distance)

			const routeLineString = new H.geo.LineString();
			success.response.route[0].shape.forEach(point => {
				const [lat, lng] = point.split(",");
				routeLineString.pushPoint({
					lat: lat,
					lng: lng
				});
			});

		}, error => {
			console.log(error);
			$('#calcularFrete1').html(`Calcular Frete`);
		$('#calcularFrete1').toggleClass('uiButtonActive');
		
		$('#calcularFrete2').html(`Calcular Frete`);
		$('#calcularFrete2').toggleClass('uiButtonActive');
		
		$('#calcularFrete3').html(`Calcular Frete`);
		$('#calcularFrete3').toggleClass('uiButtonActive');
		
		$('#calcularFrete4').html(`Calcular Frete`);
		$('#calcularFrete4').toggleClass('uiButtonActive');
		
		$('#calcularFrete5').html(`Calcular Frete`);
		$('#calcularFrete5').toggleClass('uiButtonActive');
		
		$('#calcularFrete6').html(`Calcular Frete`);
		$('#calcularFrete6').toggleClass('uiButtonActive');
		
		$('#calcularFrete7').html(`Calcular Frete`);
		$('#calcularFrete7').toggleClass('uiButtonActive');
		
		});

	}
	function onError(error) {
		alert('Can\'t reach the remote server');
		$('#calcularFrete1').html(`Calcular Frete`);
		$('#calcularFrete1').toggleClass('uiButtonActive');
		
		$('#calcularFrete2').html(`Calcular Frete`);
		$('#calcularFrete2').toggleClass('uiButtonActive');
		
		$('#calcularFrete3').html(`Calcular Frete`);
		$('#calcularFrete3').toggleClass('uiButtonActive');
	
		$('#calcularFrete4').html(`Calcular Frete`);
		$('#calcularFrete4').toggleClass('uiButtonActive');
		
		$('#calcularFrete5').html(`Calcular Frete`);
		$('#calcularFrete5').toggleClass('uiButtonActive');
		
		$('#calcularFrete6').html(`Calcular Frete`);
		$('#calcularFrete6').toggleClass('uiButtonActive');
		
		$('#calcularFrete7').html(`Calcular Frete`);
		$('#calcularFrete7').toggleClass('uiButtonActive');
		
		
	}

	function calculaFreteTaxi(tviagem, tparado, distancia) {
		var total = 5.2;
		total += + (2.65 * (distancia / 1000))
		total += + ((33, 39 / 3600) * tparado)


		$("input[name=frete]").attr("placeholder", "R$" +Math.ceil((total * 0.6))+",00");
		if ($freteAntigo != 0) {
			$sum -= +$freteAntigo;
			$freteAntigo = 0;
		}
		$sum += + Math.ceil((total * 0.6));
		$freteAntigo = Math.ceil((total * 0.6));
		if($freteAntigo==0){
			$('#pedidobtn1').prop('disabled', true);
			$('#pedidobtn2').prop('disabled', true);
			$('#pedidobtn3').prop('disabled', true);
			$('#pedidobtn4').prop('disabled', true);
			$('#pedidobtn5').prop('disabled', true);
			$('#pedidobtn6').prop('disabled', true);
			$('#pedidobtn7').prop('disabled', true);
			$("input[name=frete]").attr("placeholder", "Verifique as informações digitadas e tente novamente");
		}else{
				$('#pedidobtn1').prop('disabled', false);
			$('#pedidobtn2').prop('disabled', false);
			$('#pedidobtn3').prop('disabled', false);
			$('#pedidobtn4').prop('disabled', false);
			$('#pedidobtn5').prop('disabled', false);
			$('#pedidobtn6').prop('disabled', false);
			$('#pedidobtn7').prop('disabled', false);
		}
		
		
		
		$("input[name=fretehidden]").val(Math.ceil((total * 0.6)));
		
		
		
			$("input[name=total]").attr("placeholder", "R$" + ($sum) + ",00");			
			$("input[name=valor]").val($sum);
		
		
		$("input[name=subtotal]").attr("placeholder", "R$" + ($sum-$freteAntigo)  + ",00");
		
	


		$('#calcularFrete1').html(`Calcular Frete`);
		$('#calcularFrete1').toggleClass('uiButtonActive');
		
		$('#calcularFrete2').html(`Calcular Frete`);
		$('#calcularFrete2').toggleClass('uiButtonActive');
		
		$('#calcularFrete3').html(`Calcular Frete`);
		$('#calcularFrete3').toggleClass('uiButtonActive');
		
		$('#calcularFrete4').html(`Calcular Frete`);
		$('#calcularFrete4').toggleClass('uiButtonActive');
		
		$('#calcularFrete5').html(`Calcular Frete`);
		$('#calcularFrete5').toggleClass('uiButtonActive');
		
		$('#calcularFrete6').html(`Calcular Frete`);
		$('#calcularFrete6').toggleClass('uiButtonActive');	
		
		$('#calcularFrete7').html(`Calcular Frete`);
		$('#calcularFrete7').toggleClass('uiButtonActive');
		
		

	};


	// /////////////////fim//////////////////
	
	$("#aceitarPedido").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			link: "required"
			

		},

		submitHandler: function (form) {
			
			$.ajax({
				url: 'aceitarPedido',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						 $("#aceitarPedido").hide();
						
						
					} else {
						console.log(response)


					}
				}
			});
		}
	});
	
	
	$("#form-pedido1 input[name=cupom]").on('change', function () { 
		
		var email = $("#form-pedido1 input[name=email]").val()
		var cupom = $("#form-pedido1 input[name=cupom]").val()
		
		if(cupom.length===14){
		
		
		$.ajax({
			  url: "validarCupom",
			  type: "get", // send it through get method
			  data: { 
			    email: email, 
			    cupom: cupom
			  },
			  success: function(response) {
				  if(response=="true"){
					  // alert("foi")
					  $sum -= + 50;
					  
					  $("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
					  $( ".cupom" ).prop( "disabled", true );
						$("input[name=valor]").val($sum);
				  }else{
					  // alert("nao foi")
				  }
			  },
			  error: function(xhr) {
				  // alert("error")
			  }
			});
		}
	
	});
	
$("#form-pedido2 input[name=cupom]").on('change', function () { 
		
		var email = $("#form-pedido2 input[name=email]").val()
		var cupom = $("#form-pedido2 input[name=cupom]").val()
		
		if(cupom.length===14){
		
		
		$.ajax({
			  url: "validarCupom",
			  type: "get", // send it through get method
			  data: { 
			    email: email, 
			    cupom: cupom
			  },
			  success: function(response) {
				  if(response=="true"){
					  // alert("foi")
					  $sum -= + 50;
					  
					  $("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
					  $( ".cupom" ).prop( "disabled", true );
						$("input[name=valor]").val($sum);
				  }else{
					  // alert("nao foi")
				  }
			  },
			  error: function(xhr) {
				  // alert("error")
			  }
			});
		}
	
	});
	


$("#form-pedido3 input[name=cupom]").on('change', function () { 
	
	var email = $("#form-pedido3 input[name=email]").val()
	var cupom = $("#form-pedido3 input[name=cupom]").val()
	
	if(cupom.length===14){
	
	
	$.ajax({
		  url: "validarCupom",
		  type: "get", // send it through get method
		  data: { 
		    email: email, 
		    cupom: cupom
		  },
		  success: function(response) {
			  if(response=="true"){
				  // alert("foi")
				  $sum -= + 50;
				  
				  $("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
				  $( ".cupom" ).prop( "disabled", true );
					$("input[name=valor]").val($sum);
			  }else{
				  // alert("nao foi")
			  }
		  },
		  error: function(xhr) {
			  // alert("error")
		  }
		});
	}

});


$("#form-pedido4 input[name=cupom]").on('change', function () { 
	
	var email = $("#form-pedido4 input[name=email]").val()
	var cupom = $("#form-pedido4 input[name=cupom]").val()
	
	if(cupom.length===14){
	
	
	$.ajax({
		  url: "validarCupom",
		  type: "get", // send it through get method
		  data: { 
		    email: email, 
		    cupom: cupom
		  },
		  success: function(response) {
			  if(response=="true"){
				  // alert("foi")
				  $sum -= + 50;
				  
				  $("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
				  $( ".cupom" ).prop( "disabled", true );
					$("input[name=valor]").val($sum);
			  }else{
				 // alert("nao foi")
			  }
		  },
		  error: function(xhr) {
			  // alert("error")
		  }
		});
	}

});


$("#form-pedido5 input[name=cupom]").on('change', function () { 
	
	var email = $("#form-pedido5 input[name=email]").val()
	var cupom = $("#form-pedido5 input[name=cupom]").val()
	
	if(cupom.length===14){
	
	
	$.ajax({
		  url: "validarCupom",
		  type: "get", // send it through get method
		  data: { 
		    email: email, 
		    cupom: cupom
		  },
		  success: function(response) {
			  if(response=="true"){
				  // alert("foi")
				  $sum -= + 50;
				  
				  $("input[name=total]").attr("placeholder", "R$" + $sum + ",00");
				  $( ".cupom" ).prop( "disabled", true );
					$("input[name=valor]").val($sum);
			  }else{
				  // alert("nao foi")
			  }
		  },
		  error: function(xhr) {
			  // alert("error")
		  }
		});
	}

});




	$(".albery-container").albery({
			speed: 500,
			imgWidth: 600,
		});
	/*
	
	  var $promo=0;
	    $(window).on('load', function() {
	    	setTimeout(function(){ $('#myModal1').modal('show'); }, 1000);
	        
	        $promo=0;
	    });
	    
	    
	    $("#promo").click(function () {
	    	$('#myModal1').modal('hide');
	    	$promo=1;
	    	$cupom=1;
		});
	  	
	    
	    
	    $('#myModal1').on('hidden.bs.modal', function () {
	    	  // Load up a new modal...
	    	  if($promo===1){
	    		  
	    		  $("input[name=cupom]").attr("placeholder", "PETITPOAFAZ1");
	    		  $( "#cupom" ).prop( "disabled", true );
				  $('#especial').modal('show')
	    	  }
	    	  
	    	})	*/

	  
	
	    	
	
	
})(jQuery);

