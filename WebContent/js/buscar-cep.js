$(document).ready(function () {    	
    	function limpa_formulario_cep() {
            // Limpa valores do formulário de cep.
            $("#endereco1").val("");
            $("#bairro1").val("");
          
             $("#endereco2").val("");
            $("#bairro2").val("");
          
             $("#endereco3").val("");
            $("#bairro3").val("");
           
             $("#endereco4").val("");
            $("#bairro4").val("");
          
             $("#endereco5").val("");
            $("#bairro5").val("");
            
            $("#endereco6").val("");
            $("#bairro6").val("");
            
            $("#endereco7").val("");
            $("#bairro7").val("");
          
        }
        
        //Quando o campo cep perde o foco.
        $("#cep1").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco1").val("...")
                    $("#bairro1").val("...")

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco1").val(dados.logradouro);
                            $("#bairro1").val(dados.bairro);
                            var evt = new Event('change');
  							document.getElementById("bairro1").dispatchEvent(evt);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulario_cep();
                            mostrarAlerta('Aviso','CEP não encontrado.');
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulario_cep();
                    mostrarAlerta('Error','Formato de CEP inválido.');
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulario_cep();
            }
        });
        
        
        //Quando o campo cep perde o foco.
        $("#cep2").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco2").val("...")
                    $("#bairro2").val("...")

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco2").val(dados.logradouro);
                            $("#bairro2").val(dados.bairro);
                            var evt = new Event('change');
  							document.getElementById("bairro2").dispatchEvent(evt);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulario_cep();
                            mostrarAlerta('Aviso','CEP não encontrado.');
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulario_cep();
                    mostrarAlerta('Error','Formato de CEP inválido.');
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulario_cep();
            }
        });
        
        
           //Quando o campo cep perde o foco.
        $("#cep3").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco3").val("...")
                    $("#bairro3").val("...")

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco3").val(dados.logradouro);
                            $("#bairro3").val(dados.bairro);
                            var evt = new Event('change');
  							document.getElementById("bairro3").dispatchEvent(evt);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulario_cep();
                            mostrarAlerta('Aviso','CEP não encontrado.');
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulario_cep();
                    mostrarAlerta('Error','Formato de CEP inválido.');
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulario_cep();
            }
        });
        
           //Quando o campo cep perde o foco.
        $("#cep4").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco4").val("...")
                    $("#bairro4").val("...")

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco4").val(dados.logradouro);
                            $("#bairro4").val(dados.bairro);
                            var evt = new Event('change');
  							document.getElementById("bairro4").dispatchEvent(evt);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulario_cep();
                            mostrarAlerta('Aviso','CEP não encontrado.');
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulario_cep();
                    mostrarAlerta('Error','Formato de CEP inválido.');
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulario_cep();
            }
        });
        
           //Quando o campo cep perde o foco.
        $("#cep5").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco5").val("...")
                    $("#bairro5").val("...")

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco5").val(dados.logradouro);
                            $("#bairro5").val(dados.bairro);
                            var evt = new Event('change');
  							document.getElementById("bairro5").dispatchEvent(evt);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulario_cep();
                            mostrarAlerta('Aviso','CEP não encontrado.');
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulario_cep();
                    mostrarAlerta('Error','Formato de CEP inválido.');
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulario_cep();
            }
        });
        
        
        //Quando o campo cep perde o foco.
        $("#cep6").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco6").val("...")
                    $("#bairro6").val("...")

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco6").val(dados.logradouro);
                            $("#bairro6").val(dados.bairro);
                            var evt = new Event('change');
  							document.getElementById("bairro6").dispatchEvent(evt);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulario_cep();
                            mostrarAlerta('Aviso','CEP não encontrado.');
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulario_cep();
                    mostrarAlerta('Error','Formato de CEP inválido.');
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulario_cep();
            }
        });
        
        //Quando o campo cep perde o foco.
        $("#cep7").blur(function() {

            //Nova variável "cep" somente com dígitos.
            var cep = $(this).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#endereco7").val("...")
                    $("#bairro7").val("...")

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#endereco7").val(dados.logradouro);
                            $("#bairro7").val(dados.bairro);
                            var evt = new Event('change');
  							document.getElementById("bairro6").dispatchEvent(evt);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulario_cep();
                            mostrarAlerta('Aviso','CEP não encontrado.');
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulario_cep();
                    mostrarAlerta('Error','Formato de CEP inválido.');
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulario_cep();
            }
        });
});