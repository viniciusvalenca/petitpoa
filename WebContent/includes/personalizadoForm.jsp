<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Petit Poá - Pedido personalizado</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet">
<!--===============================================================================================-->
<link rel="icon" type="image/png" href="images/icons/favicon.png" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->

<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/daterangepicker/daterangepicker.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
	integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
	crossorigin="anonymous" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css"
	integrity="sha512-rxThY3LYIfYsVCWPCW9dB0k+e3RZB39f23ylUYTEuZMDrN/vRqLdaCBo/FbvVT6uC2r0ObfPzotsfKF9Qc5W5g=="
	crossorigin="anonymous" />
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">

<!--===============================================================================================-->
<link rel="stylesheet" type="text/css"
	href="vendor/lightbox2/css/lightbox.min.css">
<!--===============================================================================================-->
<link rel="stylesheet" href="css/bootstrap-steps.min.css" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/tiny-slider.css">
<!--[if (lt IE 9)]><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.3/min/tiny-slider.helper.ie8.js"></script><![endif]-->
<link rel="stylesheet" type="text/css" href="css/util.css">
<link rel="stylesheet" type="text/css" href="css/main.css">

<!--===============================================================================================-->
</head>

<body class="animsition">

	

<form id="form-especial" role="form" action="emitirEspecial" method="post">
	
	
		
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel">
						<span class="tit2 t-center"> <input type="text" name="produtoE" id="produtoE" value="" placeholder="Nome do produto"> </span>
					</h5>
					
				</div>
				<div class="modal-body">

					<div class="row">


						

						<div class="col-lg-10 p-b-30 mx-auto">
							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Dados:</h3>							
							<input type="hidden" name="enderecocompletoE"
								id="enderecocompletoE" value="">  <input type="hidden"
								name="fretehiddenE" value="0">

							<div class="row">

								<div class="col-md-6">

									<div class="form-group">
										<div class=' date' id="datetimepicker1">
											<span class="txt9">Data da entrega* <span
												style="color: red;">(Obrigatório 2 dias de
													antecedência para realizar o pedido. Consulte
													disponibilidade para prazos menores.)</span>
												<div
													class="wrap-inputdate pos-relative txt10 size12 bo2 bo-rad-10 m-t-3 m-b-23">

													<!-- this is where the magic happens -->
													<input type="text" class=" bo-rad-10 sizefull txt10 p-l-20 fE"
														name="dataE" id="dataE" placeholder="DD/MM/YYYY" required />
													<i
														class="btn-calendar fa input-group-addon fa-calendar ab-r-m hov-pointer m-r-18"
														aria-hidden="true"></i>

												</div>
										</div>
									</div>
								</div>







								<div class="col-md-6">
									<!-- Time -->
									<span class="txt9">Hora da entrega<br> (Consultar
										disponibilidade)*
									</span>

									<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<!-- Select2 -->
										<select class="selection-1 fE" name="horaE" id="horaE">
											<option selected>Selecione um horário</option>
											<option value="07:00-08:00">07:00-08:00</option>
											<option value="08:00-09:00">08:00-09:00</option>
											<option value="09:00-10:00">09:00-10:00</option>
											<option value="15:00-16:00">15:00-16:00</option>
											<option value="16:00-17:00">16:00-17:00</option>
											<option value="17:00-18:00">17:00-18:00</option>
											<option value="18:00-19:00">18:00-19:00</option>
											<option value="19:00-20:00">19:00-20:00</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">


								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Qual o seu nome?*</span>

									<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="nomeE" placeholder="Nome">
									</div>
								</div>

								<div class="col-md-12">
									<!-- Email -->
									<span class="txt9">Qual o seu e-mail?*</span>

									<div class="wrap-inputemail size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="emailE" placeholder="Email">
									</div>
								</div>
							</div>
							<div class="row">

								<div class="col-md-6">
									<!-- Phone -->
									<span class="txt9">Qual o seu telefone?*</span>

									<div class="wrap-inputphone size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="tel"
											name="telefoneE" placeholder="Telefone">
									</div>
								</div>
								<div class="col-md-6">
									<!-- Time -->
									<span class="txt9">Me conta! De onde nos conheceu?*</span>

									<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<!-- Select2 -->
										<select class="selection-1" name="conheceuE">
											<option selected>Selecione uma opção</option>
											<option value="Instagram">Instagram</option>
											<option value="Indicação">Indicação</option>
											<option value="Recebi uma cesta de presente">Recebi
												uma cesta de presente</option>
										</select>
									</div>
								</div>

							</div>

							<div class="row">

								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Qual o nome de quem irá receber?*</span>

									<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="nomeReceberE" placeholder="Nome">
									</div>
								</div>
							</div>
							<div class="row">
								<br>
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Retirar na tijuca?:</span> <input
										type="checkbox" class="retirar" name="retirarE" value="sim">
									Sim <br> <br>


								</div>



							</div>


							<div class="bloco_end">

								<div class="row">

									<div class="col-md-4">
										<span class="txt9">CEP:</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">

											<input type="tel" id="cepE" name="cepE"
												class="cep form-control bo-rad-10 sizefull txt10 p-l-20"
												placeholder="Entre com o CEP"> <span
												class="input-group-btn">
												<button class="btn btn-default" id="bcep" name="bcep"
													type="button">
													<span class="fa fa-search" aria-hidden="true"></span>
												</button>
											</span>
										</div>
									</div>



									<div class="col-md-8">
										<span class="txt9">Endere&ccedil;o*</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input id="enderecoE" name="enderecoE" type="text"
												class="form-control bo-rad-10 sizefull txt10 p-l-20 fE">
										</div>
									</div>



								</div>


								<div class="row">


									<div class="col-lg-2 col-xs-2">
										<span class="txt9">N&uacute;mero*</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input id="numeroE" name="numeroE" type="text"
												class="form-control bo-rad-10 sizefull txt10 p-l-20 fE">
										</div>
									</div>

									<div class="col-lg-3 col-xs-6">
										<span class="txt9">Complemento</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input id="complementoE" name="complementoE" type="text"
												class="form-control bo-rad-10 sizefull txt10 p-l-20">
										</div>
									</div>

									<div class="col-lg-3 col-xs-4">
										<span class="txt9">Bairro*</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input id="bairroE" name="bairroE" type="text"
												class="form-control bo-rad-10 sizefull txt10 p-l-20 f1">
										</div>
									</div>

									<div class="col-lg-3 col-xs-4">
										<span class="txt9">Cidade</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input name="cidadeE" id="cidadeE" type="text"
												value="Rio de Janeiro"
												class="form-control bo-rad-10 sizefull txt10 p-l-20">
										</div>
									</div>

									<div class="col-lg-1 col-xs-1 col-xs-2">
										<span class="txt9">Estado</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input name="estadoE" id="estadoE" type="text" value="RJ"
												class="form-control bo-rad-10 sizefull txt10 p-l-20">
										</div>
									</div>

								</div>
								<div class="row">
									<div class="col-md-12">
										<button type="button"
											class="btn3 flex-c-m size13 txt11 trans-0-4"
											id="calcularFreteE">Calcular frete</button>
									</div>
									<div class="col-md-12">
										<span class="txt9">Frete:</span><input class="form-control"
											type="text" name="freteE" placeholder="R$0,00" disabled>
									</div>

								</div>

								<br>
							</div>


							<div class="row">
								<br>
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Incluir cartão?:</span> <input
										type="checkbox" class="cartao" name="cartaoE" value="sim">
									Sim <br> <br>


								</div>



							</div>

							<div class="row textbox" id="Comments" style="display: none;">
								<div class="col-md-12">

									<div>
										<textarea class="form-control"
											id="exampleFormControlTextarea1" name="mensagemcartaoE"
											rows="3" placeholder="Digite seu texto para o cartão aqui."></textarea>
										<br> <br>
									</div>


								</div>
							</div>

							<div class="row">
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Caso seja um presente, é de
										aniversário?:</span><br> <input type="checkbox" class="presente"
										name="presenteE" id="Sim" value="sim"> Sim <input
										type="checkbox" class="presente" name="presenteE" id="Não"
										value="não"> Não <br> <br>


								</div>



							</div>


							<h3 class="tit-mainmenu tit10 p-b-25 t-center">
								Escolha <b>queijo</b> do grupo especial ou grupo básico:*
							</h3>



							<div class="row">
								<div class="col-md-6">

									<!-- Default panel contents -->
									<div class="subtext">Básicos</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Gouda" >Gouda</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Provolone" >Provolone</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Brie" >Brie</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Muçarela">Muçarela</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Gorgonzola" >Gorgonzola</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Queijo Minas" >Queijo Minas</label>
									</div>



								</div>

								<div class="col-md-6">

									<!-- Default panel contents -->
									<div class="subtext">Especiais</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Parmesão">Parmesão</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Muçarela de Búfala" >Muçarela
											de Búfala</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-queijo" type="checkbox"
											value="Camembert" >Camembert</label>
									</div>




								</div>

							</div>

							<br> <br>


							<h3 class="tit-mainmenu tit10 p-b-25 t-center">
								Escolha  <b>frio</b> do grupo especial ou grupo básico:*
							</h3>



							<div class="row">
								<div class="col-md-6">

									<!-- Default panel contents -->
									<div class="subtext">Básicos</div>
									<div class="checkbox">
										<label><input name="chk-custom-frios" type="checkbox"
											value="Presunto" >Presunto</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-frios" type="checkbox"
											value="Blanquet" >Blanquet</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-frios" type="checkbox"
											value="Peito de peru" >Peito de peru</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-frios" type="checkbox"
											value="Queijo bola" >Queijo bola</label>
									</div>




								</div>

								<div class="col-md-6" id="frios">

									<!-- Default panel contents -->
									<div class="subtext">Especiais</div>
									<div class="checkbox ">
										<label><input name="chk-custom-frios" type="checkbox"
											value="Parma" >Parma</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-frios" type="checkbox"
											value="Copa" >Copa</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-frios" type="checkbox"
											value="Salame" >Salame</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-frios" type="checkbox"
											value="Pepperoni" >Pepperoni</label>
									</div>




								</div>

							</div>
							

<br><br>


	<h3 class="tit-mainmenu tit10 p-b-25 t-center">Sucos:*</h3>



							<div class="row">
								<div class="col-md-6">

									<!-- Default panel contents -->
									
									<div class="checkbox">
										<label><input name="chk-custom-sucos" type="checkbox" value="Tangerina" >Tangerina</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-sucos" type="checkbox" value="Laranja" >Laranja</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-sucos" type="checkbox" value="Uva integral" >Uva integral</label>
									</div>
									


								</div>

								<div class="col-md-6">

									<!-- Default panel contents -->
								
									<div class="checkbox ">
										<label><input name="chk-custom-sucos" type="checkbox" value="Mamão com laranja" >Mamão com laranja</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-sucos" type="checkbox" value="Açaí" >Açaí</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-sucos" type="checkbox" value="Suco verde" >Suco verde</label>
									</div>
									




								</div>

							</div>

<br><br>


	<h3 class="tit-mainmenu tit10 p-b-25 t-center">Bolos:*</h3>



							<div class="row">
								<div class="col-md-6">

									<!-- Default panel contents -->
									
									<div class="checkbox">
										<label><input name="chk-custom-bolos" type="checkbox" value="Fubá cremoso" >Fubá cremoso</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-bolos" type="checkbox" value="Laranja" >Laranja</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-bolos" type="checkbox" value="Cuca de banana" >Cuca de banana</label>
									</div>
									


								</div>

								<div class="col-md-6">

									<!-- Default panel contents -->
								
									<div class="checkbox ">
										<label><input name="chk-custom-bolos" type="checkbox" value="Chocolate" >Chocolate</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-bolos" type="checkbox" value="Mesclado" >Mesclado</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-bolos" type="checkbox" value="Cenoura com chocolate" >Cenoura com chocolate</label>
									</div>
									




								</div>

							</div>
							
							
							<br> <br>
							
							

							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Escolha seu
								drink:*</h3>



							<div class="row">
								<div class="col-md-6">

									<!-- Default panel contents -->
									<div class="checkbox ">
										<label><input name="chk-custom-drink" type="checkbox"
											value="Mamão com laranja" >Vinho branco
											750ml</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-drink" type="checkbox"
											value="Açaí" >Vinho rose 750ml</label>
									</div>

									<div class="checkbox ">
										<label><input name="chk-custom-drink" type="checkbox"
											value="Vinho tinto 750ml" >Vinho tinto
											750ml</label>
									</div>



								</div>

								<div class="col-md-6">

									<!-- Default panel contents -->




									<div class="checkbox">
										<label><input name="chk-custom-drink" type="checkbox"
											value="Gin tanqueray 750ml" >Gin tanqueray
											750ml</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-drink" type="checkbox"
											value="Chandon 750ml" >Chandon 750ml</label>
									</div>



								</div>

							</div>
							
							
							<br> <br>
							
							
							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Adicionais:</h3>



							<div class="row">
								<div class="col-md-12" id="adicionais">

									<!-- Default panel contents -->

									<div class="checkbox">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Garrafa de vinho" class="vinho">Garrafa
											de vinho (tinto,branco, rose) - R$40,00</label>
									</div>
									<div class="row vinhos" style="display: none;">

										<div class="col-md-12">
											<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
												<select class="selection-1" name="tipovinho">
													<option selected>Selecione um sabor</option>
													<option value="Tinto">Tinto</option>
													<option value="Branco">Branco</option>
													<option value="Rose">Rose</option>
												</select>

											</div>
										</div>



									</div>
									<div class="checkbox">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Vinho em lata">Vinho em lata
											269ml - R$20,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Pastinhas artesanais">Pastinhas
											artesanais - R$15,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Cerveja artesanal">Cerveja
											artesanal - R$30,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Long neck de cerveja">Long
											neck de cerveja - R$10,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Brownie">Brownie - R$15,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Caneca personalizada">Caneca
											personalizada - R$50,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Mini arranjo de flor">Mini
											arranjo de flor - R$25,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Master arranjo de flor">Master
											arranjo de flor - R$48,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Nutella">Nutella 140g -
											R$15,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-custom-adicionais"
											type="checkbox" value="Sucos" class="suco">Sucos
											300ml - R$10,00</label>
									</div>





									<div class="row sucos" style="display: none;">

										<div class="col-md-12">
											<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
												<select class="selection-1" name="saborsuco">
													<option selected>Selecione um sabor</option>
													<option value="Tangerina">Tangerina</option>
													<option value="Laranja">Laranja</option>
													<option value="Uva integral">Uva integral</option>
													<option value="Mamão com laranja">Mamão com
														laranja</option>
													<option value="Açaí">Açaí</option>
													<option value="Suco verde">Suco verde</option>
												</select>

											</div>
										</div>

									</div>
									
									<div class="checkbox">
										<label><input name="chk-custom-adicionais"
											type="checkbox" id="custom-add1"><input type="text" placeholder="Nome do item" id="custom-add1-text"></label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-adicionais"
											type="checkbox" id="custom-add2"><input type="text" placeholder="Nome do item" id="custom-add2-text"></label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-adicionais"
											type="checkbox" id="custom-add3"><input type="text" placeholder="Nome do item" id="custom-add3-text"></label>
									</div>
									<div class="checkbox">
										<label><input name="chk-custom-adicionais"
											type="checkbox" id="custom-add4"><input type="text" placeholder="Nome do item" id="custom-add4-text"></label>
									</div>
									

								</div>

							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<span class="txt9">Possui restrições, preferências ou
										alguma observação?:</span> <br> <br>
									<div>
										<textarea class="form-control" id="obs" name="obsE" rows="3" placeholder="Digite seu texto aqui."></textarea>
										<br> <br>
									</div>


								</div>

								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Escolha a forma de pagamento:</span><br>
									<div class="pair col-md-6">
										<input type="checkbox" checked="true" class="fpagamento"
											name="fpagamentoE" id="dinheiro" value="Transferência/Pix">
										Transferência/Pix
									</div>
									<div class="pair col-md-6">
										<input type="checkbox" class="fpagamento" name="fpagamentoE"
											id="ame" value="Cartão de crédito"> Cartão de crédito
										<br /> <label for="fpagamento"><small>Acréscimo
												de 5%</small></label> <br> <br>

									</div>
								</div>

							</div>




							<span class="txt9">Total:</span><input class="form-control"
								type="text" name="valorE" placeholder="R$00,00" >
						</div>
					</div>
					<div class="modal-footer">
					<button id="resetE" type="reset"
							class="btn3 flex-c-m size13 txt11 trans-0-4">Resetar
							</button>
					
					
						<button id="pedidobtnE" type="submit"
							class="btn3 flex-c-m size13 txt11 trans-0-4">Realizar
							Pedido</button>

					</div>
				</div>
			</div>
</form>
	

	</section>
	
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Carol da Petit Poá</h4>
				</div>
				<div class="modal-body">
					<p id="error">PRONTO! Seu pedido foi enviado e você receberá um email confirmando o seu pedido.Ele será criado de
						forma exclusiva e com muito carinho :)</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Footer -->
	<footer class="bg1">


		<div class="end-footer bg2">
			<div class="container">
				<div class="flex-sb-m flex-w p-t-22 p-b-22">
					<div class="p-t-5 p-b-5">
						<a href="https://www.instagram.com/carol_da_petitpoa/"
							class="fs-15 c-white"><i class="fa fa-instagram"
							aria-hidden="true"></i></a>
					</div>

					<div class="txt17 p-r-20 p-t-5 p-b-5">Copyright &copy; 2020
						CaroldaPetitPoa | Desenvolvido por <a href="https://www.zeituneinformatica.com.br"
							class="fs-15 c-white">Zeituneinformatica.</a></div>
				</div>
			</div>
		</div>
	</footer>


	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/jquery/jquery.validate.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript"
		src="vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript"
		src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript"
		src="vendor/daterangepicker/moment.min.js"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"
		integrity="sha512-3n19xznO0ubPpSwYCRRBgHh63DrV+bdZfHK52b1esvId4GsfwStQNPJFjeQos2h3JwCmZl0/LgLxSKMAI55hgw=="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
		integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
		crossorigin="anonymous"></script>

	<script
		src="//cdn.rawgit.com/niftylettuce/bootstrap-datepicker-mobile/9999ca720ebf89600bda1659c96a291dc447ff39/bootstrap-datepicker-mobile.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.pt-BR.min.js"
		integrity="sha512-mVkLPLQVfOWLRlC2ZJuyX5+0XrTlbW2cyAwyqgPkLGxhoaHNSWesYMlcUjX8X+k45YB8q90s88O7sos86636NQ=="
		crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="js/slick-custom.js"></script>

	<!--===============================================================================================-->
	<script type="text/javascript"
		src="vendor/lightbox2/js/lightbox.min.js"></script>
	<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/isotope/isotope.pkgd.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>
	<!-- NOTE: prior to v2.2.1 tiny-slider.js need to be in <body> -->
	<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery.form.js"></script>
	<script src="js/buscar-cep.js"></script>
	<script src="js/jquery.mask.js"></script>
	<script src="https://js.api.here.com/v3/3.1/mapsjs-core.js"
  type="text/javascript" charset="utf-8"></script>
<script src="https://js.api.here.com/v3/3.1/mapsjs-service.js"
  type="text/javascript" charset="utf-8"></script>
  
	
	<script>
	

		/*
		 * [ Load page ] ===========================================================
		 */
		$(".animsition").animsition({
			inClass: 'fade-in',
			outClass: 'fade-out',
			inDuration: 1500,
			outDuration: 800,
			linkElement: '.animsition-link',
			loading: true,
			loadingParentElement: 'html',
			loadingClass: 'animsition-loading-1',
			loadingInner: '<div class="cp-spinner cp-meter"></div>',
			timeout: false,
			timeoutCountdown: 2000,
			onLoadEvent: true,
			browser: ['animation-duration', '-webkit-animation-duration'],
			overlay: false,
			overlayClass: 'animsition-overlay-slide',
			overlayParentElement: 'html',
			transition: function (url) { window.location.href = url; }
		
		});

		/*
		 * [ Back to top ]
		 * ===========================================================
		 */
		 /*
			 * [ Select ] ===========================================================
			 */
			$(".selection-1").select2({
				minimumResultsForSearch: -1
			});

		/*
		 * [ Daterangepicker ]
		 * ===========================================================
		 */
		$('.date').datepicker({
			language: 'pt-BR',
			format: "dd/mm/yyyy",
			autoclose: true
		});







	$(".suco").click(function () {
		if ($(this).is(":checked")) {
			$(".sucos").show();
		}
		else
			$(".sucos").hide();
	});
	
	
	$(".vinho").click(function () {
		if ($(this).is(":checked")) {
			$(".vinhos").show();
		}
		else
			$(".vinhos").hide();
	});



	$(".cartao").click(function () {
		if ($(this).is(":checked")) {
			$(".textbox").show();
		}
		else
			$(".textbox").hide();
	});
	
	$(".retirar").click(function () {
		if ($(this).is(":checked")) {
			$(".bloco_end").hide();
			$sum -= + $freteAntigo;
			$freteAntigo=0;
			
		}
		else
			$(".bloco_end").show();
		$("#enderecoE").val("");
        $("#bairroE").val("");
        $("#numeroE").val("");
        $("#cepE").val("");
       
	});

	var behavior = function (val) {
		return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	},
		options = {
			onKeyPress: function (val, e, field, options) {
				field.mask(behavior.apply({}, arguments), options);
			}
		};

	$('input[name=telefoneE]').mask(behavior, options);
	$('input[name=dataE]').mask("00/00/0000");
	$('input[name=cepE]').mask("00000-000");
	$('input[name=numeroE]').mask("9999999");

	
	$("#custom-add1-text").blur(function(){
		$("#custom-add1").val($("#custom-add1-text").val());
		});
	
	$("#custom-add2-text").blur(function(){
		$("#custom-add2").val($("#custom-add2-text").val());
		});
	
	
	$("#custom-add3-text").blur(function(){
		$("#custom-add3").val($("#custom-add3-text").val());
		});
	
	
	$("#custom-add4-text").blur(function(){
		$("#custom-add4").val($("#custom-add4-text").val());
		});
	
	
	$("#form-especial").validate({
		// Specify validation rules
		errorPlacement: function (error, element) {
			return true;
		},
		rules: {

			data: "required",
			nome: "required",
			hora: "required",
			telefone: "required",
			conheceu: "required",
			tipobebida: "required",
			nomeReceber: "required",
			email: {
				required: true,
				email: true
			},
			endereco: "required",
			numero: "required",
			bairro: "required",
			cidade: "required",
			estado: "required",
						'fpagamento': "required",

		},

		submitHandler: function (form) {
			$('#pedidobtnE').toggleClass(
				'uiButtonActive');
			$('#pedidobtnE').prop('disabled', true);
			$('.closemodal').prop(
				'disabled', true);
			$('.closemodal').addClass(
				'noHover')
				
				

			$('#enderecocompletoE').val(
				$("#form-especial input[name=enderecoE]").val() + ", " +
				$("#form-especial input[name=numeroE]").val() + ", " +
				$("#form-especial input[name=complementoE]").val() + ", " +
				$("#form-especial input[name=bairroE]").val() + ", " +
				$("#form-especial input[name=cidadeE]").val() + ", " +
				$("#form-especial input[name=estadoE]").val());


			$('#pedidobtnE')
				.html(
					`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Enviando Pedido...`);

			$.ajax({
				url: 'emitirEspecial',
				type: 'post',
				data: $(form).serialize(),
				success: function (response) {
					if (response == "True") {
						$(
							'#pedidobtnE')
							.html(
								`Realizar Pedido`);
						$(
							'#pedidobtnE')
							.toggleClass(
								'uiButtonActive');
						$(
							'#pedidobtnE')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.prop(
								'disabled',
								false);
						$(
							'.closemodal')
							.removeClass(
								'noHover')
					
						$(
							'#myModal')
							.modal(
								"show");
					} else {
						console.log(response)


					}
				}
			});
		}
	});
	
	
// //////////////////frete//////////////////
	




	var endereco;
	var numero;
	var bairro;
	var cidade;
	var data;
	var hora;
	var lat;
	var lng;

	$("#calcularFreteE").click(function () {
		$('#calcularFreteE').toggleClass('uiButtonActive');			
		$('#calcularFreteE').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);

		endereco = $("#enderecoE").val()
		numero = $("#numeroE").val()
		bairro = $("#bairroE").val()
		cidade = $("#cidadeE").val()
		data = $("#dataE").val().split("/")[2] + "-" + $("#dataE").val().split("/")[1] + "-" + $("#dataE").val().split("/")[0]
		hora = $("#horaE").val().split('-')[1] + ":30"
	
		geocode(platform)
	});


$('.fE').on('change', function () { 
	
$('#calcularFreteE').toggleClass('uiButtonActive');			
		$('#calcularFreteE').html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Calculando...`);
		
		endereco = $("#enderecoE").val()
		numero = $("#numeroE").val()
		bairro = $("#bairroE").val()
		cidade = $("#cidadeE").val()
		data = $("#dataE").val().split("/")[2] + "-" + $("#dataE").val().split("/")[1] + "-" + $("#dataE").val().split("/")[0]
		hora = $("#horaE").val().split('-')[1] + ":30"
	
		geocode(platform)
 });
 

	var $freteAntigo = 0;


	var platform = new H.service.Platform({
		'apikey': '_5sbZz1Hl8XUJ4EpMlQ9lTDq08j2wXhwwoZNkhrCZb4'
	});



	function geocode(platform) {
		var geocoder = platform.getSearchService(),
			geocodingParameters = {
				q: endereco + ", " + numero + ", " + bairro + ", " + cidade
			};

		geocoder.geocode(
			geocodingParameters,
			onSuccess,
			onError
		);
	}

	function onSuccess(result) {
		var locations = result.items[0];
		lat = result.items[0].position.lat
		lng = result.items[0].position.lng


		var routingService = platform.getRoutingService()
		const params = {
			mode: "fastest;car;traffic:enabled",
			waypoint0: "-22.92432,-43.23895",
			waypoint1: lat + "," + lng,
			departure: data + 'T' + hora,
			representation: "display",
			routeAttributes: "summary"
		};

		routingService.calculateRoute(params, success => {
console.log(success.response.route[0].summary)

			calculaFreteTaxi(success.response.route[0].summary.baseTime, success.response.route[0].summary.trafficTime - success.response.route[0].summary.baseTime, success.response.route[0].summary.distance)

			const routeLineString = new H.geo.LineString();
			success.response.route[0].shape.forEach(point => {
				const [lat, lng] = point.split(",");
				routeLineString.pushPoint({
					lat: lat,
					lng: lng
				});
			});

		}, error => {
			console.log(error);
			$('#calcularFreteE').html(`Calcular Frete`);
		$('#calcularFreteE').toggleClass('uiButtonActive');
		
		
		});

	}
	function onError(error) {
		alert('Can\'t reach the remote server');
		$('#calcularFreteE').html(`Calcular Frete`);
		$('#calcularFreteE').toggleClass('uiButtonActive');
		
		
	}

	function calculaFreteTaxi(tviagem, tparado, distancia) {
		var total = 5.2;
		total += + (2.65 * (distancia / 1000))
		total += + ((33, 39 / 3600) * tparado)


		$("input[name=freteE]").attr("placeholder", "R$" +Math.ceil((total * 0.6))+",00");
		if ($freteAntigo != 0) {
			$freteAntigo = 0;
		}
				$freteAntigo = Math.ceil((total * 0.6));
		if($freteAntigo==0){
			$('#pedidobtnE').prop('disabled', true);
			$("input[name=freteE]").attr("placeholder", "Verifique as informações digitadas e tente novamente");
		}else{
				$('#pedidobtnE').prop('disabled', false);
		}
		
		
		
		$("input[name=fretehiddenE]").val(Math.ceil((total * 0.6)));
		
		
		
		
	


		$('#calcularFreteE').html(`Calcular Frete`);
		$('#calcularFreteE').toggleClass('uiButtonActive');
		
		
		

	};

	
	</script>

</body>
</html>

