<form id="form-pedido6" role="form" action="emitirPedido" method="post">
	<!-- Modal -->
	<div class="modal fade" id="especial" data-backdrop="static"
		data-keyboard="false" tabindex="-1" role="dialog"
		aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel">
						<span class="tit2 t-center"> Box "� luz de velas" </span>
					</h5>
					
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<div class="row">


						<!-- <div class="container"
							style="width: auto !important; padding-right: 0 !important; padding-left: 0 !important;">

							<div class="albery-container">

								<div class="albery-wrapper">

									<div class="albery-item">
										<img src="images/boxmaes/1.jpeg" alt="">
									</div>
									<div class="albery-item">
										<img src="images/boxmaes/2.jpeg" alt="">
									</div>
									<div class="albery-item">
										<img src="images/boxmaes/3.jpeg" alt="">
									</div>								

									<div class="albery-item">
										<img src="images/boxmaes/5.jpeg" alt="">
									</div>

									

								</div>

								<div class="move-right">
									<a href="#" id="rightArrow"></a>
								</div>
								<div class="move-left">
									<a href="#" id="leftArrow"></a>
								</div>

							</div>

							<div class="pagination-container">
								<div class="pagination-wrapper">
									<div class="pagination-item" data-item="1">
										<img src="images/boxmaes/1.jpeg" alt="">
									</div>
									<div class="pagination-item" data-item="2">
										<img src="images/boxmaes/2.jpeg" alt="">
									</div>
									<div class="pagination-item" data-item="3">
										<img src="images/boxmaes/3.jpeg" alt="">
									</div>									

									<div class="pagination-item" data-item="4">
										<img src="images/boxmaes/5.jpeg" alt="">
									</div>

									
								</div>
							</div>
						</div> -->

						<div class="col-lg-10 p-b-30 mx-auto">
							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Dados:</h3>
							<input type="hidden" name="produto" id="produto" value='Box "� luz de velas"'>
							<input type="hidden" name="enderecocompleto"
								id="enderecocompleto6" value=""> <input type="hidden"
								name="valor" id="valorconta" value="158"> <input type="hidden"
								name="fretehidden" value="0">

							<div class="row">

								<div class="col-md-6">

									<div class="form-group">
										<div class=' date' id="datetimepicker6">
											<span class="txt9">Data da entrega* <span
												style="color: red;">(Obrigat�rio 2 dias de
													anteced�ncia para realizar o pedido. Consulte
													disponibilidade para prazos menores.)</span>
												<div
													class="wrap-inputdate pos-relative txt10 size12 bo2 bo-rad-10 m-t-3 m-b-23">

													<!-- this is where the magic happens -->
													<input type="text" class=" bo-rad-10 sizefull txt10 p-l-20 f6"
														name="data" id="data6" placeholder="DD/MM/YYYY" required />
													<i
														class="btn-calendar fa input-group-addon fa-calendar ab-r-m hov-pointer m-r-18"
														aria-hidden="true"></i>

												</div>
										</div>
									</div>
								</div>







								<div class="col-md-6">
									<!-- Time -->
									<span class="txt9">Hora da entrega<br> (Consultar
										disponibilidade)*
									</span>

									<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<!-- Select2 -->
										<select class="selection-1 f6" name="hora" id="hora6">
											<option selected>Selecione um hor�rio</option>
											<option value="16:00-21:00">16:00-21:00</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">


								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Qual o seu nome?*</span>

									<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="nome" placeholder="Nome">
									</div>
								</div>

								<div class="col-md-12">
									<!-- Email -->
									<span class="txt9">Qual o seu e-mail?*</span>

									<div class="wrap-inputemail size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="email" placeholder="Email">
									</div>
								</div>
							</div>
							<div class="row">

								<div class="col-md-6">
									<!-- Phone -->
									<span class="txt9">Qual o seu telefone?*</span>

									<div class="wrap-inputphone size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="tel"
											name="telefone" placeholder="Telefone">
									</div>
								</div>
								<div class="col-md-6">
									<!-- Time -->
									<span class="txt9">Me conta! De onde nos conheceu?*</span>

									<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<!-- Select2 -->
										<select class="selection-1" name="conheceu">
											<option selected>Selecione uma op��o</option>
											<option value="Instagram">Instagram</option>
											<option value="Indica��o">Indica��o</option>
											<option value="Recebi uma cesta de presente">Recebi
												uma cesta de presente</option>
										</select>
									</div>
								</div>

							</div>

							<div class="row">

								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Qual o nome de quem ir� receber?*</span>

									<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="nomeReceber" placeholder="Nome">
									</div>
								</div>
							</div>
							<div class="row">
								<br>
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Retirar na tijuca?:</span> <input
										type="checkbox" class="retirar" name="retirar" value="sim">
									Sim <br> <br>


								</div>



							</div>


							<div class="bloco_end">

								<div class="row">

									<div class="col-md-4">
										<span class="txt9">CEP:</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">

											<input type="tel" id="cep6" name="cep"
												class="cep form-control bo-rad-10 sizefull txt10 p-l-20"
												placeholder="Entre com o CEP"> <span
												class="input-group-btn">
												<button class="btn btn-default" id="bcep" name="bcep"
													type="button">
													<span class="fa fa-search" aria-hidden="true"></span>
												</button>
											</span>
										</div>
									</div>



									<div class="col-md-8">
										<span class="txt9">Endere&ccedil;o*</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input id="endereco6" name="endereco" type="text"
												class="form-control bo-rad-10 sizefull txt10 p-l-20 f6">
										</div>
									</div>



								</div>


								<div class="row">


									<div class="col-lg-2 col-xs-2">
										<span class="txt9">N&uacute;mero*</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input id="numero6" name="numero" type="text"
												class="form-control bo-rad-10 sizefull txt10 p-l-20 f6">
										</div>
									</div>

									<div class="col-lg-3 col-xs-6">
										<span class="txt9">Complemento</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input id="complemento6" name="complemento" type="text"
												class="form-control bo-rad-10 sizefull txt10 p-l-20">
										</div>
									</div>

									<div class="col-lg-3 col-xs-4">
										<span class="txt9">Bairro*</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input id="bairro6" name="bairro" type="text"
												class="form-control bo-rad-10 sizefull txt10 p-l-20 f6">
										</div>
									</div>

									<div class="col-lg-3 col-xs-4">
										<span class="txt9">Cidade</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input name="cidade" id="cidade6" type="text"
												value="Rio de Janeiro"
												class="form-control bo-rad-10 sizefull txt10 p-l-20">
										</div>
									</div>

									<div class="col-lg-1 col-xs-1 col-xs-2">
										<span class="txt9">Estado</span>
										<div
											class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<input name="estado" id="estado6" type="text" value="RJ"
												class="form-control bo-rad-10 sizefull txt10 p-l-20">
										</div>
									</div>

								</div>
								<div class="row">
									<div class="col-md-12">
										<button type="button"
											class="btn3 flex-c-m size13 txt11 trans-0-4"
											id="calcularFrete6">Calcular frete</button>
									</div>
									<div class="col-md-12">
										<span class="txt9">Frete:</span><input class="form-control"
											type="text" name="frete" placeholder="R$0,00" disabled>
									</div>

								</div>

								<br>
							</div>


							<div class="row">
								<br>
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Incluir cart�o?:</span> <input
										type="checkbox" class="cartao" name="cartao" value="sim">
									Sim <br> <br>


								</div>



							</div>

							<div class="row textbox" id="Comments" style="display: none;">
								<div class="col-md-12">

									<div>
										<textarea class="form-control"
											id="exampleFormControlTextarea6" name="mensagemcartao"
											rows="3" placeholder="Digite seu texto para o cart�o aqui."></textarea>
										<br> <br>
									</div>


								</div>
							</div>

							

							<h3 class="tit-mainmenu tit10 p-b-25 t-center">
								O que vem no Box:
							</h3>



							<div class="row">
							<ul class="list-unstyled">

									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Torradinhas</li>
									<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Flor de salame</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Queijo brie com geleia</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Castanhas</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Pastinha artesanal de gorgonzola</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Uva</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Morango com chocolate</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Queijo minas frescal com favo de mel</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										P�o artesanal FAS</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Petit four em formato de cora��o </li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										Coco fresco </li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										1 flor</li>
										<li><i class="fa fa-circle" style="font-size: 6px"></i>
										1 mini vela</li>

								</ul>
							
				


							</div>

							<br> <br>


								<h3 class="tit-mainmenu tit10 p-b-25 t-center">Batida:*</h3>



								<div class="row">
									<div class="col-md-6">

										<!-- Default panel contents -->

										<div class="checkbox">
											<label><input name="chk-especial-batida" type="checkbox"
												value="Cajuzinho" id="eCajuzinho">Cajuzinho (amendoin, chocolate e vodka)</label>
										</div>
										<div class="checkbox">
											<label><input name="chk-especial-batida" type="checkbox"
												value="Mousse de maracuj�" id="eMaracuj�">Mousse de maracuj�</label>
										</div>
										<div class="checkbox ">
											<label><input name="chk-especial-batida" type="checkbox"
												value="Beijinho" id="eBeijinho">Beijinho (coc� e vodka)</label>
										</div>



									</div>

									<div class="col-md-6">

										<!-- Default panel contents -->

										<div class="checkbox ">
											<label><input name="chk-especial-batida" type="checkbox"
												value="Moranguinho" id="eMoranguinho">Moranguinho</label>
										</div>
										<div class="checkbox">
											<label><input name="chk-especial-batida" type="checkbox"
												value="Mousse de lim�o" id="elim�o">Mousse de lim�o</label>
										</div>
										<div class="checkbox ">
											<label><input name="chk-especial-batida" type="checkbox"
												value="Nutella" id="eNutella">Nutella</label>
										</div>
										<div class="checkbox ">
											<label><input name="chk-especial-batida" type="checkbox"
												value="Amarette" id="eAmarette">Amarette (licor de cacau, creme de chocolate e conhaque)</label>
										</div>





									</div>

								</div>

						

							<br> <br>
						

							
							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Adicionais:</h3>



							<div class="row">
								<div class="col-md-12" id="adicionais">

									<!-- Default panel contents -->
									
									<div class="checkbox ">
										<label><input name="chk-especial-adicionais"
											type="checkbox" value="Foto polaroid" id="foto">Foto polaroid - R$8,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-especial-adicionais"
											type="checkbox" value="Mini arranjo de flor">Mini
											arranjo de flor - R$25,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-especial-adicionais"
											type="checkbox" value="Master arranjo de flor">Master
											arranjo de flor - R$48,00</label>
									</div>
								




								

								</div>

							</div>
							<br>
							<div class="row">
								<div class="col-md-12">
									<span class="txt9">Possui restri��es, prefer�ncias ou
										alguma observa��o?:</span> <br> <br>
									<div>
										<textarea class="form-control" id="obs" name="obs" rows="3">Digite seu texto aqui.</textarea>
										<br> <br>
									</div>


								</div>

								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Escolha a forma de pagamento:</span><br>
									<div class="pair col-md-6">
										<input type="checkbox" checked="true" class="fpagamento"
											name="fpagamento" id="dinheiro" value="Transfer�ncia/Pix">
										Transfer�ncia/Pix
									</div>
									<div class="pair col-md-6">
										<input type="checkbox" class="fpagamento" name="fpagamento"
											id="ame" value="Cart�o de cr�dito"> Cart�o de cr�dito
										<br /> <label for="fpagamento"><small>Acr�scimo
												de 5%</small></label> <br> <br>

									</div>
								</div>

							</div>



							<!-- Email -->
							<span class="txt9">Cupom de desconto:</span>

							<div class="wrap-inputemail size12 bo2 bo-rad-10 m-t-3 m-b-23">

								<input class="bo-rad-10 sizefull txt10 p-l-20 cupom" type="text"
									name="cupom" id="cupom" placeholder="Insira aqui seu cupom">
							</div>



							<span class="txt9">Subtotal:</span><input class="form-control"
								type="text" name="subtotal" placeholder="R$158,00" disabled>

							<span class="txt9">Desconto:</span><input class="form-control"
								type="text" name="desconto" placeholder="R$0,00" disabled>

							<span class="txt9">Total:</span><input class="form-control"
								type="text" name="total" id="total" placeholder="R$158,00" disabled>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button"
							class="btn3 flex-c-m size13 txt11 trans-0-4 closemodal"
							data-dismiss="modal">Retornar</button>
						<button id="pedidobtn6" type="submit"
							class="btn3 flex-c-m size13 txt11 trans-0-4">Realizar
							Pedido</button>

					</div>
				</div>
			</div>
		</div>
	</div>
</form>