<section class="section-lunch bgwhite" id="galeria">
		<div class="header-lunch  "
			style="background-image: url(images/galeria1.jpg);">
			<div class="bg1-overlay t-center p-t-170 p-b-165">
				<h2 class="tit4 t-center">Galeria</h2>
			</div>
		</div>
	<div class="section-gallery p-t-118 p-b-100">
		<div class="wrap-label-gallery filter-tope-group size28 flex-w flex-sb-m m-l-r-auto flex-col-c-sm p-l-15 p-r-15 m-b-60">
			<button class="label-gallery txt26 trans-0-4 is-actived" data-filter="*">
				Tudo
			</button>

			<button class="label-gallery txt26 trans-0-4" data-filter="boxp">
				BoxP
			</button>

			<button class="label-gallery txt26 trans-0-4" data-filter="boxmvinho">
				BoxM com Vinho
			</button>

			<button class="label-gallery txt26 trans-0-4" data-filter="platterm">
				Platter M
			</button>

			<button class="label-gallery txt26 trans-0-4" data-filter="platterg">
				Platter G
			</button>
			<button class="label-gallery txt26 trans-0-4" data-filter="cafedamanha">
				Caf� da manh�
			</button>
		</div>

		<div class="wrap-gallery isotope-grid flex-w p-l-25 p-r-25" >
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/1.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/1.jpeg" data-lightbox="boxp1"></a>
				</div>
			</div>
			
				<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/2.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/2.jpeg" data-lightbox="boxp2"></a>
				</div>
			</div>
			
				<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/3.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/3.jpeg" data-lightbox="boxp3"></a>
				</div>
			</div>
			
				<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/4.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/4.jpeg" data-lightbox="boxp4"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/5.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/5.jpeg" data-lightbox="boxp5"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/6.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/6.jpeg" data-lightbox="boxp6"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/7.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/7.jpeg" data-lightbox="boxp7"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/8.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/8.jpeg" data-lightbox="boxp8"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/9.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/9.jpeg" data-lightbox="boxp9"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxp" data-filter="boxp">
				<img src="images/boxp/10.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/boxp/10.jpeg" data-lightbox="boxp10"></a>
				</div>
			</div>

			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom boxmvinho" data-filter="boxmvinho">
				<img src="images/boxmvinho.jpg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m " href="images/boxmvinho.jpg" data-lightbox="boxmvinho"></a>
				</div>
			</div>

			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/1.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/1.jpeg" data-lightbox="platterm1"></a>
				</div>
			</div>
			
				<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/2.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/2.jpeg" data-lightbox="platterm2"></a>
				</div>
			</div>
			
				<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/3.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/3.jpeg" data-lightbox="platterm3"></a>
				</div>
			</div>
			
				<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/4.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/4.jpeg" data-lightbox="platterm4"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/5.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/5.jpeg" data-lightbox="platterm5"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/6.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/6.jpeg" data-lightbox="platterm6"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/7.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/7.jpeg" data-lightbox="platterm7"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/8.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/8.jpeg" data-lightbox="platterm8"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/9.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/9.jpeg" data-lightbox="platterm9"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterm" data-filter="platterm">
				<img src="images/platterm/10.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/platterm/10.jpeg" data-lightbox="platterm10"></a>
				</div>
			</div>

			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom platterg" data-filter="platterg">
				<img src="images/platterg.jpg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m " href="images/platterg.jpg" data-lightbox="platterg"></a>
				</div>
			</div>

			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/1.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/1.jpeg" data-lightbox="cafedamanha1"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/2.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/2.jpeg" data-lightbox="cafedamanha2"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/3.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/3.jpeg" data-lightbox="cafedamanha3"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/4.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/4.jpeg" data-lightbox="cafedamanha4"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/5.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/5.jpeg" data-lightbox="cafedamanha5"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/6.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/6.jpeg" data-lightbox="cafedamanha6"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/7.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/7.jpeg" data-lightbox="cafedamanha7"></a>
				</div>
			</div>
			
					<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/8.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/8.jpeg" data-lightbox="cafedamanha8"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/9.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/9.jpeg" data-lightbox="cafedamanha9"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/10.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/10.jpeg" data-lightbox="cafedamanha10"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/11.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/11.jpeg" data-lightbox="cafedamanha11"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/12.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/12.jpeg" data-lightbox="cafedamanha12"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/13.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/13.jpeg" data-lightbox="cafedamanha13"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/14.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/14.jpeg" data-lightbox="cafedamanha14"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/15.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/15.jpeg" data-lightbox="cafedamanha15"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/16.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/16.jpeg" data-lightbox="cafedamanha16"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/17.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/17.jpeg" data-lightbox="cafedamanha17"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/18.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/18.jpeg" data-lightbox="cafedamanha18"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/19.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/19.jpeg" data-lightbox="cafedamanha19"></a>
				</div>
			</div>
			
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/20.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/20.jpeg" data-lightbox="cafedamanha20"></a>
				</div>
			</div>
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/21.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/21.jpeg" data-lightbox="cafedamanha21"></a>
				</div>
			</div>
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/22.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/22.jpeg" data-lightbox="cafedamanha22"></a>
				</div>
			</div>
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/23.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/23.jpeg" data-lightbox="cafedamanha23"></a>
				</div>
			</div>
			<div class="item-gallery isotope-item bo-rad-10 hov-img-zoom cafedamanha" data-filter="cafedamanha">
				<img src="images/cafe/24.jpeg" alt="IMG-GALLERY">

				<div class="overlay-item-gallery trans-0-4 flex-c-m">
					<a class="btn-show-gallery flex-c-m" href="images/cafe/24.jpeg" data-lightbox="cafedamanha24"></a>
				</div>
			</div>

			
		</div>
		
		  <div class="isotope-pager pagination flex-c-m flex-w p-l-15 p-r-15 m-t-24 m-b-50" style="padding-top: 15px; text-align:center;">
        </div>

		
	</div>
	</section>