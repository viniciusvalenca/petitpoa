<form id="form-pedido5" role="form" action="emitirPedido" method="post">
	<!-- Modal -->
	<div class="modal fade" id="platterm" data-backdrop="static"
		data-keyboard="false" tabindex="-1"
		aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-scrollable">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel">
						<span class="tit2 t-center"> Platter M </span>
					</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<div class="row">
					
						<div class="container"
							style="width: auto !important; padding-right: 0 !important; padding-left: 0 !important;">

							<div class="albery-container">

								<div class="albery-wrapper">

									<div class="albery-item">
										<img src="images/platterm/1.jpg" alt="">
									</div>
									<div class="albery-item">
										<img src="images/platterm/2.jpg" alt="">
									</div>
									<div class="albery-item">
										<img src="images/platterm/3.jpg" alt="">
									</div>
									<div class="albery-item">
										<img src="images/platterm/4.jpg" alt="">
									</div>
									<div class="albery-item">
										<img src="images/platterm/5.jpg" alt="">
									</div>

									<div class="albery-item">
										<img src="images/platterm/6.jpg" alt="">
									</div>
									<div class="albery-item">
										<img src="images/platterm/7.jpg" alt="">
									</div>

									<div class="albery-item">
										<img src="images/platterm/8.jpg" alt="">
									</div>
									<div class="albery-item">
										<img src="images/platterm/9.jpg" alt="">
									</div>

									<div class="albery-item">
										<img src="images/platterm/10.jpg" alt="">
									</div>

									<div class="albery-item">
										<img src="images/platterm/11.jpg" alt="">
									</div>

									

								</div>

								<div class="move-right">
									<a href="#" class="rightArrow"></a>
								</div>
								<div class="move-left">
									<a href="#" class="leftArrow"></a>
								</div>

							</div>

							<div class="pagination-container">
								<div class="pagination-wrapper">
									<div class="pagination-item" data-item="1">
										<img src="images/platterm/1.jpg" alt="">
									</div>
									<div class="pagination-item" data-item="2">
										<img src="images/platterm/2.jpg" alt="">
									</div>
									<div class="pagination-item" data-item="3">
										<img src="images/platterm/3.jpg" alt="">
									</div>
									<div class="pagination-item" data-item="4">
										<img src="images/platterm/4.jpg" alt="">
									</div>

									<div class="pagination-item" data-item="5">
										<img src="images/platterm/5.jpg" alt="">
									</div>
									<div class="pagination-item" data-item="6">
										<img src="images/platterm/6.jpg" alt="">
									</div>
									<div class="pagination-item" data-item="7">
										<img src="images/platterm/7.jpg" alt="">
									</div>
									<div class="pagination-item" data-item="8">
										<img src="images/platterm/8.jpg" alt="">
									</div>
									<div class="pagination-item" data-item="9">
										<img src="images/platterm/9.jpg" alt="">
									</div>

									<div class="pagination-item" data-item="10">
										<img src="images/platterm/10.jpg" alt="">
									</div>
									<div class="pagination-item" data-item="11">
										<img src="images/platterm/11.jpg" alt="">
									</div>
								</div>
							</div>
						</div>
					
					
					
						<div class="col-lg-10 p-b-30 mx-auto">
							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Dados:</h3>
							<input type="hidden" name="produto" id="produto"  value="Platter M"> <input
								type="hidden" name="valor" value="170">
<input type="hidden" name="enderecocompleto" id="enderecocompleto5" value="">
<input type="hidden" name="fretehidden" value="0">
<input type="hidden"
								name="subtotalhidden">
							<div class="row">

								<div class="col-md-6">

									<div class="form-group">
										<div class=' date' id="datetimepicker5">
											<span class="txt9">Data da entrega* <span style="color:red;">(Obrigat�rio 2 dias de anteced�ncia para realizar o pedido. Consulte disponibilidade para prazos menores.)</span>
											<div class="wrap-inputdate pos-relative txt10 size12 bo2 bo-rad-10 m-t-3 m-b-23">

												<!-- this is where the magic happens -->
												<input type="text"
													class=" bo-rad-10 sizefull txt10 p-l-20 f5"
													name="data" id="data5"
													placeholder="DD/MM/YYYY" /> <i
													class="btn-calendar fa input-group-addon fa-calendar ab-r-m hov-pointer m-r-18"
													aria-hidden="true"></i>

											</div>
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<!-- Time -->
									<span class="txt9">Hora da entrega<br> (Consultar
										disponibilidade)*</span>

									<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<!-- Select2 -->
										<select class="selection-1 f5" name="hora" id="hora5">
											<option selected>Selecione um hor�rio</option>
											<option value="07:00-08:00">07:00-08:00</option>
											<option value="08:00-09:00">08:00-09:00</option>
											<option value="09:00-10:00">09:00-10:00</option>
											<option value="15:00-16:00">15:00-16:00</option>
											<option value="16:00-17:00">16:00-17:00</option>
											<option value="17:00-18:00">17:00-18:00</option>
											<option value="18:00-19:00">18:00-19:00</option>
											<option value="19:00-20:00">19:00-20:00</option>
										</select>
									</div>
								</div>
							</div>
							<div class="row">


								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Qual o seu nome?*</span>

									<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="nome" placeholder="Nome">
									</div>
								</div>

								<div class="col-md-12">
									<!-- Email -->
									<span class="txt9">Qual o seu e-mail?*</span>

									<div class="wrap-inputemail size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="email" placeholder="Email">
									</div>
								</div>
							</div>
							<div class="row">

								<div class="col-md-6">
									<!-- Phone -->
									<span class="txt9">Qual o seu telefone?*</span>

									<div class="wrap-inputphone size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="tel"
											name="telefone" placeholder="Telefone">
									</div>
								</div>
									<div class="col-md-6">
									<!-- Time -->
									<span class="txt9">Me conta! De onde nos conheceu?*</span>

									<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<!-- Select2 -->
										<select class="selection-1" name="conheceu">
											<option selected>Selecione uma op��o</option>
											<option value="Instagram">Instagram</option>
											<option value="Indica��o">Indica��o</option>
											<option value="Recebi uma cesta de presente">Recebi uma cesta de presente</option>
										</select>
									</div>
								</div>

							</div>

							<div class="row">

								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Qual o nome de quem ir� receber?*</span>

									<div class="wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input class="bo-rad-10 sizefull txt10 p-l-20" type="text"
											name="nomeReceber" placeholder="Nome">
									</div>
								</div>
							</div>
							<div class="row">
							<br>
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Retirar na tijuca?:</span> <input
										type="checkbox" class="retirar" name="retirar" value="sim">
									Sim <br> <br>


								</div>



							</div>


<div class="bloco_end">
							<div class="row">

								<div class="col-md-4">
									<span class="txt9">CEP:</span>
									<div
										class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">

										<input type="tel" id="cep5" name="cep"
											class="cep form-control bo-rad-10 sizefull txt10 p-l-20"
											placeholder="Entre com o CEP"> <span
											class="input-group-btn">
											<button class="btn btn-default" id="bcep" name="bcep"
												type="button">
												<span class="fa fa-search" aria-hidden="true"></span>
											</button>
										</span>
									</div>
								</div>



								<div class="col-md-8">
									<span class="txt9">Endere&ccedil;o*</span>
									<div
										class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
										<input id="endereco5" name="endereco" type="text"
											class="form-control bo-rad-10 sizefull txt10 p-l-20">
									</div>
								</div>



							</div>
							
							
							<div class="row">
				

				<div class="col-lg-2 col-xs-2">
				<span class="txt9">N&uacute;mero*</span>
					<div class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
						<input id="numero5" name="numero" type="text"
							class="form-control bo-rad-10 sizefull txt10 p-l-20 f5">
					</div>
				</div>
				
				<div class="col-lg-3 col-xs-6">
				<span class="txt9">Complemento</span>
					<div class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
						<input id="complemento5" name="complemento" type="text"
							class="form-control bo-rad-10 sizefull txt10 p-l-20">
					</div>
				</div>
				
				<div class="col-lg-3 col-xs-4">
				<span class="txt9">Bairro*</span>
					<div class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
						<input id="bairro5" name="bairro" type="text"
							class="form-control bo-rad-10 sizefull txt10 p-l-20 f5">
					</div>
				</div>
		
				<div class="col-lg-3 col-xs-4">
				<span class="txt9">Cidade</span>
					<div class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
						<input name="cidade" id="cidade5" type="text" value="Rio de Janeiro"
							class="form-control bo-rad-10 sizefull txt10 p-l-20">
					</div>
				</div>
		
				<div class="col-lg-1 col-xs-1 col-xs-2">
				<span class="txt9">Estado</span>
					<div class="input-group wrap-inputname size12 bo2 bo-rad-10 m-t-3 m-b-23">
						<input name="estado" id="estado5" type="text" value="RJ"
							class="form-control bo-rad-10 sizefull txt10 p-l-20">
					</div>
				</div>
			
			</div>
						<div class="row">
								<div class="col-md-12">
<button type="button"
							class="btn3 flex-c-m size13 txt11 trans-0-4"
							id="calcularFrete5">Calcular frete</button>
							</div>
								

</div>
<br>
</div>
						
							<div class="row">
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Incluir cart�o?:</span> <input
										type="checkbox" class="cartao" name="cartao" value="sim"> Sim <br> <br>

								</div>



							</div>

							<div class="row textbox" id="Comments" style="display: none;">
								<div class="col-md-12">

									<div>
										<textarea class="form-control"
											id="exampleFormControlTextarea1" name="mensagemcartao" rows="3" placeholder="Digite seu texto para o cart�o aqui."></textarea>
										<br> <br>
									</div>


								</div>
							</div>
							
													<div class="row">
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Caso seja um presente, � de anivers�rio?:</span><br> <input
										type="checkbox" class="presente" name="presente" id="Sim" value="sim">
									Sim 
<input
										type="checkbox" class="presente" name="presente" id="N�o" value="n�o">
									N�o <br> <br>


								</div>



							</div>

							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Escolha at� 2 <b>queijos</b> do grupo especial ou grupo b�sico:*</h3>



							<div class="row">
								<div class="col-md-6">

									<!-- Default panel contents -->
									<div class="subtext">B�sicos</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Gouda" id="mGouda">Gouda</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Provolone" id="mProvolone">Provolone</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Brie" id="mBrie">Brie</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Mussarela" id="mMussarela">Mu�arela</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Gorgonzola" id="mGorgonzola">Gorgonzola</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Queijo Minas" id="mQueijoMinas">Queijo Minas</label>
									</div>



								</div>

								<div class="col-md-6">

									<!-- Default panel contents -->
									<div class="subtext">Especiais</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Parmes�o" id="mParmes�o">Parmes�o</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Mussarela de B�fala" id="mMussareladeB�fala">Mu�arela de B�fala</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-queijo" type="checkbox"
											value="Camembert" id="mCamembert">Camembert</label>
									</div>




								</div>

							</div>

							<br> <br>


							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Escolha at� 3 <b>frios</b> do grupo especial ou grupo b�sico:*</h3>



							<div class="row">
								<div class="col-md-6">

									<!-- Default panel contents -->
									<div class="subtext">B�sicos</div>
									<div class="checkbox">
										<label><input name="chk-platterm-frios" type="checkbox"
											value="Presunto" id="mPresunto">Presunto</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-frios" type="checkbox"
											value="Blanquet" id="mBlanquet">Blanquet</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-frios" type="checkbox"
											value="Peito de peru" id="mPeitodeperu">Peito de peru</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-frios" type="checkbox"
											value="Queijo bola" id="mQueijobola">Queijo bola</label>
									</div>




								</div>

								<div class="col-md-6" id="frios">

									<!-- Default panel contents -->
									<div class="subtext">Especiais</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-frios" type="checkbox"
											value="Parma" id="mParma">Parma</label>
									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-frios" type="checkbox"
											value="Copa" id="mCopa">Copa</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-frios" type="checkbox"
											value="Salame" id="mSalame">Salame</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-frios" type="checkbox"
											value="Pepperoni" id="mPepperoni">Pepperoni</label>
									</div>




								</div>

							</div>



							<br> <br>
							<h3 class="tit-mainmenu tit10 p-b-25 t-center">Adicionais:</h3>



							<div class="row">
								<div class="col-md-12" id="adicionais">

									<!-- Default panel contents -->

									<div class="checkbox">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Garrafa de vinho" class="vinho">Garrafa de
											vinho (tinto,branco, rose) - R$40,00</label>
									</div>
																											<div class="row vinhos" style="display: none;">

										<div class="col-md-12">
<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<select class="selection-1" name="tipovinho">
												<option selected>Selecione um sabor</option>
												<option value="Tinto">Tinto</option>
												<option value="Branco">Branco</option>
												<option value="Rose">Rose</option>
											</select>

</div>
										</div>



									</div>
									<div class="checkbox">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Vinho em lata">Vinho em lata
											269ml - R$20,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Pastinhas artesanais">Pastinhas
											artesanais - R$15,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Cerveja artesanal">Cerveja
											artesanal - R$30,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Long neck de cerveja">Long
											neck de cerveja - R$10,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Brownie">Brownie - R$15,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Caneca personalizada" >Caneca personalizada - R$50,00</label>
									</div>
										<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Mini arranjo de flor" >Mini arranjo de flor - R$25,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Master arranjo de flor" >Master arranjo de flor - R$48,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Nutella">Nutella 140g - R$15,00</label>
									</div>
									<div class="checkbox ">
										<label><input name="chk-platterm-adicionais"
											type="checkbox" value="Sucos" class="suco">Sucos
											300ml - R$10,00</label>
									</div>





									<div class="row sucos" style="display: none;">

										<div class="col-md-12">

									<div class="wrap-inputtime size12 bo2 bo-rad-10 m-t-3 m-b-23">
											<select class="selection-1" name="saborsuco">
												<option selected>Selecione um sabor</option>
												<option value="Tangerina">Tangerina</option>
												<option value="Laranja">Laranja</option>
												<option value="Uva integral">Uva integral</option>
												<option value="Mam�o com laranja">Mam�o com laranja</option>
												<option value="A�a�">A�a�</option>
												<option value="Suco verde">Suco verde</option>
											</select>

</div>


										</div>







									</div>

								</div>

							</div>
								<br>
							<div class="row">
							<div class="col-md-12">
<span class="txt9">Possui restri��es, prefer�ncias ou alguma observa��o?:</span> 
									<br> <br>
									<div>
										<textarea class="form-control"
											id="obs" name="obs"
											rows="3">Digite seu texto aqui.</textarea>
										<br> <br>
									</div>

</div>
								<div class="col-md-12">
									<!-- Name -->
									<span class="txt9">Escolha a forma de pagamento:</span><br>
									<div class="pair col-md-6"> <input
										type="checkbox" checked="true" class="fpagamento" name="fpagamento" id="dinheiro" value="Transfer�ncia/Pix" >
									Transfer�ncia/Pix
									</div>
									<div class="pair col-md-6">
<input
										type="checkbox" class="fpagamento" name="fpagamento" id="ame" value="Cart�o de cr�dito">
									Cart�o de cr�dito <br /><label for="fpagamento"><small>Acr�scimo de 5%</small></label> <br> <br>

</div>
								</div>
								</div>
								
									
								
									<!-- Email -->
									<span class="txt9">Cupom de desconto:</span>

									<div class="wrap-inputemail size12 bo2 bo-rad-10 m-t-3 m-b-23">
										
											<input class="bo-rad-10 sizefull txt10 p-l-20 cupom"
								type="text" name="cupom" id="cupom" placeholder="Insira aqui seu cupom" >
									</div>
								
								<span class="txt9">Subtotal:</span><input class="form-control"
								type="text" name="subtotal" placeholder="R$170,00" disabled>

							<span class="txt9">Desconto:</span><input class="form-control"
								type="text" name="desconto" placeholder="R$0,00" disabled>
								
<span class="txt9">Frete:</span><input class="form-control"
								type="text" name="frete" placeholder="R$0,00" disabled >
					
								
								
							<span class="txt9">Total:</span><input class="form-control"
								type="text" name="total" placeholder="R$170,00" disabled >
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn3 flex-c-m size13 txt11 trans-0-4 closemodal"
							data-dismiss="modal">Retornar</button>
						<button id="pedidobtn5" type="submit" class="btn3 flex-c-m size13 txt11 trans-0-4">Realizar
							Pedido</button>

					</div>
				</div>
			</div>
		</div>
	</div>
</form>