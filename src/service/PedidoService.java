package service;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import hibernate.util.HibernateUtil;
import model.Pedido;
public class PedidoService {
	
public boolean register(Pedido pedido){
	 Session session = HibernateUtil.openSession();
	 if(isPedidoExists(pedido)) return false;	
	
	 Transaction tx = null;	
	 try {
		 tx = session.getTransaction();
		 tx.begin();
		 session.saveOrUpdate(pedido);		
		 tx.commit();
	 } catch (Exception e) {
		 if (tx != null) {
			 tx.rollback();
		 }
		 e.printStackTrace();
	 } finally {
		 session.close();
	 }	
	 return true;
}



public boolean isPedidoExists(Pedido pedido){
	 Session session = HibernateUtil.openSession();
	 boolean result = false;
	 Transaction tx = null;
	 try{
		 tx = session.getTransaction();
		 tx.begin();
		 Query query = session.createQuery("from User where emailCliente='"+pedido.getEmailCliente()+"'");
		 Pedido p = (Pedido)query.uniqueResult();
		 tx.commit();
		 if(p!=null) result = true;
	 }catch(Exception ex){
		 if(tx!=null){
			 tx.rollback();
		 }
	 }finally{
		 session.close();
	 }
	 return result;
}

public Pedido isUserExists(String email){
	 Session session = HibernateUtil.openSession();
	 Pedido pedido=null;
	 Transaction tx = null;
	 try{
		 tx = session.getTransaction();
		 tx.begin();
		 Query query = session.createQuery("from Pedido where emailcliente='"+email+"'");
		 pedido =  (Pedido) query.uniqueResult();
		 
		 tx.commit();
	 }catch(Exception ex){
		 if(tx!=null){
			 tx.rollback();
		 }
	 }finally{
		 session.close();
	 }
	 return pedido;
}
}

