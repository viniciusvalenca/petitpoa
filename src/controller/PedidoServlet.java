package controller;

import java.util.Properties;
import java.util.TimeZone;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import model.Pedido;
import service.PedidoService;

public class PedidoServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		option(request, response);

	}

	protected void option(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			String url = request.getServletPath();

			if (url.equalsIgnoreCase("/emitirPedido")) {
				emitirPedido(request, response);

			} else if (url.equalsIgnoreCase("/responderPedido")) {
				responderPedido(request, response);

			} else if (url.equalsIgnoreCase("/aceitarPedido")) {
				aceitarPedido(request, response);

			} else if (url.equalsIgnoreCase("/validarCupom")) {
				validarCupom(request, response);

			} else if (url.equalsIgnoreCase("/emitirEspecial")) {
				emitirEspecial(request, response);

			}
			else if (url.equalsIgnoreCase("/especial")) {
				especial(request, response);
			

			}else {
				response.sendRedirect("/");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void emitirPedido(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain; charset=UTF-8");
		response.setContentType("application/pdf");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF8");
		PrintWriter out = response.getWriter();
		// set input and output stream
		// ServletOutputStream servletOutputStream = response.getOutputStream();
		String nome = request.getParameter("nome");
		String nomeReceber = request.getParameter("nomeReceber");
		String endereco = request.getParameter("enderecocompleto");
		String hora = request.getParameter("hora");
		String conheceu = request.getParameter("conheceu");
		String email = request.getParameter("email");
		String data = request.getParameter("data");
		String telefone = request.getParameter("telefone");
		String produto = request.getParameter("produto");
		String valor = request.getParameter("valor");
		String subtotal = request.getParameter("subtotalhidden");
		String obs = request.getParameter("obs");
		String cartao = request.getParameter("cartao");
		String retirar = request.getParameter("retirar");
		String mensagemcartao = request.getParameter("mensagemcartao");
		String presente = request.getParameter("presente");
		String frete = request.getParameter("fretehidden");
		String fpagamento = request.getParameter("fpagamento");

		String queijoboxp[] = request.getParameterValues("chk-boxp-queijo");
		String frioboxp[] = request.getParameterValues("chk-boxp-frios");
		String adicionaisboxp[] = request.getParameterValues("chk-boxp-adicionais");
		
		String queijoespecial[] = request.getParameterValues("chk-especial-queijo");
		String frioespecial[] = request.getParameterValues("chk-especial-frios");
		String adicionaisespecial[] = request.getParameterValues("chk-especial-adicionais");
		String sucosespecial[] = request.getParameterValues("chk-especial-sucos");
		String vinhosespecial[] = request.getParameterValues("chk-especial-vinho");
		String batidaespecial[] = request.getParameterValues("chk-especial-batida");

		String queijoespecial2[] = request.getParameterValues("chk-especial2-queijo");
		String frioespecial2[] = request.getParameterValues("chk-especial2-frios");
		String adicionaisespecial2[] = request.getParameterValues("chk-especial2-adicionais");
		String batidaespecial2[] = request.getParameterValues("chk-especial2-batida");
		String vinhosespecial2[] = request.getParameterValues("chk-especial2-vinho");
		
		String queijoboxm[] = request.getParameterValues("chk-boxm-queijo");
		String frioboxm[] = request.getParameterValues("chk-boxm-frios");
		String drinkboxm[] = request.getParameterValues("chk-boxm-drink");
		String adicionaisboxm[] = request.getParameterValues("chk-boxm-adicionais");

		String queijoplatterm[] = request.getParameterValues("chk-platterm-queijo");
		String frioplatterm[] = request.getParameterValues("chk-platterm-frios");
		String adicionaisplatterm[] = request.getParameterValues("chk-platterm-adicionais");

		String queijoplatterg[] = request.getParameterValues("chk-platterg-queijo");
		String frioplatterg[] = request.getParameterValues("chk-platterg-frios");
		String adicionaisplatterg[] = request.getParameterValues("chk-platterg-adicionais");

		String queijocafe[] = request.getParameterValues("chk-cafe-queijo");
		String friocafe[] = request.getParameterValues("chk-cafe-frios");
		String adicionaiscafe[] = request.getParameterValues("chk-cafe-adicionais");

		String sucoscafe[] = request.getParameterValues("chk-cafe-sucos");
		String sucosboxp[] = request.getParameterValues("chk-boxp-sucos");
		String vinhosboxp[] = request.getParameterValues("chk-boxp-vinho");
		String bolos[] = request.getParameterValues("chk-cafe-bolos");
		String sucoaddsabor = request.getParameter("saborsuco");
		String tipovinho = request.getParameter("tipovinho");
		
		
		System.out.println(produto);
		ServletContext context = getServletContext();
		String reportLocation = context.getRealPath("");

		request.setCharacterEncoding("UTF-8");

		Integer sub = Integer.parseInt(subtotal);

		Properties props = new Properties();

		props.put("mail.smtp.host", "mail.caroldapetitpoa.com.br");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");

		// get Session
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("no.reply@caroldapetitpoa.com.br", "#(dJ;jF(mz](");
			}
		});
		// compose message
		try {

			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress("contato@caroldapetitpoa.com.br"));
			message.setFrom(new InternetAddress("no.reply@caroldapetitpoa.com.br"));
			message.setReplyTo(InternetAddress.parse(email, false));
			message.setSubject(nome + " - " + produto + " - " + data + " - " + hora, "UTF-8");

			String message1 = "<html>";

			message1 += "<table style='width:100%; border:1px solid black'>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<th style='border:1px solid black ;font-size:50px' colspan='2'>" + produto + "</th>	";

			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Nome: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + nome + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Hora: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + hora + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Data: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + data + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Telefone: </b></td>	";
			message1 += "<td style='border:1px solid black'><a href='whatsapp://send?text=MESSAGE&phone=+55"
					+ telefone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "") + "'>" + telefone
					+ "</a></td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>De onde nos conheceu?: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + conheceu + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>E-mail: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + email + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Nome de quem irá receber: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + nomeReceber + "</td>	";
			message1 += "</tr>	";
			if (retirar==null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Endereço: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + endereco + "</td>	";
				message1 += "</tr>	";
			}
			if (produto.equals("Box P")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoboxp))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioboxp)) + "</td>	";
				message1 += "</tr>	";
				if (sucosboxp != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Suco: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(sucosboxp))
							+ "</td>	";
					message1 += "</tr>	";
				} else {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Vinho: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(vinhosboxp))
							+ "</td>	";
					message1 += "</tr>	";
				}
				if (adicionaisboxp != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisboxp))
							+ "</td>	";
					message1 += "</tr>	";
				}

			} else if (produto.equals("Box M com \"bons drinks\"")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoboxm))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioboxm))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Drink: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(drinkboxm))
						+ "</td>	";
				message1 += "</tr>	";
				if (adicionaisboxm != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisboxm))
							+ "</td>	";
					message1 += "</tr>	";
				}
			} else if (produto.equals("Platter M")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoplatterm))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioplatterm))
						+ "</td>	";
				message1 += "</tr>	";
				if (adicionaisplatterm != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisplatterm))
							+ "</td>	";
					message1 += "</tr>	";
				}
			} else if (produto.equals("Platter G")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoplatterg))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioplatterg))
						+ "</td>	";
				message1 += "</tr>	";
				if (adicionaisplatterg != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisplatterg))
							+ "</td>	";
					message1 += "</tr>	";
				}
			} else if (produto.equals("Café da manhã")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijocafe))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(friocafe)) + "</td>	";
				message1 += "</tr>	";

				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Suco: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(sucoscafe))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Bolo: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(bolos)) + "</td>	";
				message1 += "</tr>	";
				if (adicionaiscafe != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaiscafe))
							+ "</td>	";
					message1 += "</tr>	";
				}

			}else if (produto.equals("Box \"à luz de velas\"")) {
				
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Batida: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(batidaespecial))
						+ "</td>	";
				message1 += "</tr>	";
				
				if (adicionaisespecial != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisespecial))
							+ "</td>	";
					message1 += "</tr>	";
				}

			} else if (produto.equals("Sacola \"me leva que eu vou\"")) {			
				
				if (batidaespecial2 != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Batida: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(batidaespecial2))
							+ "</td>	";
					message1 += "</tr>	";
				} else if (vinhosespecial2 != null){
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Vinho: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(vinhosespecial2))
							+ "</td>	";
					message1 += "</tr>	";
				}else{
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Prosseco: </b></td>	";
					message1 += "<td style='border:1px solid black'>Prosseco" 
							+ "</td>	";
					message1 += "</tr>	";
				}
				if (adicionaisespecial2 != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisespecial2))
							+ "</td>	";
					message1 += "</tr>	";
				}

			}
			if (!produto.equals("Box \"à luz de velas\"") ) {
			if (!sucoaddsabor.equals("Selecione um sabor") ) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Suco adicional: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + sucoaddsabor + "</td>	";
				message1 += "</tr>	";
			}}
			if (!produto.equals("Box \"à luz de velas\"") ) {
			if (!tipovinho.equals("Selecione um sabor")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Sabor Vinho: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + tipovinho + "</td>	";
				message1 += "</tr>	";
			}}
			if (!obs.equals("")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Observação: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + obs + "</td>	";
				message1 += "</tr>	";
			}
			if (cartao != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Mensagem do cartão: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + mensagemcartao + "</td>	";
				message1 += "</tr>	";
			}
			if (presente != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Caso seja um presente, é de aniversário?: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + presente + "</td>	";
				message1 += "</tr>	";
			}

			if (retirar!=null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td colspan='2' style='border:1px solid black'><b>Frete: Retirar na Tijuca</b></td>	";
				message1 += "</tr>";
			} else {
				
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td colspan='2' style='border:1px solid black'><b>Subtotal: R$" + sub + ",00</b></td>	";
					message1 += "</tr>";
				
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td colspan='2' style='border:1px solid black'><b>Frete: R$" + frete + ",00</b></td>	";
				message1 += "</tr>";
			}
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td colspan='2' style='border:1px solid black'><b>Forma de Pagamento: " + fpagamento
					+ "</b></td>	";
			message1 += "</tr>";

			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td colspan='2' style='border:1px solid black'><b>Total: R$" + valor + ",00</b></td>	";
			message1 += "</tr></table>	<br><br><br>	";

			message1 += "<table style='width:100%'><tr>";
			message1 += "<th style=' text-align: center;vertical-align: top;'>";
			message1 += "<a href='http://www.caroldapetitpoa.com.br/responderPedido?e=aceito&pd=" + produto + "&vp="
					+ sub + "&f=" + frete + "8&t=" + valor + "&n=" + nome + "&m=" + email + "&d=" + data + "&h=" + hora
					+ "&fp=" + fpagamento + "&end=" + endereco
					+ "' style='background-color: green;  color: white;  padding: 15px 25px;  text-align: center;  text-decoration: none;  display: inline-block;'>ACEITAR</a>";
			message1 += "</th><th style=' text-align: center;vertical-align: top;'>";
			message1 += "<a href='http://www.caroldapetitpoa.com.br/responderPedido?e=recusado&pd=" + produto + "&vp="
					+ sub + "&f=" + frete + "8&t=" + valor + "&n=" + nome + "&m=" + email + "&d=" + data + "&h=" + hora
					+ "&fp=" + fpagamento + "&end=" + endereco
					+ "' style='background-color: red;  color: white;  padding: 15px 25px;  text-align: center;  text-decoration: none;  display: inline-block;'>RECUSAR</a>";
			message1 += "</th></tr></table><br><br>";

			message1 += "<b>______________________________</b>";
			message1 += "<table><tr><th style=' text-align: left;vertical-align: top;'>";
			message1 += "<br><a href='http://www.caroldapetitpoa.com.br'>www.caroldapetitpoa.com.br</a>";
			message1 += "<br><a href='http://www.instagram.com/carol_da_petitpoa'>@carol_da_petitpoa</a>";
			message1 += "<br><a href='https://api.whatsapp.com/send?phone=5521965802201'>+55 21 96580-2201</a></th>";

			message1 += "<th style=' text-align: left;vertical-align: top;'>"
					+ "<img src=\"cid:AbcXyz123\" style='display: inline-block;'/></th></tr></table>";

			message1 += "</html>";

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(message1, "text/html; charset=UTF-8");

			// creates multi-part
			Multipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);

			// adds inline image attachments
			MimeBodyPart imagePart = new MimeBodyPart();
			imagePart.setHeader("Content-ID", "<AbcXyz123>");
			imagePart.setDisposition(MimeBodyPart.INLINE);
			// attach the image file
			imagePart.attachFile(reportLocation + "images/logo2.png");

			multipart.addBodyPart(imagePart);

			message.setContent(multipart);

			out.print("True");
			out.flush();
			out.close();
			// sends the e-mail
			Transport.send(message);

			// send message

			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setFrom(new InternetAddress("no.reply@caroldapetitpoa.com.br"));
			message.setReplyTo(InternetAddress.parse(email, false));
			message.setSubject("Pré-pedido do " + produto + " para o dia " + data + " realizado com sucesso!", "UTF-8");

			String message1 = "<html>";

			message1 = "Recebemos seu pré-pedido. Obrigada! :)\r\n" + "\r\n"
					+ "Entraremos em contato para finalizarmos o pagamento e confirmarmos a disponibilidade da sua entrega. \r\n"
					+ "Caso o contato não seja feito em até 3h clicando no número que aparece no rodapé do email você pode falar com a gente pelo WhatsApp! Até logo!<br><br>";

			message1 += "<table style='width:100%; border:1px solid black'>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<th style='border:1px solid black ;font-size:50px' colspan='2'>" + produto + "</th>	";

			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Nome: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + nome + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Hora: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + hora + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Data: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + data + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Telefone: </b></td>	";
			message1 += "<td style='border:1px solid black'><a href='https://api.whatsapp.com/send?phone=55"
					+ telefone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "") + "'>" + telefone
					+ "</a></td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>De onde nos conheceu?: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + conheceu + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>E-mail: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + email + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Nome de quem irá receber: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + nomeReceber + "</td>	";
			message1 += "</tr>	";
			if (retirar==null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Endereço: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + endereco + "</td>	";
				message1 += "</tr>	";
			}
			if (produto.equals("Box P")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoboxp))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioboxp)) + "</td>	";
				message1 += "</tr>	";
				if (sucosboxp != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Suco: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(sucosboxp))
							+ "</td>	";
					message1 += "</tr>	";
				} else {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Vinho: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(vinhosboxp))
							+ "</td>	";
					message1 += "</tr>	";
				}
				if (adicionaisboxp != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisboxp))
							+ "</td>	";
					message1 += "</tr>	";
				}

			} else if (produto.equals("Box M com \"bons drinks\"")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoboxm))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioboxm))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Drink: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(drinkboxm))
						+ "</td>	";
				message1 += "</tr>	";
				if (adicionaisboxm != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisboxm))
							+ "</td>	";
					message1 += "</tr>	";
				}
			} else if (produto.equals("Platter M")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoplatterm))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioplatterm))
						+ "</td>	";
				message1 += "</tr>	";
				if (adicionaisplatterm != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisplatterm))
							+ "</td>	";
					message1 += "</tr>	";
				}
			} else if (produto.equals("Platter G")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoplatterg))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioplatterg))
						+ "</td>	";
				message1 += "</tr>	";
				if (adicionaisplatterg != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisplatterg))
							+ "</td>	";
					message1 += "</tr>	";
				}
			} else if (produto.equals("Café da manhã")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijocafe))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(friocafe)) + "</td>	";
				message1 += "</tr>	";

				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Suco: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(sucoscafe))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Bolo: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(bolos)) + "</td>	";
				message1 += "</tr>	";
				if (adicionaiscafe != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaiscafe))
							+ "</td>	";
					message1 += "</tr>	";
				}

			}else if (produto.equals("Box \"à luz de velas\"")) {
				
				
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Batida: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(batidaespecial))
						+ "</td>	";
				message1 += "</tr>	";
				if (adicionaisespecial != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisespecial))
							+ "</td>	";
					message1 += "</tr>	";
				}

			} else if (produto.equals("Sacola \"me leva que eu vou\"")) {			
				
				if (batidaespecial2 != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Batida: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(batidaespecial2))
							+ "</td>	";
					message1 += "</tr>	";
				} else if (vinhosespecial2 != null){
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Vinho: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(vinhosespecial2))
							+ "</td>	";
					message1 += "</tr>	";
				}else{
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Prosseco: </b></td>	";
					message1 += "<td style='border:1px solid black'>Prosseco" 
							+ "</td>	";
					message1 += "</tr>	";
				}
				if (adicionaisespecial2 != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisespecial2))
							+ "</td>	";
					message1 += "</tr>	";
				}

			}
			if (!produto.equals("Box \"à luz de velas\"") ) {
			if (!sucoaddsabor.equals("Selecione um sabor")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Suco adicional: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + sucoaddsabor + "</td>	";
				message1 += "</tr>	";
			}}
			if (!produto.equals("Box \"à luz de velas\"") ) {
			if (!tipovinho.equals("Selecione um sabor")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Sabor Vinho: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + tipovinho + "</td>	";
				message1 += "</tr>	";
			}}
			if (!obs.equals("")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Observação: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + obs + "</td>	";
				message1 += "</tr>	";
			}
			if (cartao != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Mensagem do cartão: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + mensagemcartao + "</td>	";
				message1 += "</tr>	";
			}
			if (presente != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Caso seja um presente, é de aniversário?: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + presente + "</td>	";
				message1 += "</tr>	";
			}

			if (retirar!=null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td colspan='2' style='border:1px solid black'><b>Frete: Retirar na Tijuca</b></td>	";
				message1 += "</tr>";
			} else {
				
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td colspan='2' style='border:1px solid black'><b>Subtotal: R$" + sub + ",00</b></td>	";
					message1 += "</tr>";
				
				
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td colspan='2' style='border:1px solid black'><b>Frete: R$" + frete + ",00</b></td>	";
				message1 += "</tr>";
			}
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td colspan='2' style='border:1px solid black'><b>Forma de Pagamento: " + fpagamento
					+ "</b></td>	";
			message1 += "</tr>";

			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td colspan='2' style='border:1px solid black'><b>Total: R$" + valor + ",00</b></td>	";
			message1 += "</tr></table>	<br><br><br>	";
			message1 += "<b>______________________________</b>";
			message1 += "<table><tr><th style=' text-align: left;vertical-align: top;'>";
			message1 += "<br><a href='http://www.caroldapetitpoa.com.br'>www.caroldapetitpoa.com.br</a>";
			message1 += "<br><a href='http://www.instagram.com/carol_da_petitpoa'>@carol_da_petitpoa</a>";
			message1 += "<br><a href='https://api.whatsapp.com/send?phone=5521965802201'>+55 21 96580-2201</a></th>";

			message1 += "<th style=' text-align: left;vertical-align: top;'>"
					+ "<img src=\"cid:AbcXyz123\" style='display: inline-block;'/></th></tr></table>";

			message1 += "</html>";

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(message1, "text/html; charset=UTF-8");

			// creates multi-part
			Multipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);

			// adds inline image attachments
			MimeBodyPart imagePart = new MimeBodyPart();
			imagePart.setHeader("Content-ID", "<AbcXyz123>");
			imagePart.setDisposition(MimeBodyPart.INLINE);
			// attach the image file
			imagePart.attachFile(reportLocation + "images/logo2.png");

			multipart.addBodyPart(imagePart);

			message.setContent(multipart);

			// sends the e-mail
			Transport.send(message);

			// send message

			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	public void responderPedido(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF8");
		PrintWriter out = response.getWriter();

		String escolha = request.getParameter("e");
		request.setAttribute("escolha", escolha);
		String email = request.getParameter("m");
		request.setAttribute("email", email);
		String nome = request.getParameter("n");
		request.setAttribute("nome", nome);
		String produto = request.getParameter("pd");
		request.setAttribute("produto", produto);
		String data = request.getParameter("d");
		request.setAttribute("data", data);
		String hora = request.getParameter("h");
		request.setAttribute("hora", hora);
		String valorProduto = request.getParameter("vp");
		request.setAttribute("valorProduto", valorProduto);
		String frete = request.getParameter("f");
		request.setAttribute("frete", frete);
		String total = request.getParameter("t");
		request.setAttribute("total", total);
		String fpagamento = request.getParameter("fp");
		String endereco = request.getParameter("end");
		request.setAttribute("endereco", endereco);
		request.setAttribute("fpagamento", fpagamento);
		ServletContext context = getServletContext();
		String reportLocation = context.getRealPath("");
		request.setCharacterEncoding("UTF-8");
		if (fpagamento.equals("Cartão de crédito")) {
			request.setAttribute("ativoLink", "true");
			request.getRequestDispatcher("resposta_pedido.jsp").forward(request, response);
			return;
		}

		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date data2 = null;
		try {
			data2 = formato.parse(data);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(data2);

		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'"); // Quoted
																		// "Z"
																		// to
																		// indicate
																		// UTC,
																		// no
																		// timezone
																		// offset
		df.setTimeZone(tz);

		// Start Date is on: April 1, 2008, 9:00 am
		java.util.Calendar startDate = new GregorianCalendar();
		startDate.set(java.util.Calendar.MONTH, cal.get(Calendar.MONTH));
		startDate.set(java.util.Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
		startDate.set(java.util.Calendar.YEAR, cal.get(Calendar.YEAR));
		startDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(hora.substring(0, 2)) + 3);
		startDate.set(java.util.Calendar.MINUTE, 0);
		startDate.set(java.util.Calendar.SECOND, 0);

		// End Date is on: April 1, 2008, 13:00
		java.util.Calendar endDate = new GregorianCalendar();
		endDate.set(java.util.Calendar.MONTH, cal.get(Calendar.MONTH));
		endDate.set(java.util.Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
		endDate.set(java.util.Calendar.YEAR, cal.get(Calendar.YEAR));
		endDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(hora.substring(6, 8)) + 3);
		endDate.set(java.util.Calendar.MINUTE, 0);
		endDate.set(java.util.Calendar.SECOND, 0);

		Date d1 = startDate.getTime();
		Date d2 = endDate.getTime();

		String nowAsISO1 = df.format(d1);
		request.setAttribute("d1", nowAsISO1);
		String nowAsISO2 = df.format(d2);
		request.setAttribute("d2", nowAsISO2);

		System.out.println(nowAsISO1);
		System.out.println(nowAsISO2);

		Properties props = new Properties();

		props.put("mail.smtp.host", "mail.caroldapetitpoa.com.br");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");
		// get Session
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("no.reply@caroldapetitpoa.com.br", "#(dJ;jF(mz](");
			}
		});
		// compose message
		try {

			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setFrom(new InternetAddress("no.reply@caroldapetitpoa.com.br"));
			message.setReplyTo(InternetAddress.parse(email, false));

			String message1 = "<html>";

			if (escolha.equals("aceito")) {
				message.setSubject("O seu pedido do " + produto + " para o dia " + data + " foi aceito!", "UTF-8");
				message1 = "Seu pedido foi aceito. Obrigada! :)\r\n" + "\r\n"
						+ "Obaaa! Seu pedido será produzido e entregue com muito carinho. Após realizar o pagamento seu pedido estará confirmado! Não esquece de mandar o comprovante pra gente, ta? \r\n"
						+ "Caso o contato não seja feito em até 3h clicando no número que aparece no rodapé do email você pode falar com a gente pelo WhatsApp! Até logo!<br><br>";

				message1 += "Ana Carolina da Silva Pinheiro<br>";
				message1 += "Banco Itaú<br>";
				message1 += "Agência: 0500<br>";
				message1 += "Cc: 005017127-1<br>";
				message1 += "CPF: 125.323.327-60<br>";
				message1 += "Pix: caroldapetitpoa@gmail.com<br>";
				message1 += "Total: R$" + total + ",00<br><br><br>";

			} else {
				message.setSubject("O seu pedido do " + produto + " para o dia " + data + " foi recusado =[", "UTF-8");

				message1 = "Infelizmente não iremos conseguir te atender nesta data e horário :( \r\n" + "\r\n"
						+ "Entraremos em contato para buscarmos outras possibilidades. \r\n"
						+ "Caso o contato não seja feito em até 3h clicando no número que aparece no rodapé do email você pode falar com a gente pelo WhatsApp! Até logo!<br><br>";

			}

			message1 += "<b>______________________________</b>";
			message1 += "<table><tr><th style=' text-align: left;vertical-align: top;'>";
			message1 += "<br><a href='http://www.caroldapetitpoa.com.br'>www.caroldapetitpoa.com.br</a>";
			message1 += "<br><a href='http://www.instagram.com/carol_da_petitpoa'>@carol_da_petitpoa</a>";
			message1 += "<br><a href='https://api.whatsapp.com/send?phone=5521965802201'>+55 21 96580-2201</a></th>";

			message1 += "<th style=' text-align: left;vertical-align: top;'>"
					+ "<img src=\"cid:AbcXyz123\" style='display: inline-block;'/></th></tr></table>";

			message1 += "</html>";

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(message1, "text/html; charset=UTF-8");

			// creates multi-part
			Multipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);

			// adds inline image attachments
			MimeBodyPart imagePart = new MimeBodyPart();
			imagePart.setHeader("Content-ID", "<AbcXyz123>");
			imagePart.setDisposition(MimeBodyPart.INLINE);
			// attach the image file
			imagePart.attachFile(reportLocation + "images/logo2.png");

			multipart.addBodyPart(imagePart);

			message.setContent(multipart);

			request.getRequestDispatcher("resposta_pedido.jsp").forward(request, response);
			out.print("True");
			out.flush();
			out.close();

			// sends the e-mail
			Transport.send(message);
			System.out.println("message sent successfully");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	public void aceitarPedido(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// responderPedido?e=aceitar&pd="+produto+"&vp=100&f=8&t=108&n=joao&m=oi@oi.com.br&d=21/12/2020&h=15:00-16:00
		response.setContentType("text/plain; charset=UTF-8");
		String nome = request.getParameter("nome");
		String produto = request.getParameter("produto");
		String data = request.getParameter("data");
		String hora = request.getParameter("hora");
		String email = request.getParameter("email");
		String total = request.getParameter("total");
		String link = request.getParameter("link");

		request.setAttribute("nome", nome);

		request.setAttribute("produto", produto);

		request.setAttribute("data", data);

		request.setAttribute("hora", hora);

		request.setAttribute("total", total);

		request.setAttribute("escolha", "aceito");
		request.setAttribute("ativoLink", "false");

		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF8");
		PrintWriter out = response.getWriter();

		ServletContext context = getServletContext();
		String reportLocation = context.getRealPath("");
		request.setCharacterEncoding("UTF-8");

		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date data2 = null;
		try {
			data2 = formato.parse(data);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();
		cal.setTime(data2);

		DateFormat df = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'"); // Quoted
																		// "Z"
																		// to
																		// indicate
																		// UTC,
																		// no
																		// timezone
																		// offset

		// Start Date is on: April 1, 2008, 9:00 am
		java.util.Calendar startDate = new GregorianCalendar();
		startDate.set(java.util.Calendar.MONTH, cal.get(Calendar.MONTH));
		startDate.set(java.util.Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
		startDate.set(java.util.Calendar.YEAR, cal.get(Calendar.YEAR));
		startDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(hora.substring(0, 2)));
		startDate.set(java.util.Calendar.MINUTE, 0);
		startDate.set(java.util.Calendar.SECOND, 0);

		// End Date is on: April 1, 2008, 13:00
		java.util.Calendar endDate = new GregorianCalendar();
		endDate.set(java.util.Calendar.MONTH, cal.get(Calendar.MONTH));
		endDate.set(java.util.Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
		endDate.set(java.util.Calendar.YEAR, cal.get(Calendar.YEAR));
		endDate.set(java.util.Calendar.HOUR_OF_DAY, Integer.valueOf(hora.substring(6, 8)));
		endDate.set(java.util.Calendar.MINUTE, 0);
		endDate.set(java.util.Calendar.SECOND, 0);

		Date d1 = startDate.getTime();
		Date d2 = endDate.getTime();

		String nowAsISO1 = df.format(d1);
		request.setAttribute("d1", nowAsISO1);
		String nowAsISO2 = df.format(d2);
		request.setAttribute("d2", nowAsISO2);

		System.out.println(nowAsISO1);
		System.out.println(nowAsISO2);

		Properties props = new Properties();

		props.put("mail.smtp.host", "mail.caroldapetitpoa.com.br");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		boolean enableSSL = true;

		props.put("mail.smtp.ssl.enable", enableSSL);
		// get Session
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("no.reply@caroldapetitpoa.com.br", "#(dJ;jF(mz](");
			}
		});
		// compose message
		try {

			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setFrom(new InternetAddress("no.reply@caroldapetitpoa.com.br"));
			message.setReplyTo(InternetAddress.parse(email, false));

			String message1 = "<html>";

			message.setSubject("O seu pedido do " + produto + " para o dia " + data + " foi aceito!", "UTF-8");
			message1 = "Seu pedido foi aceito. Obrigada! :)\r\n" + "\r\n"
					+ "Obaaa! Seu pedido será produzido e entregue com muito carinho. Após realizar o pagamento seu pedido estará confirmado! Não esquece de mandar o comprovante pra gente, ta? \r\n"
					+ "Caso o contato não seja feito em até 3h clicando no número que aparece no rodapé do email você pode falar com a gente pelo WhatsApp! Até logo!<br><br>";

			message1 += "Ana Carolina da Silva Pinheiro<br>";
			message1 += "Total: R$" + total + ",00<br>";
			message1 += "Link para pagamento em cartão de crédito: " + link + "<br>";
			// message1+="<th style='width:60% text-align: left;vertical-align:
			// top;'>"+
			// "<img src=\"cid:AbcXyz1234\" style='display:
			// inline-block;'/></th></tr></table><br><br><br>";

			message1 += "<b>______________________________</b>";
			message1 += "<table><tr><th style=' text-align: left;vertical-align: top;'>";
			message1 += "<br><a href='http://www.caroldapetitpoa.com.br'>www.caroldapetitpoa.com.br</a>";
			message1 += "<br><a href='http://www.instagram.com/carol_da_petitpoa'>@carol_da_petitpoa</a>";
			message1 += "<br><a href='https://api.whatsapp.com/send?phone=5521965802201'>+55 21 96580-2201</a></th>";

			message1 += "<th style=' text-align: left;vertical-align: top;'>"
					+ "<img src=\"cid:AbcXyz123\" style='display: inline-block;'/></th></tr></table>";

			message1 += "</html>";

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(message1, "text/html; charset=UTF-8");

			// creates multi-part
			Multipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);

			// adds inline image attachments
			MimeBodyPart imagePart = new MimeBodyPart();
			imagePart.setHeader("Content-ID", "<AbcXyz123>");
			imagePart.setDisposition(MimeBodyPart.INLINE);
			// attach the image file
			imagePart.attachFile(reportLocation + "images/logo2.png");

			multipart.addBodyPart(imagePart);

			MimeBodyPart imagePart2 = new MimeBodyPart();
			imagePart2.setHeader("Content-ID", "<AbcXyz1234>");
			imagePart2.setDisposition(MimeBodyPart.INLINE);
			// attach the image file
			imagePart2.attachFile(reportLocation + "images/ame.png");

			// multipart.addBodyPart(imagePart2);

			message.setContent(multipart);

			out.print("True");
			out.flush();
			out.close();

			// sends the e-mail
			Transport.send(message);

			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	public void validarCupom(HttpServletRequest request, HttpServletResponse response) {
		try {

			response.setContentType("text/plain; charset=UTF-8");
			PrintWriter out = response.getWriter();
			Pedido pedido = new Pedido();
			String email = request.getParameter("email");
			String cupom = request.getParameter("cupom");

			PedidoService pedidoService = new PedidoService();
			boolean resposta = false;
			pedido = pedidoService.isUserExists(email);
			if (pedido != null) {
				resposta = pedido.validarCupom(cupom);
			}
			System.out.println("resposta");

			System.out.println(resposta);

			out.print(resposta);
			out.flush();
			out.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public void especial(HttpServletRequest request, HttpServletResponse response) {
		try {

			request.getRequestDispatcher("includes/personalizadoForm.jsp").forward(request, response);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	public void emitirEspecial(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/plain; charset=UTF-8");
		response.setContentType("application/pdf");
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF8");
		PrintWriter out = response.getWriter();
		// set input and output stream
		// ServletOutputStream servletOutputStream = response.getOutputStream();
		String nome = request.getParameter("nomeE");
		String nomeReceber = request.getParameter("nomeReceberE");
		String endereco = request.getParameter("enderecocompletoE");
		String hora = request.getParameter("horaE");
		String conheceu = request.getParameter("conheceuE");
		String email = request.getParameter("emailE");
		String data = request.getParameter("dataE");
		String telefone = request.getParameter("telefoneE");
		String produto = request.getParameter("produtoE");
		String valor = request.getParameter("valorE");
		String obs = request.getParameter("obsE");
		String cartao = request.getParameter("cartaoE");
		String retirar = request.getParameter("retirarE");
		String mensagemcartao = request.getParameter("mensagemcartaoE");
		String presente = request.getParameter("presenteE");
		String frete = request.getParameter("fretehiddenE");
		String fpagamento = request.getParameter("fpagamentoE");

		String queijoespecial[] = request.getParameterValues("chk-custom-queijo");
		String frioespecial[] = request.getParameterValues("chk-custom-frios");
		String adicionaisespecial[] = request.getParameterValues("chk-custom-adicionais");
		
		String drinkespecial[] = request.getParameterValues("chk-custom-drink");

		String sucosespecial[] = request.getParameterValues("chk-custom-sucos");
		String vinhosespecial[] = request.getParameterValues("chk-custom-vinho");
		String bolos[] = request.getParameterValues("chk-custom-bolos");
		String sucoaddsabor = request.getParameter("saborsuco");
		String tipovinho = request.getParameter("tipovinho");
		
		
System.out.println(produto);
		ServletContext context = getServletContext();
		String reportLocation = context.getRealPath("");

		request.setCharacterEncoding("UTF-8");

		Integer sub = Integer.parseInt(valor) - Integer.parseInt(frete);

		Properties props = new Properties();

		props.put("mail.smtp.host", "mail.caroldapetitpoa.com.br");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		boolean enableSSL = true;

		props.put("mail.smtp.ssl.enable", enableSSL);

		// get Session
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("no.reply@caroldapetitpoa.com.br", "#(dJ;jF(mz](");
			}
		});
		// compose message
		try {

			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress("contato@caroldapetitpoa.com.br"));
			message.setFrom(new InternetAddress("no.reply@caroldapetitpoa.com.br"));
			message.setReplyTo(InternetAddress.parse(email, false));
			message.setSubject(nome + " - " + produto + " - " + data + " - " + hora, "UTF-8");

			String message1 = "<html>";

			message1 += "<table style='width:100%; border:1px solid black'>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<th style='border:1px solid black ;font-size:50px' colspan='2'>" + produto + "</th>	";

			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Nome: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + nome + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Hora: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + hora + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Data: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + data + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Telefone: </b></td>	";
			message1 += "<td style='border:1px solid black'><a href='whatsapp://send?text=MESSAGE&phone=+55"
					+ telefone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "") + "'>" + telefone
					+ "</a></td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>De onde nos conheceu?: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + conheceu + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>E-mail: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + email + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Nome de quem irá receber: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + nomeReceber + "</td>	";
			message1 += "</tr>	";
			if (retirar==null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Endereço: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + endereco + "</td>	";
				message1 += "</tr>	";
			}
			
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoespecial))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioespecial)) + "</td>	";
				message1 += "</tr>	";
				if (sucosespecial != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Suco: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(sucosespecial))
							+ "</td>	";
					message1 += "</tr>	";
				}
					if (vinhosespecial != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Vinho: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(vinhosespecial))
							+ "</td>	";
					message1 += "</tr>	";
				}
				if (adicionaisespecial != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisespecial))
							+ "</td>	";
					message1 += "</tr>	";
				}

			 
				if (drinkespecial != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Drink: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(drinkespecial))
						+ "</td>	";
				message1 += "</tr>	";
				
			}
				if (sucosespecial != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Suco: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(sucosespecial))
						+ "</td>	";
				message1 += "</tr>	";
				}
				if (bolos != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Bolo: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(bolos)) + "</td>	";
				message1 += "</tr>	";
				}
				

						if (!sucoaddsabor.equals("Selecione um sabor") ) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Suco adicional: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + sucoaddsabor + "</td>	";
				message1 += "</tr>	";
			}
			if (!tipovinho.equals("Selecione um sabor")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Sabor Vinho: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + tipovinho + "</td>	";
				message1 += "</tr>	";
			}
			if (!obs.equals("")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Observação: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + obs + "</td>	";
				message1 += "</tr>	";
			}
			if (cartao != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Mensagem do cartão: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + mensagemcartao + "</td>	";
				message1 += "</tr>	";
			}
			if (presente != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Caso seja um presente, é de aniversário?: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + presente + "</td>	";
				message1 += "</tr>	";
			}

			if (retirar!=null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td colspan='2' style='border:1px solid black'><b>Frete: Retirar na Tijuca</b></td>	";
				message1 += "</tr>";
			} else {
				
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td colspan='2' style='border:1px solid black'><b>Subtotal: R$" + sub + ",00</b></td>	";
					message1 += "</tr>";
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td colspan='2' style='border:1px solid black'><b>Frete: R$" + frete + ",00</b></td>	";
					message1 += "</tr>";
				}
				
			
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td colspan='2' style='border:1px solid black'><b>Forma de Pagamento: " + fpagamento
					+ "</b></td>	";
			message1 += "</tr>";

			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td colspan='2' style='border:1px solid black'><b>Total: R$" + valor + ",00</b></td>	";
			message1 += "</tr></table>	<br><br><br>	";

			message1 += "<table style='width:100%'><tr>";
			message1 += "<th style=' text-align: center;vertical-align: top;'>";
			message1 += "<a href='http://www.caroldapetitpoa.com.br/responderPedido?e=aceito&pd=" + produto + "&vp="
					+ sub + "&f=" + frete + "8&t=" + valor + "&n=" + nome + "&m=" + email + "&d=" + data + "&h=" + hora
					+ "&fp=" + fpagamento + "&end=" + endereco
					+ "' style='background-color: green;  color: white;  padding: 15px 25px;  text-align: center;  text-decoration: none;  display: inline-block;'>ACEITAR</a>";
			message1 += "</th><th style=' text-align: center;vertical-align: top;'>";
			message1 += "<a href='http://www.caroldapetitpoa.com.br/responderPedido?e=recusado&pd=" + produto + "&vp="
					+ sub + "&f=" + frete + "8&t=" + valor + "&n=" + nome + "&m=" + email + "&d=" + data + "&h=" + hora
					+ "&fp=" + fpagamento + "&end=" + endereco
					+ "' style='background-color: red;  color: white;  padding: 15px 25px;  text-align: center;  text-decoration: none;  display: inline-block;'>RECUSAR</a>";
			message1 += "</th></tr></table><br><br>";

			message1 += "<b>______________________________</b>";
			message1 += "<table><tr><th style=' text-align: left;vertical-align: top;'>";
			message1 += "<br><a href='http://www.caroldapetitpoa.com.br'>www.caroldapetitpoa.com.br</a>";
			message1 += "<br><a href='http://www.instagram.com/carol_da_petitpoa'>@carol_da_petitpoa</a>";
			message1 += "<br><a href='https://api.whatsapp.com/send?phone=5521965802201'>+55 21 96580-2201</a></th>";

			message1 += "<th style=' text-align: left;vertical-align: top;'>"
					+ "<img src=\"cid:AbcXyz123\" style='display: inline-block;'/></th></tr></table>";

			message1 += "</html>";

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(message1, "text/html; charset=UTF-8");

			// creates multi-part
			Multipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);

			// adds inline image attachments
			MimeBodyPart imagePart = new MimeBodyPart();
			imagePart.setHeader("Content-ID", "<AbcXyz123>");
			imagePart.setDisposition(MimeBodyPart.INLINE);
			// attach the image file
			imagePart.attachFile(reportLocation + "images/logo2.png");

			multipart.addBodyPart(imagePart);

			message.setContent(multipart);

			out.print("True");
			out.flush();
			out.close();
			// sends the e-mail
			Transport.send(message);

			// send message

			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

		try {
			MimeMessage message = new MimeMessage(session);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			message.setFrom(new InternetAddress("no.reply@caroldapetitpoa.com.br"));
			message.setReplyTo(InternetAddress.parse(email, false));
			message.setSubject("Pré-pedido do " + produto + " para o dia " + data + " realizado com sucesso!", "UTF-8");

			String message1 = "<html>";

			message1 = "Recebemos seu pré-pedido. Obrigada! :)\r\n" + "\r\n"
					+ "Entraremos em contato para finalizarmos o pagamento e confirmarmos a disponibilidade da sua entrega. \r\n"
					+ "Caso o contato não seja feito em até 3h clicando no número que aparece no rodapé do email você pode falar com a gente pelo WhatsApp! Até logo!<br><br>";

			message1 = "<html>";

			message1 += "<table style='width:100%; border:1px solid black'>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<th style='border:1px solid black ;font-size:50px' colspan='2'>" + produto + "</th>	";

			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Nome: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + nome + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Hora: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + hora + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Data: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + data + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Telefone: </b></td>	";
			message1 += "<td style='border:1px solid black'><a href='whatsapp://send?text=MESSAGE&phone=+55"
					+ telefone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "") + "'>" + telefone
					+ "</a></td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>De onde nos conheceu?: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + conheceu + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>E-mail: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + email + "</td>	";
			message1 += "</tr>	";
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td style='border:1px solid black'><b>Nome de quem irá receber: </b></td>	";
			message1 += "<td style='border:1px solid black'>" + nomeReceber + "</td>	";
			message1 += "</tr>	";
			if (retirar==null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Endereço: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + endereco + "</td>	";
				message1 += "</tr>	";
			}
			
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Queijos: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(queijoespecial))
						+ "</td>	";
				message1 += "</tr>	";
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Frios: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(frioespecial)) + "</td>	";
				message1 += "</tr>	";
				if (sucosespecial != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Suco: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(sucosespecial))
							+ "</td>	";
					message1 += "</tr>	";
				}
					if (vinhosespecial != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Vinho: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(vinhosespecial))
							+ "</td>	";
					message1 += "</tr>	";
				}
				if (adicionaisespecial != null) {
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td style='border:1px solid black'><b>Adicionais: </b></td>	";
					message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(adicionaisespecial))
							+ "</td>	";
					message1 += "</tr>	";
				}

			 
				if (drinkespecial != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Drink: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(drinkespecial))
						+ "</td>	";
				message1 += "</tr>	";
				
			}
				if (sucosespecial != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Suco: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(sucosespecial))
						+ "</td>	";
				message1 += "</tr>	";
				}
				if (bolos != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Bolo: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + removerchar(Arrays.toString(bolos)) + "</td>	";
				message1 += "</tr>	";
				}
				

						if (!sucoaddsabor.equals("Selecione um sabor") ) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Suco adicional: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + sucoaddsabor + "</td>	";
				message1 += "</tr>	";
			}
			if (!tipovinho.equals("Selecione um sabor")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Sabor Vinho: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + tipovinho + "</td>	";
				message1 += "</tr>	";
			}
			if (!obs.equals("")) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Observação: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + obs + "</td>	";
				message1 += "</tr>	";
			}
			if (cartao != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Mensagem do cartão: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + mensagemcartao + "</td>	";
				message1 += "</tr>	";
			}
			if (presente != null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td style='border:1px solid black'><b>Caso seja um presente, é de aniversário?: </b></td>	";
				message1 += "<td style='border:1px solid black'>" + presente + "</td>	";
				message1 += "</tr>	";
			}

			if (retirar!=null) {
				message1 += "<tr style='border:1px solid black'>	";
				message1 += "<td colspan='2' style='border:1px solid black'><b>Frete: Retirar na Tijuca</b></td>	";
				message1 += "</tr>";
			} else {
				
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td colspan='2' style='border:1px solid black'><b>Subtotal: R$" + sub + ",00</b></td>	";
					message1 += "</tr>";
					message1 += "<tr style='border:1px solid black'>	";
					message1 += "<td colspan='2' style='border:1px solid black'><b>Frete: R$" + frete + ",00</b></td>	";
					message1 += "</tr>";
				}
				
			
			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td colspan='2' style='border:1px solid black'><b>Forma de Pagamento: " + fpagamento
					+ "</b></td>	";
			message1 += "</tr>";


			message1 += "<tr style='border:1px solid black'>	";
			message1 += "<td colspan='2' style='border:1px solid black'><b>Total: R$" + valor + ",00</b></td>	";
			message1 += "</tr></table>	<br><br><br>	";
			message1 += "<b>______________________________</b>";
			message1 += "<table><tr><th style=' text-align: left;vertical-align: top;'>";
			message1 += "<br><a href='http://www.caroldapetitpoa.com.br'>www.caroldapetitpoa.com.br</a>";
			message1 += "<br><a href='http://www.instagram.com/carol_da_petitpoa'>@carol_da_petitpoa</a>";
			message1 += "<br><a href='https://api.whatsapp.com/send?phone=5521965802201'>+55 21 96580-2201</a></th>";

			message1 += "<th style=' text-align: left;vertical-align: top;'>"
					+ "<img src=\"cid:AbcXyz123\" style='display: inline-block;'/></th></tr></table>";

			message1 += "</html>";

			// creates message part
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent(message1, "text/html; charset=UTF-8");

			// creates multi-part
			Multipart multipart = new MimeMultipart("related");
			multipart.addBodyPart(messageBodyPart);

			// adds inline image attachments
			MimeBodyPart imagePart = new MimeBodyPart();
			imagePart.setHeader("Content-ID", "<AbcXyz123>");
			imagePart.setDisposition(MimeBodyPart.INLINE);
			// attach the image file
			imagePart.attachFile(reportLocation + "images/logo2.png");

			multipart.addBodyPart(imagePart);

			message.setContent(multipart);

			// sends the e-mail
			Transport.send(message);

			// send message

			System.out.println("message sent successfully");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	

	public String removerchar(String str) {
		String tmp = "";
		tmp = str.replace("[", "");
		tmp = tmp.replace("]", "");
		return tmp;
	}

}

// Pedido pedido = new Pedido();

// PedidoService pedidoService = new PedidoService();

// Boolean resposta = pedidoService.register(pedido);
// pedido = pedidoService.isUserExists(email);
// if(pedido!=null){
// pedido.novoPedido();
// }else{
// pedido = new Pedido();
// pedido.setEmailCliente(email);
// pedido.novoPedido();
// }
// pedido.verificarCupom();
// Boolean resposta = pedidoService.register(pedido);
// pedido.validarCupom("PETIT-G89Km2JY");
