
package model;

import java.io.Serializable;
import java.sql.Blob;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.joda.time.Period;

import util.CodeConfig;
import util.VoucherCodes;

@Entity
@Table(name = "Pedido")
public class Pedido implements Serializable {

	public static final String DATE_FORMAT_NOW = "dd/MM/yyyy";
	
	@Override
	public String toString() {
		return "Pedido [id=" + id + ", emailCliente=" + emailCliente + ", numeroPedidos=" + numeroPedidos
				+ ", numeroCupons=" + numeroCupons + ", cupomDesconto=" + cupomDesconto + "]";
	}
	@Id
	@GeneratedValue
	private Long id;
	private String emailCliente;
	private int numeroPedidos;
	private int numeroCupons;
	private String cupomDesconto;
	private Date cupomValidade;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmailCliente() {
		return emailCliente;
	}
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	public int getNumeroPedidos() {
		return numeroPedidos;
	}
	public void setNumeroPedidos(int numeroPedidos) {
		this.numeroPedidos = numeroPedidos;
	}
	public int getNumeroCupons() {
		return numeroCupons;
	}
	public void setNumeroCupons(int numeroCupons) {
		this.numeroCupons = numeroCupons;
	}
	public String getCupomDesconto() {
		return cupomDesconto;
	}
	public void setCupomDesconto(String cupomDesconto) {
		this.cupomDesconto = cupomDesconto;
	}
	
	public Pedido(Long id, String emailCliente, int numeroPedidos, int numeroCupons, String cupomDesconto,
			Date cupomValidade) {
		super();
		this.id = id;
		this.emailCliente = emailCliente;
		this.numeroPedidos = numeroPedidos;
		this.numeroCupons = numeroCupons;
		this.cupomDesconto = cupomDesconto;
		this.cupomValidade = cupomValidade;
	}
	public Date getCupomValidade() {
		return cupomValidade;
	}
	public void setCupomValidade(Date cupomValidade) {
		this.cupomValidade = cupomValidade;
	}
	public Pedido() {
		super();
		
	}
	
	public void novoPedido(){
		this.numeroPedidos++;
		
	}
	
	public void verificarCupom(){
		if(this.numeroPedidos>=5){
			CodeConfig config = CodeConfig.length(8).withPrefix("PETIT-");            
            String code = VoucherCodes.generate(config);
            this.cupomDesconto=code;
            java.util.Date data = new java.util.Date();  
            java.sql.Date dataSql = new java.sql.Date(data.getTime());
            this.cupomValidade=dataSql;
            this.numeroPedidos=this.numeroPedidos-5;
            this.numeroCupons=this.numeroCupons+1;
		}
		
	}
	
	public boolean validarCupom(String cupom){
		if(this.cupomDesconto.equals(cupom)){
			java.util.Date data = new java.util.Date();  
            java.sql.Date dataSql = new java.sql.Date(data.getTime());            
           
            DateTime start = new DateTime(dataSql);
            DateTime end = new DateTime(this.cupomValidade);          

            Months months = Months.monthsBetween(start, end);
            if(months.getMonths()<0){
            	this.cupomDesconto=null;
            	this.cupomValidade=null;
            	return false;
            }else{
            	this.cupomDesconto=null;
            	this.cupomValidade=null;
            	return true;
            }
            
		}
		return false;
		
	}
	
	

	
	

	
	






}
